<?php
/**
 * Created by PhpStorm.
 * User: Iridian 1
 * Date: 1/02/2016
 * Time: 12:29 PM
 */

namespace AppBundle\Service;

use AppBundle\Entity\Tarjeta;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\PropertyAccess\PropertyAccess;
define('MULTIPART_BOUNDARY', '----'.md5(time()));
define('EOL',"\r\n");
class QI
{
    protected $em;
    protected $request_stack;
    protected $locale;
    protected $container;

    protected $textos		= null;
    protected $textosDB		= null;
    protected $textosBigDB	= null;

    protected $settings		= null;
    protected $imagenes = null;
    protected $imagenesGen = null;
    protected $pixel		= null;
    protected $archivos		= null;


    public function __construct(EntityManager $em, RequestStack $request_stack, Container $container)
    {
        $this->em = $em;
        $this->request_stack = $request_stack;
        $this->container = $container;
        //$this->locale = $request_stack->getCurrentRequest()->getLocale();
    }

    public function qs($clase)
    {
        return $this->em->getRepository('AppBundle:Texto')->findAll();
    }

    private function getResults($entidad){
        $qb = $this->em->createQueryBuilder()
            ->select('s')
            ->from($entidad, 's', 's.llave')
            ->where('s.llave is not null')
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true)
            ->getArrayResult();
        return $qb;
    }

    /**
     * Retorna la variable con textos populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los textos intenacionalizados
     */
    private function getTextosDB() {
        if ($this->textosDB == null) {
            $this->textosDB = $this->getResults('AppBundle:Texto');
        }
        return $this->textosDB;
    }
    private function getArchivos() {
        if ($this->archivos == null) {
            $this->archivos = $this->getResults('AppBundle:Archivo');
        }
        return $this->archivos;
    }
    public function getArchivo($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $lang = ucfirst($locale);
        $arrTextos = $this->getArchivos();
        $col = 'archivo'.$lang;

        if(isset($arrTextos[$key][$col]) && $arrTextos[$key][$col] != '')
            return $this->container->getParameter('app.path.images').'/'.$arrTextos[$key][$col];
        else
            return $key;
    }

    /**
     * Retorna la variable con el pixel la primera vez que se llama esta función, el pixel se trae de la base de datos
     * @return multitype: string con el id del pixel
     */
    public function getPixel() {
        if ($this->pixel == null) {
            $this->pixel = $this->em->getRepository('AppBundle:Pixel')->find(1);
        }
        return ($this->pixel) ? $this->pixel->getIdentificador() : '';
    }

    /**
     * Obtener uno de los texto fijos internacionalizados según el UserCulture actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El texto solicitado
     */
    public function getTextoDB($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $arrTextos = $this->getTextosDB();

        if(isset($arrTextos[$key][$locale]) && $arrTextos[$key][$locale] != '')
            return $arrTextos[$key][$locale];
        else
            return $key;
    }

    private function getTextosBigDB() {
        if ($this->textosBigDB == null) {
            $this->textosBigDB = $this->getResults('AppBundle:TextoBig');
        }
        return $this->textosBigDB;
    }

    public function getTextoBigDB($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $arrTextos = $this->getTextosBigDB();
        if(isset($arrTextos[$key][$locale]) && $arrTextos[$key][$locale] != '')
            return $arrTextos[$key][$locale];
        else
            return $key;
    }


    private function getImagenes() {
        if ($this->imagenes == null) {
            $this->imagenes = $this->getResults('AppBundle:Imagengaleria');
        }
        return $this->imagenes;
    }

    private function getImagenesGeneral() {
        if ($this->imagenesGen == null) {
            $this->imagenesGen = $this->getResults('AppBundle:Imagen');
        }
        return $this->imagenesGen;
    }

    public function getImagengen($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenesGeneral();
        $path = $this->container->getParameter('app.path.images');
        //exit(\Doctrine\Common\Util\Debug::dump($imagenes));
        if(isset($imagenes[$key])){
            return $path.'/'.$imagenes[$key]['image'];
        }
        else
            return $path.'/';
    }


    public function getImagen($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenes();
        $path = $this->container->getParameter('app.path.imagesgal');
        if(isset($imagenes[$key])){
            return $path.'/'.$imagenes[$key]['image'.$locale];
        }
        else
            return $path.'/';
    }

    public function getImagenAlt($key) {
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key]['alt'];
        }
        else
            return '';
    }

    public function getImagenLink($key) {
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key]['link'];
        }
        else
            return '';
    }

    public function getImagenTitulo($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenes();
        $path = $this->container->getParameter('app.path.imagesgal');
        if(isset($imagenes[$key])){
            return $imagenes[$key]['titulo'.$locale];
        }
        else
            return '';
    }



    public function getImagenResumen($key) {
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $imagenes = $this->getImagenes();
        if(isset($imagenes[$key])){
            return $imagenes[$key]['resumen'.$locale];
        }
        else
            return '';
    }


    /**
     * Retorna la variable con settings populada la primera vez que se llama esta función, los textos se traen de la base de datos
     * @return multitype: Arreglo con los settings
     */
    private function getSettingsDB() {
        if ($this->settings == null) {
            $this->settings = $this->getResults('AppBundle:Settings');
        }
        return $this->settings;
    }


    /**
     * Obtener uno de los texto fijos internacionalizados según el local actual y el nombre del texto solicitado. Los textos se traen de la base de datos
     * @param string $key Nombre (identificador) del texto solicitado
     * @return string El setting solicitado
     */
    public function getSettingDB($key) {
        $settings = $this->getSettingsDB();
        if(isset($settings[$key]) && $settings[$key]['valor'] != '')
            return $settings[$key]['valor'];
        else
            return $key;
    }

    public function getGaleria($llave){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.imagesgal');
        $path_carta = $this->container->getParameter('app.path.carta');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('a.id','a.llave',"concat('".$path."/',a.imageEn) as fondo","concat('".$path."/',a.imageEs) as icono","concat('".$path."/',a.image".$locale.") as imagen",'a.alt','a.link', "concat('".$path_carta."/',a.documento".$locale.") as doc","a.titulo".$locale." as titulo","a.resumen".$locale." as resumen")
            ->from('AppBundle:Imagengaleria', 'a')
            ->leftJoin('a.galeria', 'g')
            ->where('g.llave = :llave')
            ->andWhere('a.visible = 1')
            ->setParameter('llave', $llave)
            ->orderBy('a.orden', 'asc');

        $out = array();
        $results = $qb->getQuery()->getArrayResult();
        foreach($results as $item){
            $link = $item['link'];
            if(strpos($link,'http') !== false)
                $item['target'] = '_blank';
            else{
                $item['target'] = '_self';
                if(strpos($link,'#') === false && $link != null)
                    $item['link'] = $this->container->get('router')->generate($link);
            }
            array_push($out,$item);
        }
        return $out;
    }

    public function getGaleriaFull($llave){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.imagesgal');
        $path_carta = $this->container->getParameter('app.path.carta');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('a.id',"concat('".$path."/',a.image".$locale.") as imagen",'a.alt','a.link', "concat('".$path_carta."/',a.documento".$locale.") as doc","a.titulo".$locale." as titulo","a.resumen".$locale." as resumen")
            ->from('AppBundle:Imagengaleria', 'a')
            ->leftJoin('a.galeria', 'g')
            ->where('g.llave = :llave')
            ->setParameter('llave', $llave)
            ->orderBy('a.orden', 'asc');

        $out = array();
        $results = $qb->getQuery()->getArrayResult();
        foreach($results as $item){
            $link = $item['link'];
            if(strpos($link,'http') !== false)
                $item['target'] = '_blank';
            else{
                $item['target'] = '_self';
                if(strpos($link,'#') === false && $link != null)
                    $item['link'] = $this->container->get('router')->generate($link);
            }
            array_push($out,$item);
        }
        return $out;
    }

    public function getAniosBlog(){
        return $this->em->getRepository("BlogiridianBundle:Post")
            ->createQueryBuilder('post')
            ->select('post.id as id, MONTH(post.fecha) as mes, YEAR(post.fecha) as anio, post.fecha as fecha')
            ->where('post.visible = 1')
            ->groupBy('anio, mes')
            ->orderBy('post.fecha', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function getResultadosBlog($term,$limit = null,$year = null, $month = null,$producto=null,$destacado = null,$search=null){
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.blogs');
        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p.id','c.id as categoriaId','r.nombre'.$locale.' as producto','p.titulo'.$locale.' as titulo','p.contenido'.$locale.' as contenido',"concat('".$path."/',p.image) as imagen, p.fecha"
            )
            ->from('BlogiridianBundle:Post', 'p')
            ->leftJoin('p.producto',"r")
            ->leftJoin('p.categoria',"c")
            ->andWhere('p.visible = 1')
            ->groupBy('p.id')
            ->orderBy('p.fecha', 'asc');
        if($limit)
            $qb->setMaxResults($limit);
        if($year && $month){
            $qb->andWhere('MONTH(p.fecha) = '.$month)
                ->andWhere('YEAR(p.fecha) = '.$year);
        }
        if($producto){
            $qb->andWhere('p.producto = '.$producto->getId());
        }
        if($destacado){
            $qb->andWhere('p.destacado = 1');
        }
        if($search ){
            $qb->andWhere("p.titulo".$locale." like '%".$search."%'");
        }
        if($term){
            $cad = '';
            $or = '';
            $searches= explode(' ', $term);
            foreach ($searches as $param) {
                $cad .= $or."p.tituloEs like '%".$param."%'";
                $or = ' or ';
                $cad .= $or."p.tituloEn like '%".$param."%'";
                $cad .= $or."p.tags like '%".$param."%'";
            }
            $qb->andWhere($cad);
        }

        $results = $qb->getQuery()->getArrayResult();
        return $results;
    }

    public function getById($tabla,$campo,$id){
        $out = $this->em->getRepository($tabla)->findBy(array($campo=>$id),array('orden'=>'asc'));
        return $out;
    }

    /**
     * @param $subject
     * @param $from
     * @param $to
     * @param $custom
     * @param $template
     */
    public function sendMail($subject, $from, $to, $template){
        try{
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: '.$from."\r\n".
                'Reply-To: '.$from."\r\n" .
                'X-Mailer: PHP/' . phpversion();

            $message = $template;

            if(mail($to, $subject, $message, $headers)){
                //echo 'Your mail has been sent successfully.';
            } else{
                //echo 'Unable to send email. Please try again.';
            }
        }catch(\Exception $e){

        }

    }

    /**
     * @param $asunto
     * @param $to
     * @param $html
     * @return bool|string
     */
    public function sendMailIB($asunto,$to,$html){
        $postdata =array(
            'from' => "noreply@info.plaz.com.co",
            'to' => $to,
            'replyTo' => "noreply@info.plaz.com.co",
            'subject' => $asunto,
            'html' => $html,
            'intermediateReport'=> 'true'
        );
        return $this->send_mail_infoBip($postdata);
    }

    function getBody($fields) {
        $content = '';
        foreach ($fields as $FORM_FIELD => $value) {
            $content .= '--' . MULTIPART_BOUNDARY . EOL;
            $content .= 'Content-Disposition: form-data; name="' . $FORM_FIELD . '"' . EOL;
            $content .= EOL . $value . EOL;
        }
        return $content . '--' . MULTIPART_BOUNDARY . '--'; // Email body should end with "--"
    }

    /*
     * Method to get the headers for a basic authentication with username and passowrd
    */
    function getHeader($username, $password){
        // basic Authentication
        $auth = base64_encode("$username:$password");

        // Define the header
        return array("Authorization:Basic $auth", 'Content-Type: multipart/form-data ; boundary=' . MULTIPART_BOUNDARY );
    }

    function send_sms_infoBip($postdata)
    {
        $url = 'https://api.infobip.com/sms/1/text/single';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type: application/json'
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_USERPWD,'desarrolloplaz' . ":" . 'Temporaltemporal1');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postdata );
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    function send_mail_infoBip($postdata)
    {
        $url = 'https://api.infobip.com/email/1/send';
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);
        $additional_headers = array(
            'Accept: application/json',
            'Content-Type:  multipart/form-data ; boundary=' . MULTIPART_BOUNDARY
        );
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch,CURLOPT_USERPWD,'desarrolloplaz' . ":" . 'Temporaltemporal1');
        curl_setopt($ch, CURLOPT_POSTFIELDS,$this->getBody($postdata) );
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $data = curl_exec($ch);
        $information = curl_getinfo($ch);
        //dump($data);
        //die(dump($information));
        curl_close($ch);
        return $data;
    }

    public function getSeo( $url, $homepage)
    {
        $url_abs = $url;
        $url = str_replace($homepage,'',$url);
        $locale = $this->request_stack->getCurrentRequest()->getLocale();
        $locale = ucfirst($locale);
        $path = $this->container->getParameter('app.path.images');
        $qb = $this->em->createQueryBuilder();
        $qb2 = $this->em->createQueryBuilder();
        //exit(dump($url));
        if($url == "")
            $url = "/";
        $seo = $qb
            ->select('p.id','p.titulo'.$locale.' as titulo',"concat('".$path."/',p.imagen) as imagen",'p.descripcion'.$locale.' as descripcion')
            ->from('AppBundle:Seo', 'p')
            ->where('p.url = :url')
            ->setParameter('url',$url)
            ->orderBy('p.id', 'desc')
            ->getQuery()
            ->getOneOrNullResult();

        if($seo == null){
            $seo = $qb2
                ->select('s.id','s.titulo'.$locale.' as titulo',"concat('".$path."/',s.imagen) as imagen",'s.descripcion'.$locale.' as descripcion')
                ->from('AppBundle:Seo', 's')
                ->orderBy('s.id', 'desc')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        }
        return array(
            'seo' => $seo,
            'homepage'=>str_replace(array("app.php/","app_dev.php/"),array("",""),$homepage),
            'url'=>$url_abs
        );
    }

    public function slugify($text)
    {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }

    public function getYoutubeId($url){
        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
        return $my_array_of_vars['v'];
    }

    /**
     * @param $entity
     */
    public function saveFire($entity){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $metadata = $em->getClassMetadata(get_class($entity));
        $campos = $metadata->fieldNames;
        $accessor = PropertyAccess::createPropertyAccessor();
        $arr = array();
        foreach ($campos as $campo){
            $valor = $accessor->getValue($entity,$campo);
            if($valor instanceof \DateTime){
                $arr[$campo] = $valor->format("Y-m-d H:i:s");
            }else{
                $arr[$campo] = $valor;
            }
        }
        $campos = $metadata->associationMappings;
        foreach ($campos as $campo){
            $campo = $campo['fieldName'];
            $valor = $accessor->getValue($entity,$campo);
            $arr_in = array();
            if($valor instanceof PersistentCollection){
                foreach ($valor as $inner){
                    array_push($arr_in,$inner->getId());
                }
                $arr[$campo] = $arr_in;
            }else{
                if(is_string($valor) || $valor == null){
                }else
                    $arr[$campo] = $valor->getId();
            }
        }
        $json = json_encode($arr);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        $tabla = explode(':',get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        $ref = $database->getReference("/$tabla/".$entity->getId())->set($arr);
    }

    /**
     * @param $entity
     */
    public function removeFire($entity){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        $tabla = explode(':',get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        if(is_numeric($entity->getId())){
            $ref = $database->getReference("/$tabla/".$entity->getId())->remove();
        }
    }

    /**
     * @param $entity
     */
    public function removeFireBack($tabla, $id){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        if($id){
            $ref = $database->getReference("/$tabla/".$id)->remove();
        }
    }



    /**
     * @param $entity
     */
    public function removeFireByUid($entity,$uid){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        $tabla = explode(':',get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        $ref = $database->getReference("/$tabla/$uid/".$entity->getId())->remove();
        //dump($ref);
    }

    /**
     * @param $entity
     */
    public function saveFireByUid(Tarjeta $entity){
        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $this->em;
        $metadata = $em->getClassMetadata(get_class($entity));
        $campos = $metadata->fieldNames;
        $accessor = PropertyAccess::createPropertyAccessor();
        $arr = array();
        foreach ($campos as $campo){
            $valor = $accessor->getValue($entity,$campo);
            if($valor instanceof \DateTime){
                $arr[$campo] = $valor->format("Y-m-d H:i:s");
            }else{
                $arr[$campo] = $valor;
            }
        }
        $campos = $metadata->associationMappings;
        foreach ($campos as $campo){
            $campo = $campo['fieldName'];
            $valor = $accessor->getValue($entity,$campo);
            $arr_in = array();
            if($valor instanceof PersistentCollection){
                foreach ($valor as $inner){
                    array_push($arr_in,$inner->getId());
                }
                $arr[$campo] = $arr_in;
            }else{
                $arr[$campo] = $valor->getId();
            }
        }
        $json = json_encode($arr);
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/../../../firebase_credentials.json');
        $firebase = (new Factory())
            ->withServiceAccount($serviceAccount)
            ->create();
        $database = $firebase->getDatabase();
        $tabla = explode(':',get_class($entity));
        $tabla = end($tabla);
        $tabla = explode("\\",get_class($entity));
        $tabla = end($tabla);
        $uid = $entity->getUsuario()->getUid();
        $ref = $database->getReference("/$tabla/$uid/".$entity->getId())->set($arr);
    }
}
