<?php

namespace NosotrosBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/nosotros", name="nosotros")
     */
    public function nosotrosAction()
    {
        $faqs = $this->getDoctrine()->getRepository('NosotrosBundle:Faq')->findBy(array('visible'=>true),array('orden'=>'asc'));
        return $this->render('NosotrosBundle:Default:nosotros.html.twig',array('faqs'=>$faqs));
    }
}
