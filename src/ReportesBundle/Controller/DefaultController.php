<?php

namespace ReportesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\Query\ResultSetMapping;
use CarroiridianBundle\Entity\Factura;



class DefaultController extends Controller
{
    /**
     * @Route("/test-mail", name="test_mail")
     */
    public function testMailAction(Request $request)
    {
        $qi = $this->get('qi');
        $resp = $qi->sendMailIB('Prueba','mauricio@iridian.co','<h1>Prueba de listapp</h1>');
        die(dump($resp));

    }

    /**
     * @Route("/reporte-restaurantes", name="reporte_restaurantes")
     */
    public function reporteRestaurantesAction(Request $request)
    {
        date_default_timezone_set('America/Bogota');
        $hoy = str_replace(' ', '', date('Y-m-d'));
        $html = '';
        $restaurantes = $this->getDoctrine()->getRepository('ListappBundle:Restaurante')->findBy(array('visible'=>true));
        foreach ($restaurantes as $restaurante){
            $pedidos = $this->getDoctrine()->getRepository('ListappBundle:Pedido')->createQueryBuilder('q')
                ->select('q.id','p.nombre plato',"DATE_FORMAT(q.fecha, '%h:%i pm') hora",'u.nombre cliente')
                ->leftJoin('q.plato','p')
                ->leftJoin('p.restaurante','r')
                ->leftJoin('q.usuario','u')
                ->where('r.id = '.$restaurante->getId())
                ->andWhere("DATE_FORMAT(q.fecha, '%Y-%m-%d') = '".$hoy."'")
                ->getQuery()
                ->getArrayResult();
            if(count($pedidos) > 0){
                $html = "<h1 style='font-family: Helvetica, Arial, sans-serif;color: #2ca397'>Hola ".$restaurante->getNombre()."</h1>";
                $html .= "<p style='font-family: Helvetica, Arial, sans-serif;'>Tienes ".count($pedidos)." Listapp para hoy ".$hoy." (AAAA-MM-DD)</p><br>";
                $html .= "<table  cellspacing='0' cellpadding='10' border='0' style='border: 2px solid #2ca397;'>
                  <thead style='background-color: #2ca397;color: #fff;font-family: Helvetica, Arial, sans-serif;'>
                    <tr style='padding: 13px;text-align: left;'>
                      <th style='padding: 13px;text-align: left;'>Id</th>
                      <th style='padding: 13px;text-align: left;'>Plato</th>
                      <th style='padding: 13px;text-align: left;'>Hora</th>
                      <th style='padding: 13px;text-align: left;'>Cliente</th>
                  </tr></thead><tbody>";
                foreach ($pedidos as $pedido){
                    $html .= "<tr>
                                <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>".$pedido['id']."</td>
                                <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>".$pedido['plato']."</td>
                                <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>".$pedido['hora']."</td>
                                <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>".$pedido['cliente']."</td>
                            </tr>";
                }
                $html .= "</tbody></table><br><br>";
                $qi = $this->get('qi');
                $from = $qi->getSettingDB('sender_mail');
                $this->get('qi')->sendMail('Pedidos Listapp de hoy '.$hoy ,$from,$restaurante->getEmail() ,$html);
                $this->get('qi')->sendMail('Pedidos Listapp de hoy '.$hoy.' para '.$restaurante->getNombre() ,$from,$qi->getSettingDB('mail_recepcion') ,$html);
            }
        }
        return $this->render('ReportesBundle:Default:pedidos_hoy.html.twig', array(
            'html' => $html
        ));
    }

    /**
     * @Route("/mailprueba", name="mailprueba")
     */
    public function mailpruebaAction(Request $request){
        $qi = $this->get('qi');
        $from = $qi->getSettingDB('mail_envio_reservas');
        $interno = "ferney@iridian.co";
        $from="contacto_nat@mail.listapp.com.co";

        $msg = "mail prueba infobip";

        $postdata =array(
            'from' => $from,
            'to' => $interno,
            'replyTo' => $interno,
            'subject' => "mail prueba infobip",
            'html' =>  $msg,
            'intermediateReport'=> 'true'
        );

        $server_output= $qi->send_mail_infoBip($postdata);

        dump($postdata);
        die(dump($server_output));
        return $this->render('ContactoBundle:Default:zonas.html.twig');
    }

    /**
     * @Route("/reporte-pedido-hoy", name="reporte_pedidos_hoy")
     */
    public function reportePedidosHoyAction(Request $request)
    {
        date_default_timezone_set('America/Bogota');
        $hoy = str_replace(' ', '', date('Y-m-d'));
        $pedidos = $this->getDoctrine()->getRepository('ListappBundle:Pedido')->createQueryBuilder("p")
            ->leftJoin("p.plato","pl")
            ->leftJoin("pl.restaurante","r")
            ->leftJoin("p.usuario","u")
            ->where("DATE_FORMAT(p.fecha, '%Y-%m-%d') = '".$hoy."'")
            ->orderBy("r.id","asc")
            ->getQuery()
            ->getResult();

        return $this->render('ReportesBundle:Default:reporte_pedidos_hoy.html.twig', array(
            'pedidos' => $pedidos,
            "hoy"=>$hoy
        ));
    }


    /**
     * @Route("/reporte-total-vendido", name="reporte_total_vendido")
     */
    public function reporteTotalAction(Request $request)
    {
        $reporte = $this->reporteNativo('SELECT SUM(c.total) as precio, " " as fecha FROM compra c WHERE c.eatadocarrito_id = 2 and c.prueba != 0 order by c.created_at;');
        return $this->render('ReportesBundle:Default:reporte.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Total vendido'
        ));
    }

    /**
     * @Route("/reporte-completo", name="reporte_completo")
     */
    public function reporteCompletoAction(Request $request)
    {
        $reporte = $this->reporteNativo('SELECT c.total as precio, c.created_at as fecha FROM compra c  WHERE c.eatadocarrito_id = 2 and c.prueba != 0 order by c.created_at;');
        return $this->render('ReportesBundle:Default:reporte.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte completo'
        ));
    }

    /**
     * @Route("/reporte-diario", name="reporte_diario")
     */
    public function reporteDirarioAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4)
            $rango = 'and c.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $reporte = $this->reporteNativo('SELECT SUM(c.total) as precio, concat(YEAR(c.created_at),"-",MONTH(c.created_at),"-", DAY(c.created_at)) as fecha FROM compra c  WHERE c.eatadocarrito_id = 2 and c.prueba != 0 '.$rango.' group by concat(YEAR(c.created_at),MONTH(c.created_at), DAY(c.created_at)) order by c.created_at');
        return $this->render('ReportesBundle:Default:reporte.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte diario'
        ));
    }

    /**
     * @Route("/reporte-mensual", name="reporte_mensual")
     */
    public function reporteMensualAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4)
            $rango = 'and c.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $reporte = $this->reporteNativo('SELECT SUM(c.total) as precio, concat(YEAR(c.created_at),"-",MONTH(c.created_at)) as fecha FROM compra c  WHERE c.eatadocarrito_id = 2 and c.prueba != 0 '.$rango.' group by concat(YEAR(c.created_at),MONTH(c.created_at)) order by c.created_at');
        return $this->render('ReportesBundle:Default:reporte.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte mensual'
        ));
    }

    /**
     * @Route("/reporte-producto", name="reporte_producto")
     */
    public function reporteProductcAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4)
            $rango = 'and ci.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $cols = array('id','nombre','cantidad');
        $reporte = $this->reporteNativoGen($cols,'select p.id as  id, concat(p.nombre," - ",r.nombre) as nombre, count(p.id) as cantidad from plato p left join restaurante r on p.restaurante_id = r.id left join pedido ci on ci.plato_id = p.id left join compra c on c.id = ci.compra_id WHERE c.eatadocarrito_id = 2 and c.prueba != 0 '.$rango.' group by p.id order by cantidad desc');
        return $this->render('ReportesBundle:Default:reporte_producto.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte por producto',
            'cols'=>$cols
        ));
    }


    /**
     * @Route("/reporte-restaurante", name="reporte_restaurante")
     */
    public function reporteCategoriacAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4)
            $rango = 'and c.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $cols = array('id','nombre','cantidad');
        $reporte = $this->reporteNativoGen($cols,'select ca.id, ca.nombre as nombre, count(ca.id) as cantidad,sum(c.precio) as total from restaurante ca left join plato p on p.restaurante_id = ca.id left join pedido ci on ci.plato_id = p.id left join compra c on c.id = ci.compra_id WHERE c.eatadocarrito_id = 2 and c.prueba != 0'.$rango.' group by ca.id order by total desc');
        return $this->render('ReportesBundle:Default:reporte_producto.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte por categoría',
            'cols'=>$cols
        ));
    }

    /**
     * @Route("/reporte-dia", name="reporte_dia")
     */
    public function reporteDiaAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4 && false)
            $rango = 'and c.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $cols = array('dia','cantidad','precio');
        $reporte = $this->reporteNativoGen($cols,'SELECT sum(c.total) as  precio, c.created_at as fecha, if(weekday(c.created_at) = 0, \'Lunes\', 	if(weekday(c.created_at) = 1,\'Martes\', 		if(weekday(c.created_at) = 2,\'Miercoles\', 			if(weekday(c.created_at) = 3,\'Jueves\', 				if(weekday(c.created_at) = 4,\'Viernes\', 					if(weekday(c.created_at) = 5,\'Sabado\',\'Domingo\')                 )             ) 		)     ) ) as dia, weekday(c.created_at) as dia_num , count(c.id) as cantidad FROM compra c  WHERE c.eatadocarrito_id = 2 and c.prueba != 0 '.$rango.' group by dia order by dia_num asc');
        return $this->render('ReportesBundle:Default:reporte_producto.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte por día',
            'cols'=>$cols
        ));
    }

    /**
     * @Route("/reporte-estados", name="reporte_estado")
     */
    public function reporteEstadoAction(Request $request)
    {
        $start = $request->query->get('start');
        $end = $request->query->get('end');
        $rango = '';
        if(strlen($start) > 4 && strlen($end) > 4)
            $rango = 'and c.created_at BETWEEN "'.$start.' 00:00:00" AND "'.$end.' 23:59:59"';
        $cols = array('nombre','cantidad');
        $reporte = $this->reporteNativoGen($cols,'SELECT count(c.id) as cantidad, e.nombre as nombre from compra c  
          left join estado_carrito e on c.eatadocarrito_id = e.id 
          where  c.prueba != 0 and
            e.id is not null 
            and e.id != 5 
            '.$rango.'
        group by c.eatadocarrito_id 
        order by c.id asc');
        return $this->render('ReportesBundle:Default:reporte_producto.html.twig', array(
            'reporte' => $reporte,
            'titulo'=>'Reporte por estados',
            'cols'=>$cols
        ));
    }

    private function reporteNativo($cadena){
        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('precio', 'precio', 'string');
        $rsm->addScalarResult('fecha', 'fecha', 'string');
        $reporte = $this->getDoctrine()->getManager()->createNativeQuery($cadena, $rsm)->getArrayResult();
        return $reporte;
    }

    private function reporteNativoGen($cols,$cadena){
        $rsm = new ResultSetMapping();
        foreach ($cols as $col){
            $rsm->addScalarResult($col, $col, 'string');
        }
        $reporte = $this->getDoctrine()->getManager()->createNativeQuery($cadena, $rsm)->getArrayResult();
        return $reporte;
    }
}
