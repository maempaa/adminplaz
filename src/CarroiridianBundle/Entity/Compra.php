<?php

namespace CarroiridianBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ListappBundle\Entity\Usuario;
use PagosPayuBundle\Entity\RepuestaPago;

/**
 * Compra
 *
 * @ORM\Table(name="compra")
 * @ORM\Entity(repositoryClass="CarroiridianBundle\Repository\CompraRepository")
 */
class Compra
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Usuario", inversedBy="compras")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @var Compraitem[]
     *
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Compraitem", mappedBy="compra", cascade={"remove"})
     */
    private $compraitems;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Cupon")
     * @ORM\JoinColumn(nullable=false, nullable=true)
     */
    private $cupon;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tarjeta")
     * @ORM\JoinColumn(nullable=true)
     */
    private $tarjeta;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $cuotas;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Plan", inversedBy="compras")
     * @ORM\JoinColumn(nullable=true)
     */
    private $plan;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $inicio;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $fin;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Transaccion")
     * @ORM\JoinColumn(nullable=true)
     */
    private $transaccion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $metodo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid")
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;


    /**
     * @var float
     *
     * @ORM\Column(name="precio", type="float", nullable=true)
     */
    private $precio;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\EstadoCarrito")
     * @ORM\JoinColumn(name="eatadocarrito_id", referencedColumnName="id", nullable=true)
     */
    protected $estado;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prueba", type="boolean")
     */
    private $prueba;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="text", nullable=true)
     */
    private $descripcion;

    /**
 * @var string
 *
 * @ORM\Column(name="message", type="text", nullable=true)
 */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="text", nullable=true)
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Envio")
     * @ORM\JoinColumn(name="direccion_id", referencedColumnName="id", nullable=true)
     */
    protected $direccion;

    /**
     * Items that have been purchased.
     *
     * @var RepuestaPago[]
     * @ORM\OneToMany(targetEntity="PagosPayuBundle\Entity\RepuestaPago", mappedBy="compra", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    protected $RespuestasPago;

    //////////////////////  DATA FOR API PAYU

    /**
     * @var string
     *
     * @ORM\Column(name="cantidad", type="string", nullable=true)
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="string", nullable=true)
     */
    private $total;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="order_id", type="string", nullable=true)
     */
    private $orderId;

    /**
     * @var string
     *
     * @ORM\Column(name="transaction_id", type="string", nullable=true)
     */
    private $transactionId;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_network_response_code", type="string", nullable=true)
     */
    private $paymentNetworkResponseCode;

    /**
     * @var string
     *
     * @ORM\Column(name="trazability_code", type="string", nullable=true)
     */
    private $trazabilityCode;

    /**
     * @var string
     *
     * @ORM\Column(name="authorization_code", type="string", nullable=true)
     */
    private $authorizationCode;

    /**
     * @var string
     *
     * @ORM\Column(name="response_code", type="string", nullable=true)
     */
    private $responseCode;

    /**
     * @var string
     *
     * @ORM\Column(name="operation_date", type="string", nullable=true)
     */
    private $operationDate;

    /**
     * @var string
     *
     * @ORM\Column(name="desc_text", type="string", nullable=true)
     */
    private $descText;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bolsa", type="boolean", nullable=true)
     */
    private $bolsa = false;

    /**
     * @var float
     *
     * @ORM\Column(name="propina", type="float", nullable=true)
     */
    private $propina = 0;



    /**
     * (Initialize some fields).
     */
    public function __construct()
    {
        $this->id = $this->generateId();
        $this->guid = $this->id;
        $this->createdAt = new \DateTime();
        $this->inicio = new \DateTime();

    }

    public function __toString()
    {
        return $this->getId().' ';
    }

    /**
     * @param int $storeId
     *
     * @return string
     */
    public function generateId($storeId = 1)
    {
        return preg_replace('/[^0-9]/i', '', sprintf('%d%d%03d%s', $storeId, date('Y'), date('z'), microtime()));
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return Compra
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Compra
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }



    /**
     * Add compraitem
     *
     * @param \CarroiridianBundle\Entity\Compraitem $compraitem
     *
     * @return Compra
     */
    public function addCompraitem(\CarroiridianBundle\Entity\Compraitem $compraitem)
    {
        $this->compraitems[] = $compraitem;
    
        return $this;
    }

    /**
     * Remove compraitem
     *
     * @param \CarroiridianBundle\Entity\Compraitem $compraitem
     */
    public function removeCompraitem(\CarroiridianBundle\Entity\Compraitem $compraitem)
    {
        $this->compraitems->removeElement($compraitem);
    }

    /**
     * Get compraitems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompraitems()
    {
        return $this->compraitems;
    }

    /**
     * Set bonos
     *
     * @param \CarroiridianBundle\Entity\Bono $bono
     *
     * @return Compra
     */
    public function setBono(\CarroiridianBundle\Entity\Bono $bono = null)
    {
        $this->bono = $bono;

        return $this;
    }

    /**
     * Get bonos
     *
     * @return \CarroiridianBundle\Entity\Bono
     */
    public function getBonos()
    {
        return $this->bonos;
    }


    /**
     * Set prueba
     *
     * @param boolean $prueba
     *
     * @return Compra
     */
    public function setPrueba($prueba)
    {
        $this->prueba = $prueba;
    
        return $this;
    }

    /**
     * Get prueba
     *
     * @return boolean
     */
    public function getPrueba()
    {
        return $this->prueba;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Compra
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set estado
     *
     * @param \CarroiridianBundle\Entity\EstadoCarrito $estado
     *
     * @return Compra
     */
    public function setEstado(\CarroiridianBundle\Entity\EstadoCarrito $estado = null)
    {
        $this->estado = $estado;
    
        return $this;
    }

    /**
     * Get estado
     *
     * @return \CarroiridianBundle\Entity\EstadoCarrito
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set direccion
     *
     * @param \CarroiridianBundle\Entity\Envio $direccion
     *
     * @return Compra
     */
    public function setDireccion(\CarroiridianBundle\Entity\Envio $direccion = null)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return \CarroiridianBundle\Entity\Envio
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Compra
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Get Email
     *
     * @return string
     */
    public function getEmail()
    {
        $user = $this->getDireccion()->getUser();
        return $user->getEmail();
    }

    /**
     * Get Telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        $user = $this->getDireccion()->getUser();
        return $user->getTelefono();
    }

    function getTotalFormat(){
        return '$'.number_format($this->total).' ';
    }

    /**
     * Get UsuarioCompleto
     *
     * @return string
     */
    public function getUsuarioCompleto()
    {
        $user = $this->getUsuario();
        if($user){
            $nombre = $user->getNombre();
            $cad = '<table class="table table-bordered">';
            $cad .= '<tr><td><strong>Nombre</strong></td><td>'.$nombre.'</td></tr>';
            $cad .= '<tr><td><strong>Email</strong></td><td>'.$user->getEmail().'</td></tr>';
            $cad .= '<tr><td><strong>Teléfono</strong></td><td>'.$user->getTelefono().'</td></tr>';
            $cad .= '</table>';
            return $cad;
        }
        return '';

    }

    /**
     * Get DireccionCompleta
     *
     * @return string
     */
    public function getDireccionCompleta()
    {
        $direccion = $this->getDireccion();
        if($direccion){
            $cad = '<table class="table table-bordered">';
            //$cad .= '<tr><td><strong>Deparatamento</strong></td><td>'.$direccion->getDepartamento().'</td></tr>';
            //$cad .= '<tr><td><strong>Ciudad</strong></td><td>'.$direccion->getCiudad().'</td></tr>';
            $cad .= '<tr><td><strong>Dirección</strong></td><td>'.$direccion->getDireccion().'</td></tr>';
            if($direccion->getAdicionales())
            $cad .= '<tr><td><strong>Adicionales</strong></td><td>'.$direccion->getAdicionales().'</td></tr>';
            if($this->getInicio())
                $cad .= '<tr><td><strong>Rango Inicio</strong></td><td>'.$this->getInicio()->format('Y-m-d H:i:s').'</td></tr>';
            if($this->getFin())
                $cad .= '<tr><td><strong>Rango fin</strong></td><td>'.$this->getFin()->format('Y-m-d H:i:s').'</td></tr>';
            $cad .= '</table>';
            return $cad;
        }
        return '';
    }

    /**
     * Get Productos
     *
     * @return string
     */
    public function getProductos()
    {
        $direccion = $this->getDireccion();
        if($direccion || true){
            $cad = '<table class="table table-bordered"><tr><td>Nombre</td><td>Presentación</td><td>Cantidad</td><td>Maduración</td><td>Precio</td></tr>';
            foreach ($this->getCompraitems() as $item){
                $cad .= '<tr><td>'.$item->getProducto()->getNombreEs().'</td><td>'.$item->getTalla().'</td><td>'.$item->getCantidad().'</td><td>'.$item->getMaduracion().'</td><td>$'.number_format($item->getPrecio()).'</td></tr>';
            }
            $cad .= '</table>';
            return $cad;
        }
        return '';
    }

    /**
     * Get Transaccion
     *
     * @return string
     */
    public function getTransaccion()
    {
        $continuar = true;
        $respuestas = $this->getRespuestasPago();
        if($respuestas){
            $cad = '<table class="table table-bordered">';
            foreach ($respuestas as $respuesta){
                if(0){$respuesta = new RepuestaPago();}
                if($respuesta->getTipo() == 'CONFIRMACION' && $continuar){
                    $continuar = false;
                    $cad .= '<tr><td><strong>Id Transacción</strong></td><td>'.$respuesta->getTransactionId().'</td></tr>';
                    $cad .= '<tr><td><strong>Referencia</strong></td><td>'.$respuesta->getReferenceCode().'</td></tr>';
                    $cad .= '<tr><td><strong>Valor</strong></td><td>$'.number_format($respuesta->getTXVALUE()).'</td></tr>';
                    $cad .= '<tr><td><strong>Código Estado</strong></td><td>'.$respuesta->getTransactionState().'</td></tr>';
                    $cad .= '<tr><td><strong>Fecha</strong></td><td>'.$respuesta->getProcessingDate().'</td></tr>';
                }
            }
            $cad .= '</table>';
            return $cad;
        }
        return '';
    }



    /**
     * Add respuestasPago
     *
     * @param \PagosPayuBundle\Entity\RepuestaPago $respuestasPago
     *
     * @return Compra
     */
    public function addRespuestasPago(\PagosPayuBundle\Entity\RepuestaPago $respuestasPago)
    {
        $this->RespuestasPago[] = $respuestasPago;
    
        return $this;
    }

    /**
     * Remove respuestasPago
     *
     * @param \PagosPayuBundle\Entity\RepuestaPago $respuestasPago
     */
    public function removeRespuestasPago(\PagosPayuBundle\Entity\RepuestaPago $respuestasPago)
    {
        $this->RespuestasPago->removeElement($respuestasPago);
    }

    /**
     * Get respuestasPago
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRespuestasPago()
    {
        return $this->RespuestasPago;
    }

    /**
     * Add comprabono
     *
     * @param \CarroiridianBundle\Entity\Comprabono $comprabono
     *
     * @return Compra
     */
    public function addComprabono(\CarroiridianBundle\Entity\Comprabono $comprabono)
    {
        $this->comprabonos[] = $comprabono;

        return $this;
    }

    /**
     * Remove comprabono
     *
     * @param \CarroiridianBundle\Entity\Comprabono $comprabono
     */
    public function removeComprabono(\CarroiridianBundle\Entity\Comprabono $comprabono)
    {
        $this->comprabonos->removeElement($comprabono);
    }

    /**
     * Get comprabonos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComprabonos()
    {
        return $this->comprabonos;
    }

    /**
     * Add bono
     *
     * @param \CarroiridianBundle\Entity\Bono $bono
     *
     * @return Compra
     */
    public function addBono(\CarroiridianBundle\Entity\Bono $bono)
    {
        $this->bonos[] = $bono;

        return $this;
    }

    /**
     * Remove bono
     *
     * @param \CarroiridianBundle\Entity\Bono $bono
     */
    public function removeBono(\CarroiridianBundle\Entity\Bono $bono)
    {
        $this->bonos->removeElement($bono);
    }

    /**
     * Set cantidad.
     *
     * @param string|null $cantidad
     *
     * @return Compra
     */
    public function setCantidad($cantidad = null)
    {
        $this->cantidad = $cantidad;
    
        return $this;
    }

    /**
     * Get cantidad.
     *
     * @return string|null
     */
    public function getCantidad()
    {
        return $this->cantidad;
    }

    /**
     * Set total.
     *
     * @param string|null $total
     *
     * @return Compra
     */
    public function setTotal($total = null)
    {
        $this->total = $total;
    
        return $this;
    }

    /**
     * Get total.
     *
     * @return string|null
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return Compra
     */
    public function setCode($code = null)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set orderId.
     *
     * @param string|null $orderId
     *
     * @return Compra
     */
    public function setOrderId($orderId = null)
    {
        $this->orderId = $orderId;
    
        return $this;
    }

    /**
     * Get orderId.
     *
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set transactionId.
     *
     * @param string|null $transactionId
     *
     * @return Compra
     */
    public function setTransactionId($transactionId = null)
    {
        $this->transactionId = $transactionId;
    
        return $this;
    }

    /**
     * Get transactionId.
     *
     * @return string|null
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set state.
     *
     * @param string|null $state
     *
     * @return Compra
     */
    public function setState($state = null)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state.
     *
     * @return string|null
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set paymentNetworkResponseCode.
     *
     * @param string|null $paymentNetworkResponseCode
     *
     * @return Compra
     */
    public function setPaymentNetworkResponseCode($paymentNetworkResponseCode = null)
    {
        $this->paymentNetworkResponseCode = $paymentNetworkResponseCode;
    
        return $this;
    }

    /**
     * Get paymentNetworkResponseCode.
     *
     * @return string|null
     */
    public function getPaymentNetworkResponseCode()
    {
        return $this->paymentNetworkResponseCode;
    }

    /**
     * Set trazabilityCode.
     *
     * @param string|null $trazabilityCode
     *
     * @return Compra
     */
    public function setTrazabilityCode($trazabilityCode = null)
    {
        $this->trazabilityCode = $trazabilityCode;
    
        return $this;
    }

    /**
     * Get trazabilityCode.
     *
     * @return string|null
     */
    public function getTrazabilityCode()
    {
        return $this->trazabilityCode;
    }

    /**
     * Set authorizationCode.
     *
     * @param string|null $authorizationCode
     *
     * @return Compra
     */
    public function setAuthorizationCode($authorizationCode = null)
    {
        $this->authorizationCode = $authorizationCode;
    
        return $this;
    }

    /**
     * Get authorizationCode.
     *
     * @return string|null
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * Set responseCode.
     *
     * @param string|null $responseCode
     *
     * @return Compra
     */
    public function setResponseCode($responseCode = null)
    {
        $this->responseCode = $responseCode;
    
        return $this;
    }

    /**
     * Get responseCode.
     *
     * @return string|null
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set operationDate.
     *
     * @param string|null $operationDate
     *
     * @return Compra
     */
    public function setOperationDate($operationDate = null)
    {
        $this->operationDate = $operationDate;
    
        return $this;
    }

    /**
     * Get operationDate.
     *
     * @return string|null
     */
    public function getOperationDate()
    {
        return $this->operationDate;
    }

    /**
     * Set descText.
     *
     * @param string|null $descText
     *
     * @return Compra
     */
    public function setDescText($descText = null)
    {
        $this->descText = $descText;
    
        return $this;
    }

    /**
     * Get descText.
     *
     * @return string|null
     */
    public function getDescText()
    {
        return $this->descText;
    }

    /**
     * Set message.
     *
     * @param string|null $message
     *
     * @return Compra
     */
    public function setMessage($message = null)
    {
        $this->message = $message;
    
        return $this;
    }

    /**
     * Get message.
     *
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set date.
     *
     * @param string|null $date
     *
     * @return Compra
     */
    public function setDate($date = null)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date.
     *
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set inicio
     *
     * @param \DateTime $inicio
     *
     * @return Compra
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    
        return $this;
    }

    /**
     * Get inicio
     *
     * @return \DateTime
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Compra
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set usuario
     *
     * @param \ListappBundle\Entity\Usuario $usuario
     *
     * @return Compra
     */
    public function setUsuario(\ListappBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Set plan
     *
     * @param \ListappBundle\Entity\Plan $plan
     *
     * @return Compra
     */
    public function setPlan(\ListappBundle\Entity\Plan $plan)
    {
        $this->plan = $plan;
    
        return $this;
    }

    /**
     * Get plan
     *
     * @return \ListappBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set transaccion
     *
     * @param \ListappBundle\Entity\Transaccion $transaccion
     *
     * @return Compra
     */
    public function setTransaccion(\ListappBundle\Entity\Transaccion $transaccion = null)
    {
        $this->transaccion = $transaccion;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \ListappBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set cuotas
     *
     * @param integer $cuotas
     *
     * @return Compra
     */
    public function setCuotas($cuotas)
    {
        $this->cuotas = $cuotas;
    
        return $this;
    }

    /**
     * Get cuotas
     *
     * @return integer
     */
    public function getCuotas()
    {
        return $this->cuotas;
    }

    /**
     * Set tarjeta
     *
     * @param \AppBundle\Entity\Tarjeta $tarjeta
     *
     * @return Compra
     */
    public function setTarjeta(\AppBundle\Entity\Tarjeta $tarjeta)
    {
        $this->tarjeta = $tarjeta;
    
        return $this;
    }

    /**
     * Get tarjeta
     *
     * @return \AppBundle\Entity\Tarjeta
     */
    public function getTarjeta()
    {
        return $this->tarjeta;
    }

    /**
     * Set cupon
     *
     * @param \CarroiridianBundle\Entity\Cupon $cupon
     *
     * @return Compra
     */
    public function setCupon(\CarroiridianBundle\Entity\Cupon $cupon = null)
    {
        $this->cupon = $cupon;
    
        return $this;
    }

    /**
     * Get cupon
     *
     * @return \CarroiridianBundle\Entity\Cupon
     */
    public function getCupon()
    {
        return $this->cupon;
    }

    /**
     * Set metodo
     *
     * @param string $metodo
     *
     * @return Compra
     */
    public function setMetodo($metodo)
    {
        $this->metodo = $metodo;
    
        return $this;
    }

    /**
     * Get metodo
     *
     * @return string
     */
    public function getMetodo()
    {
        return $this->metodo;
    }

    /**
     * Set bolsa
     *
     * @param boolean $bolsa
     *
     * @return Compra
     */
    public function setBolsa($bolsa)
    {
        $this->bolsa = $bolsa;
    
        return $this;
    }

    /**
     * Get bolsa
     *
     * @return boolean
     */
    public function getBolsa()
    {
        return $this->bolsa;
    }

    /**
     * Set propina
     *
     * @param float $propina
     *
     * @return Compra
     */
    public function setPropina($propina)
    {
        $this->propina = $propina;
    
        return $this;
    }

    /**
     * Get propina
     *
     * @return float
     */
    public function getPropina()
    {
        return $this->propina;
    }

    /**
     * Set fin
     *
     * @param \DateTime $fin
     *
     * @return Compra
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    
        return $this;
    }

    /**
     * Get fin
     *
     * @return \DateTime
     */
    public function getFin()
    {
        return $this->fin;
    }
}
