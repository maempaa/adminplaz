<?php

namespace ListappBundle\Controller;

use Doctrine\ORM\PersistentCollection;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\PropertyAccess\PropertyAccess;

class DefaultController extends Controller
{
    /**
     * @Route("/listapp")
     */
    public function indexAction()
    {
        return $this->render('ListappBundle:Default:index.html.twig');
    }

    /**
     * @Route("/load")
     */
    public function loadAction()
    {
        $textos = $this->getDoctrine()->getRepository('AppBundle:Texto')->findAll();
        $textos_grandes = $this->getDoctrine()->getRepository('AppBundle:TextoBig')->findAll();
        $planes = $this->getDoctrine()->getRepository('ListappBundle:Plan')->findAll();
        $restaurantes = $this->getDoctrine()->getRepository('ListappBundle:Restaurante')->findAll();
        $platos = $this->getDoctrine()->getRepository('ListappBundle:Plato')->findAll();
        foreach (array_merge($textos,$textos_grandes,$planes,$restaurantes,$platos) as $entity){
            $this->get('qi')->saveFire($entity);
        }
        return $this->render('ListappBundle:Default:index.html.twig');
    }




}
