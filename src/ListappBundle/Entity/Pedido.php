<?php

namespace ListappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\PedidoRepository")
 */
class Pedido
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Usuario", inversedBy="pedidos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Plato", inversedBy="pedidos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plato;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Compra", inversedBy="platos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $compra;

    /**
     * @ORM\Column(type="datetime")
     */
    private $fecha;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->id." ";
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Pedido
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    
        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    public function getFechaEs(){
        return $this->getFecha()->format("M d Y, H:i p").'m';
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Pedido
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Pedido
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set usuario
     *
     * @param \ListappBundle\Entity\Usuario $usuario
     *
     * @return Pedido
     */
    public function setUsuario(\ListappBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \ListappBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set plato
     *
     * @param \ListappBundle\Entity\Plato $plato
     *
     * @return Pedido
     */
    public function setPlato(\ListappBundle\Entity\Plato $plato)
    {
        $this->plato = $plato;
    
        return $this;
    }

    /**
     * Get plato
     *
     * @return \ListappBundle\Entity\Plato
     */
    public function getPlato()
    {
        return $this->plato;
    }

    /**
     * Set compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return Pedido
     */
    public function setCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compra = $compra;
    
        return $this;
    }

    /**
     * Get compra
     *
     * @return \CarroiridianBundle\Entity\Compra
     */
    public function getCompra()
    {
        return $this->compra;
    }
}
