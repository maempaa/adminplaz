<?php

namespace ListappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\CalificacionRepository")
 */
class Calificacion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Usuario")
     * @ORM\JoinColumn(nullable=false)
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Plato")
     * @ORM\JoinColumn(nullable=false)
     */
    private $plato;

    /**
     * @ORM\Column(type="float")
     */
    private $valor;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $comentario;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }


    /**
     * Set valor
     *
     * @param float $valor
     *
     * @return Calificacion
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set comentario
     *
     * @param string $comentario
     *
     * @return Calificacion
     */
    public function setComentario($comentario)
    {
        $this->comentario = $comentario;
    
        return $this;
    }

    /**
     * Get comentario
     *
     * @return string
     */
    public function getComentario()
    {
        return $this->comentario;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Calificacion
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Calificacion
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set usuario
     *
     * @param \ListappBundle\Entity\Usuario $usuario
     *
     * @return Calificacion
     */
    public function setUsuario(\ListappBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;
    
        return $this;
    }

    /**
     * Get usuario
     *
     * @return \ListappBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set plato
     *
     * @param \ListappBundle\Entity\Plato $plato
     *
     * @return Calificacion
     */
    public function setPlato(\ListappBundle\Entity\Plato $plato)
    {
        $this->plato = $plato;
    
        return $this;
    }

    /**
     * Get plato
     *
     * @return \ListappBundle\Entity\Plato
     */
    public function getPlato()
    {
        return $this->plato;
    }
}
