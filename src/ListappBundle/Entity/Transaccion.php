<?php

namespace ListappBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\TransaccionRepository")
 */
class Transaccion
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    /**
     * @ORM\Column(type="text")
     */
    private $error;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $orderId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transactionId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentNetworkResponseCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $paymentNetworkResponseErrorMessage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $trazabilityCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorizationCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pendingReason;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $responseCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $errorCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $responseMessage;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transactionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $transactionTime;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $operationDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $extraParameters;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity="CarroiridianBundle\Entity\Compra")
     */
    private $compra;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    public function __construct()
    {
        $this->created_at = new \DateTime();
    }

    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Transaccion
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set error
     *
     * @param string $error
     *
     * @return Transaccion
     */
    public function setError($error)
    {
        $this->error = $error;
    
        return $this;
    }

    /**
     * Get error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return Transaccion
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    
        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set transactionId
     *
     * @param string $transactionId
     *
     * @return Transaccion
     */
    public function setTransactionId($transactionId)
    {
        $this->transactionId = $transactionId;
    
        return $this;
    }

    /**
     * Get transactionId
     *
     * @return string
     */
    public function getTransactionId()
    {
        return $this->transactionId;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Transaccion
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set paymentNetworkResponseCode
     *
     * @param string $paymentNetworkResponseCode
     *
     * @return Transaccion
     */
    public function setPaymentNetworkResponseCode($paymentNetworkResponseCode)
    {
        $this->paymentNetworkResponseCode = $paymentNetworkResponseCode;
    
        return $this;
    }

    /**
     * Get paymentNetworkResponseCode
     *
     * @return string
     */
    public function getPaymentNetworkResponseCode()
    {
        return $this->paymentNetworkResponseCode;
    }

    /**
     * Set paymentNetworkResponseErrorMessage
     *
     * @param string $paymentNetworkResponseErrorMessage
     *
     * @return Transaccion
     */
    public function setPaymentNetworkResponseErrorMessage($paymentNetworkResponseErrorMessage)
    {
        $this->paymentNetworkResponseErrorMessage = $paymentNetworkResponseErrorMessage;
    
        return $this;
    }

    /**
     * Get paymentNetworkResponseErrorMessage
     *
     * @return string
     */
    public function getPaymentNetworkResponseErrorMessage()
    {
        return $this->paymentNetworkResponseErrorMessage;
    }

    /**
     * Set trazabilityCode
     *
     * @param string $trazabilityCode
     *
     * @return Transaccion
     */
    public function setTrazabilityCode($trazabilityCode)
    {
        $this->trazabilityCode = $trazabilityCode;
    
        return $this;
    }

    /**
     * Get trazabilityCode
     *
     * @return string
     */
    public function getTrazabilityCode()
    {
        return $this->trazabilityCode;
    }

    /**
     * Set authorizationCode
     *
     * @param string $authorizationCode
     *
     * @return Transaccion
     */
    public function setAuthorizationCode($authorizationCode)
    {
        $this->authorizationCode = $authorizationCode;
    
        return $this;
    }

    /**
     * Get authorizationCode
     *
     * @return string
     */
    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    /**
     * Set pendingReason
     *
     * @param string $pendingReason
     *
     * @return Transaccion
     */
    public function setPendingReason($pendingReason)
    {
        $this->pendingReason = $pendingReason;
    
        return $this;
    }

    /**
     * Get pendingReason
     *
     * @return string
     */
    public function getPendingReason()
    {
        return $this->pendingReason;
    }

    /**
     * Set responseCode
     *
     * @param string $responseCode
     *
     * @return Transaccion
     */
    public function setResponseCode($responseCode)
    {
        $this->responseCode = $responseCode;
    
        return $this;
    }

    /**
     * Get responseCode
     *
     * @return string
     */
    public function getResponseCode()
    {
        return $this->responseCode;
    }

    /**
     * Set errorCode
     *
     * @param string $errorCode
     *
     * @return Transaccion
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    
        return $this;
    }

    /**
     * Get errorCode
     *
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * Set responseMessage
     *
     * @param string $responseMessage
     *
     * @return Transaccion
     */
    public function setResponseMessage($responseMessage)
    {
        $this->responseMessage = $responseMessage;
    
        return $this;
    }

    /**
     * Get responseMessage
     *
     * @return string
     */
    public function getResponseMessage()
    {
        return $this->responseMessage;
    }

    /**
     * Set transactionDate
     *
     * @param string $transactionDate
     *
     * @return Transaccion
     */
    public function setTransactionDate($transactionDate)
    {
        $this->transactionDate = $transactionDate;
    
        return $this;
    }

    /**
     * Get transactionDate
     *
     * @return string
     */
    public function getTransactionDate()
    {
        return $this->transactionDate;
    }

    /**
     * Set transactionTime
     *
     * @param string $transactionTime
     *
     * @return Transaccion
     */
    public function setTransactionTime($transactionTime)
    {
        $this->transactionTime = $transactionTime;
    
        return $this;
    }

    /**
     * Get transactionTime
     *
     * @return string
     */
    public function getTransactionTime()
    {
        return $this->transactionTime;
    }

    /**
     * Set operationDate
     *
     * @param string $operationDate
     *
     * @return Transaccion
     */
    public function setOperationDate($operationDate)
    {
        $this->operationDate = $operationDate;
    
        return $this;
    }

    /**
     * Get operationDate
     *
     * @return string
     */
    public function getOperationDate()
    {
        return $this->operationDate;
    }

    /**
     * Set extraParameters
     *
     * @param string $extraParameters
     *
     * @return Transaccion
     */
    public function setExtraParameters($extraParameters)
    {
        $this->extraParameters = $extraParameters;
    
        return $this;
    }

    /**
     * Get extraParameters
     *
     * @return string
     */
    public function getExtraParameters()
    {
        return $this->extraParameters;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Transaccion
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Transaccion
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return Transaccion
     */
    public function setCompra(\CarroiridianBundle\Entity\Compra $compra = null)
    {
        $this->compra = $compra;
    
        return $this;
    }

    /**
     * Get compra
     *
     * @return \CarroiridianBundle\Entity\Compra
     */
    public function getCompra()
    {
        return $this->compra;
    }
}
