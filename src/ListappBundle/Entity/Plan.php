<?php

namespace ListappBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="integer")
     */
    private $dias;

    /**
     * @ORM\Column(type="float")
     */
    private $precio;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $precioServicio;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Compra", mappedBy="plan", orphanRemoval=true)
     */
    private $compras;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $precio_texto;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_pedidos;

    public function __construct()
    {
        $this->compras = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nombre;
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Plan
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set dias
     *
     * @param integer $dias
     *
     * @return Plan
     */
    public function setDias($dias)
    {
        $this->dias = $dias;
    
        return $this;
    }

    /**
     * Get dias
     *
     * @return integer
     */
    public function getDias()
    {
        return $this->dias;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Plan
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;
    
        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Plan
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Plan
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Plan
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set precioTexto
     *
     * @param string $precioTexto
     *
     * @return Plan
     */
    public function setPrecioTexto($precioTexto)
    {
        $this->precio_texto = $precioTexto;
    
        return $this;
    }

    /**
     * Get precioTexto
     *
     * @return string
     */
    public function getPrecioTexto()
    {
        return $this->precio_texto;
    }

    /**
     * Set numPedidos
     *
     * @param integer $numPedidos
     *
     * @return Plan
     */
    public function setNumPedidos($numPedidos)
    {
        $this->num_pedidos = $numPedidos;
    
        return $this;
    }

    /**
     * Get numPedidos
     *
     * @return integer
     */
    public function getNumPedidos()
    {
        return $this->num_pedidos;
    }

    /**
     * Add compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return Plan
     */
    public function addCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras[] = $compra;
    
        return $this;
    }

    /**
     * Remove compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     */
    public function removeCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras->removeElement($compra);
    }

    /**
     * Get compras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompras()
    {
        return $this->compras;
    }

    /**
     * Set precioServicio
     *
     * @param float $precioServicio
     *
     * @return Plan
     */
    public function setPrecioServicio($precioServicio)
    {
        $this->precioServicio = $precioServicio;
    
        return $this;
    }

    /**
     * Get precioServicio
     *
     * @return float
     */
    public function getPrecioServicio()
    {
        return $this->precioServicio;
    }
}
