<?php

namespace ListappBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\UsuarioRepository")
 */
class Usuario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $direccion;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $uid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_data;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Tarjeta", mappedBy="usuario", orphanRemoval=true)
     */
    private $tarjetas;

    /**
     * @ORM\OneToMany(targetEntity="ListappBundle\Entity\Pedido", mappedBy="usuario", orphanRemoval=true)
     */
    private $pedidos;

    /**
     * @ORM\OneToMany(targetEntity="CarroiridianBundle\Entity\Compra", mappedBy="usuario", orphanRemoval=true)
     */
    private $compras;



    public function __construct()
    {
        $this->pedidos = new ArrayCollection();
        $this->compras = new ArrayCollection();
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre.' || '.$this->getEmail();
    }



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    
        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set uid
     *
     * @param string $uid
     *
     * @return Usuario
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    
        return $this;
    }

    /**
     * Get uid
     *
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set providerData
     *
     * @param string $providerData
     *
     * @return Usuario
     */
    public function setProviderData($providerData)
    {
        $this->provider_data = $providerData;
    
        return $this;
    }

    /**
     * Get providerData
     *
     * @return string
     */
    public function getProviderData()
    {
        return $this->provider_data;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Usuario
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;
    
        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Add pedido
     *
     * @param \ListappBundle\Entity\Pedido $pedido
     *
     * @return Usuario
     */
    public function addPedido(\ListappBundle\Entity\Pedido $pedido)
    {
        $this->pedidos[] = $pedido;
    
        return $this;
    }

    /**
     * Remove pedido
     *
     * @param \ListappBundle\Entity\Pedido $pedido
     */
    public function removePedido(\ListappBundle\Entity\Pedido $pedido)
    {
        $this->pedidos->removeElement($pedido);
    }

    /**
     * Get pedidos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }

    /**
     * Add compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     *
     * @return Usuario
     */
    public function addCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras[] = $compra;
    
        return $this;
    }

    /**
     * Remove compra
     *
     * @param \CarroiridianBundle\Entity\Compra $compra
     */
    public function removeCompra(\CarroiridianBundle\Entity\Compra $compra)
    {
        $this->compras->removeElement($compra);
    }

    /**
     * Get compras
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompras()
    {
        return $this->compras;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     *
     * @return Usuario
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Add tarjeta
     *
     * @param \AppBundle\Entity\Tarjeta $tarjeta
     *
     * @return Usuario
     */
    public function addTarjeta(\AppBundle\Entity\Tarjeta $tarjeta)
    {
        $this->tarjetas[] = $tarjeta;
    
        return $this;
    }

    /**
     * Remove tarjeta
     *
     * @param \AppBundle\Entity\Tarjeta $tarjeta
     */
    public function removeTarjeta(\AppBundle\Entity\Tarjeta $tarjeta)
    {
        $this->tarjetas->removeElement($tarjeta);
    }

    /**
     * Get tarjetas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTarjetas()
    {
        return $this->tarjetas;
    }
}
