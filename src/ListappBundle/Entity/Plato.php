<?php

namespace ListappBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ListappBundle\Repository\PlatoRepository")
 */
class Plato
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $foto;

    /**
     * @ORM\Column(type="text", length=255,nullable=true)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $calificacion;

    /**
     * @ORM\Column(type="integer")
     */
    private $cantcal = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $orden = 1;

    /**
     * @ORM\Column(type="boolean")
     */
    private $visible = true;

    /**
     * @ORM\Column(type="text")
     */
    private $tags;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Dia")
     * @ORM\JoinColumn(name="dia_id", referencedColumnName="id")
     */
    private $dia;

    /**
     * @ORM\ManyToOne(targetEntity="ListappBundle\Entity\Restaurante", inversedBy="platos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $restaurante;

    /**
     * @ORM\OneToMany(targetEntity="ListappBundle\Entity\Pedido", mappedBy="plato", orphanRemoval=true)
     */
    private $pedidos;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $key_firebase;

    public function __construct()
    {
        $this->pedidos = new ArrayCollection();
        $this->created_at = new \DateTime();
    }

    public function __toString()
    {
        return $this->nombre;
    }

    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Plato
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set foto
     *
     * @param string $foto
     *
     * @return Plato
     */
    public function setFoto($foto)
    {
        $this->foto = $foto;
    
        return $this;
    }

    /**
     * Get foto
     *
     * @return string
     */
    public function getFoto()
    {
        return $this->foto;
    }

    /**
     * Set calificacion
     *
     * @param float $calificacion
     *
     * @return Plato
     */
    public function setCalificacion($calificacion)
    {
        $this->calificacion = $calificacion;
    
        return $this;
    }

    /**
     * Get calificacion
     *
     * @return float
     */
    public function getCalificacion()
    {
        return $this->calificacion;
    }

    /**
     * Set orden
     *
     * @param integer $orden
     *
     * @return Plato
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    
        return $this;
    }

    /**
     * Get orden
     *
     * @return integer
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     *
     * @return Plato
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Plato
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    
        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set keyFirebase
     *
     * @param string $keyFirebase
     *
     * @return Plato
     */
    public function setKeyFirebase($keyFirebase)
    {
        $this->key_firebase = $keyFirebase;
    
        return $this;
    }

    /**
     * Get keyFirebase
     *
     * @return string
     */
    public function getKeyFirebase()
    {
        return $this->key_firebase;
    }

    /**
     * Set restaurante
     *
     * @param \ListappBundle\Entity\Restaurante $restaurante
     *
     * @return Plato
     */
    public function setRestaurante(\ListappBundle\Entity\Restaurante $restaurante)
    {
        $this->restaurante = $restaurante;
    
        return $this;
    }

    /**
     * Get restaurante
     *
     * @return \ListappBundle\Entity\Restaurante
     */
    public function getRestaurante()
    {
        return $this->restaurante;
    }

    /**
     * Add pedido
     *
     * @param \ListappBundle\Entity\Pedido $pedido
     *
     * @return Plato
     */
    public function addPedido(\ListappBundle\Entity\Pedido $pedido)
    {
        $this->pedidos[] = $pedido;
    
        return $this;
    }

    /**
     * Remove pedido
     *
     * @param \ListappBundle\Entity\Pedido $pedido
     */
    public function removePedido(\ListappBundle\Entity\Pedido $pedido)
    {
        $this->pedidos->removeElement($pedido);
    }

    /**
     * Get pedidos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPedidos()
    {
        return $this->pedidos;
    }

    /**
     * Set cantcal
     *
     * @param integer $cantcal
     *
     * @return Plato
     */
    public function setCantcal($cantcal)
    {
        $this->cantcal = $cantcal;
    
        return $this;
    }

    /**
     * Get cantcal
     *
     * @return integer
     */
    public function getCantcal()
    {
        return $this->cantcal;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     *
     * @return Plato
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set dia
     *
     * @param \ListappBundle\Entity\Dia $dia
     *
     * @return Plato
     */
    public function setDia(\ListappBundle\Entity\Dia $dia)
    {
        $this->dia = $dia;
    
        return $this;
    }

    /**
     * Get dia
     *
     * @return \ListappBundle\Entity\Dia
     */
    public function getDia()
    {
        return $this->dia;
    }
}
