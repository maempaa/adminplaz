<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\Tarjeta;
use CarroiridianBundle\Entity\Compra;
use CarroiridianBundle\Entity\Compraitem;
use CarroiridianBundle\Entity\Envio;
use ListappBundle\Entity\Calificacion;
use ListappBundle\Entity\Pedido;
use ListappBundle\Entity\Usuario;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DefaultController extends Controller
{
    /**
     * @Route("/api/home")
     */
    public function indexAction()
    {
        return $this->render('ApiBundle:Default:index.html.twig');
    }



    /**
     * Retorna todas los rangos de edad
     * @Route("/api/test")
     * @Method({"GET"})
     */
    public function testAction()
    {
        $serializer = $this->get('jms_serializer');
        $qi= $this->get("qi");
        $resultado= array("ok"=>true);

        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
    }

    /**
     * Retorna si un código es válido o no y su id
     * @Route("/api/cupon/{email}/{codigo}")
     * @Method({"GET"})
     */
    public function codigoAction($email,$codigo)
    {
        $serializer = $this->get('jms_serializer');

        $user= $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('email'=>$email));
        if($user == null){
            $resultado= array("ok"=>false,"error_message"=>"Usuario inválido");
        }else{
            $cupon = $this->getDoctrine()->getRepository('CarroiridianBundle:Cupon')->findOneBy(array('codigo'=>$codigo));
            if($user == null){
                $resultado= array("ok"=>false,"error_message"=>"Código inválido");
            }else{
                $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->findOneBy(array('usuario'=>$user,'cupon'=>$cupon));
                if($compra){
                    $resultado= array("ok"=>false,"error_message"=>"Cupón usado anteriormente");
                }else{
                    $resultado= array("ok"=>true,"cupon_id"=>$cupon->getId(),'valor'=>$cupon->getValor(),'porcentaje'=>$cupon->getPorcentaje());
                }
            }
        }

        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
    }

    /**
     * Retorna la  del plan
     * @Route("/api/vigencia/{email}")
     * @Method({"GET"})
     */
    public function vigenciaAction($email)
    {
        $serializer = $this->get('jms_serializer');
        $qi= $this->get("qi");
        $resultado= array("ok"=>false,"listapps_restantes"=>0,"dias"=>0);

        $user= $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('email'=>$email));
        $estado= $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>"APPROVED"));
        $compraAnt = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->findOneBy(array('usuario'=>$user,'estado'=>$estado),array('id'=>'desc'));
        if($compraAnt != null) {

            $pedidos = $this->getDoctrine()->getRepository('ListappBundle:Pedido')->findBy(array('usuario' => $user, 'compra' => $compraAnt));
            $plana=$compraAnt->getPlan();
            $now = time(); // or your date as well
            $your_date = $compraAnt->getCreatedAt()->getTimestamp();
            $datediff = $now - $your_date;
            $dias=round($datediff / (60 * 60 * 24));
            if($dias < $plana->getDias() && count($pedidos) < $plana->getNumPedidos()){
                $resultado = array("ok" => true, "error_msg" => $plana->getId(), "status" => 2,"mes"=>$plana->getNumPedidos(),"inicio"=>$compraAnt->getInicio(),"dias"=>$plana->getDias()-$dias,"listapps_restantes"=>$plana->getNumPedidos()-count($pedidos));
                $serializer = $this->get('jms_serializer');
                $json = $serializer->serialize($resultado,'json');
                return new Response($json);
            }

        }

        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
    }

    /**
     * Adicionar un tarjeta a un usuario dado
     * @Route("/api/agregar-tarjeta")
     * @Method({"POST"})
     */
    public function agregarTarjetaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $content = $request->getContent();
        $request_get = json_decode($content, true);

        $titular        = $request_get['titular'];
        $numDoc         = $request_get['numDoc'];
        $numeroTarjeta  = $request_get['numeroTarjeta'];
        $cvv            = $request_get['cvv'];
        $fecha          = $request_get['fecha'];
        $uid            = $request_get['uid'];
        $tipoTarjeta    = $request_get['tipoTarjeta'];
        $tipoDoc        = $request_get['tipoDoc'];

        $datos_payu = $this->getDoctrine()->getRepository('PagosPayuBundle:DatosPayu')->findOneBy(array("origen"=>"app"));
        $user = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('uid'=>$uid));
        if($user == null){
            //ERROR
            $resultado= array("ok"=>false,"error_msg"=>"Usuario inválido");
        }else{
            try{
                $nueva = new Tarjeta();
                $nueva->setFecha(str_replace("-","/", $fecha));
                $nueva->setCvv($cvv);
                $nueva->setNumDoc($numDoc);
                $nueva->setNumeroTarjeta(substr($numeroTarjeta,-4));
                $nueva->setUltimos( substr($numeroTarjeta,-4));
                $nueva->setTipoDoc($tipoDoc);
                $nueva->setTipoTarjeta($tipoTarjeta);
                $nueva->setTitular($titular);
                $nueva->setEmail($user->getEmail());
                $nueva->setUsuario($user);


                $apiKey = $datos_payu->getApiKey();
                $apiLogin= $datos_payu->getApiLogin();
                $url = $datos_payu->getUrlProduccion();
                if($datos_payu->getTest()){
                    $url = 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi';
                }
                $fields = array(
                    'language' => "es",
                    'command' => "CREATE_TOKEN",
                    'merchant' => [
                        "apiLogin" => $apiLogin,//"tI7P45k3FawXHpG",
                        "apiKey" => $apiKey//"23115978"
                    ],
                    'creditCardToken' => [
                        "payerId" => $uid,
                        "name" => $nueva->getTitular(),
                        "identificationNumber" => $nueva->getNumDoc(),
                        "paymentMethod" => $nueva->getTipoTarjeta(),
                        "number" => $numeroTarjeta,
                        "expirationDate" => $nueva->getFecha()
                    ]
                );

                $data_string = json_encode($fields);
                //dump($data_string);
                $ch = curl_init($url);
                //dump($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Content-Type: application/json',
                        'Content-Length: ' . strlen($data_string))
                );
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)');
                //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

                $result = curl_exec($ch);
                curl_close($ch);
                $serializer = SerializerBuilder::create()->build();

                //die(dump($result));

                $xml = new \SimpleXMLElement($result);

                if($xml->error){
                    $resultado= array("ok"=>false,"error_msg"=>$xml->error->__toString(),'pos'=>1);

                }else {
                    $nueva->setCode($xml->code->__toString());
                    $nueva->setCreditCardTokenId($xml->creditCardToken->creditCardTokenId->__toString());
                    $nueva->setCvv(null);

                    $em->persist($nueva);
                    $em->flush();
                    $this->get('qi')->saveFireByUid($nueva);
                    $nueva->setEmail(null);
                    $resultado= array("ok"=>true,"error_msg"=>"","tarjeta"=>$nueva);
                }

            }catch (\Exception $e){
                $resultado = array("ok"=>false,"error_msg"=>$e->getMessage(),'pos'=>2);

            }
        }
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
    }

    /**
     * pagar una factura
     * @Route("/api/secure/pagar")
     * @Method({"POST"})
     */
    public function pagarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $token      = $request->get('tarjeta');
        $email     = $request->get('email');
        $total      = $request->get('total');
        $tax      = $request->get('impuesto');
        $plan_id      = $request->get('plan');
        $cuotas      = $request->get('cuotas_old');
        $web      = $request->get('web',"");
        $cupon_id      = $request->get('web',"");

        $request_get = [];
        if ( $web == "") {
            $content = $request->getContent();
            $request_get = json_decode($content, true);
            $token      = $request_get['tarjeta'];
            $email     	= $request_get['email'];
            $total      = $request_get['total'];
            $tax      	= $request_get['impuesto'];
            $plan_id    = $request_get['plan'];
            $cuotas     = $request_get['cuotas_old'];
            $cupon_id      	= $request_get['cupon_id'];
        }

        return $this->pago($em,$token,$email,$total,$tax,$plan_id,$cuotas,$web, null, $cupon_id);
    }





    /**
     * pagar una factura
     * @Route("/api/secure/pedido")
     * @Method({"POST"})
     */
    public function pedidoAction(Request $request)
    {
        /* @var $em EntityManager */
        $em = $this->getDoctrine()->getManager();
        $qi = $this->get('qi');


        $content = $request->getContent();
        $request_get = json_decode($content, true);
        $fecha       = $request_get['fechaSolicitud'];
        $uid         = $request_get['uid'];
        $direccionId = $request_get['idDireccion'];
        $tarjetaId    = $request_get['tarjetaId'];
        $comentarios    = $request_get['comentarios'];
        $productos = $request_get['productos'];

        $metodoId    = $request_get['metodoPago'];
        $inicio    = $request_get['horarioRangoIni'];
        $fin    = $request_get['horarioRangoFin'];
        $propina    = $request_get['propina'];
        $bolsa    = $request_get['bolsa'];
        $total  = $request_get['total'];
        $subtotal = $request_get['subtotal'];
        $cupon_id = $request_get['cupon_id'];
        $cvv = $request_get['cvv'];
        $metodos = array('1'=>'EFECTIVO','2'=>'TARJETA','3'=>'DATÁFONO');



        $user  = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('uid'=>$uid));
        $now = time(); // or your date as well

        if($user == null ){
            $resultado= array("ok"=>false,"error_msg"=>"Lo sentimos, usuario inválido","status"=>1);
        }else{
            $direccion = $this->getDoctrine()->getRepository('CarroiridianBundle:Envio')->findOneBy(array('usuario'=>$user,'id'=>$direccionId));
            if($direccion == null){
                $resultado= array("ok"=>false,"error_msg"=>"Lo sentimos, dirección inválida","status"=>2);
            }else{
                $tarjeta = $this->getDoctrine()->getRepository('AppBundle:Tarjeta')->findOneBy(array('usuario'=>$user,'id'=>$tarjetaId));
                if($tarjeta == null && is_numeric($tarjetaId)){
                    $resultado= array("ok"=>false,"error_msg"=>"Lo sentimos, la tarjeta es inválida","status"=>3);
                }else{
                    //$total = $qi->getSettingDB("valor_domicilio");
                    $total = 0;

                    $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>'APPROVED'));
                    $compra = new Compra();
                    $compra->setUsuario($user);
                    $compra->setMessage($comentarios);
                    $compra->setEstado($estado);
                    $compra->setPrueba(true);
                    $compra->setDireccion($direccion);
                    $compra->setMetodo($metodos[$metodoId]);
                    $compra->setPropina($propina);
                    $compra->setBolsa(false);
                    $domicilio = $qi->getSettingDB('valor_domicilio');
                    $total += $domicilio;
                    if($bolsa == '1'){
                        $precio_bolsa = $qi->getSettingDB('valor_bolsa');
                        $total += $precio_bolsa;
                        $compra->setBolsa(true);
                    }
                    if(strlen($inicio) > 0){
                        $inicio = new \DateTime($inicio);
                        $compra->setInicio($inicio);
                    }
                    if(strlen($fin) > 0){
                        $fin = new \DateTime($fin);
                        $compra->setFin($fin);
                    }
                    if($propina > 0){
                        $total += $propina;
                    }

                    $em->persist($compra);
                    $em->flush();
                    $html_prods = '<table border="0" style="width: 780px; text-align: center; border-collapse: collapse"><tbody>';
                    $html_prods .= '<tr style=" font-weight: bolder"> <td style="width: 130px; height: 50px">&nbsp;</td> <td colspan="4">&nbsp;</td> <td style="width: 130px">&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td colspan="4" style="text-align: left; "> <h2 style="padding: 1% 3%; background-color: #5d4bff; display: inline-block; font-size: 12pt; font-weight: 300; color: white; text-align: left; border-radius: 5px">Resumen</h2> </td> <td>&nbsp;</td> </tr>';
                    $html_prods .= '<tr style="font-weight: bolder"> <td style="width: 130px; height: 25px; font-weight: bolder">&nbsp;</td> <td style="width: 130px; font-weight: bolder">Producto</td> <td style="width: 130px; font-weight: bolder">Cantidad</td> <td style="width: 130px; font-weight: bolder">Especificaciones</td> <td style="width: 130px; font-weight: bolder">Total</td> <td style="width: 130px">&nbsp;</td> </tr> <tr> <td>&nbsp;</td> <td colspan="4" style="height: 6px; background-color: #e6e7e8">&nbsp;</td> <td>&nbsp;</td> </tr>';
                    foreach ($productos as $grupo){
                        $prod_id = $grupo['id'];
                        $talla_id = $grupo['unitcart'];
                        $cant = $grupo['numcart'];
                        $maduracion_id = 0;
                        if(array_key_exists('maduracion_id',$grupo))
                            $maduracion_id = $grupo['maduracion_id'];
                        $producto = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto')->find($prod_id);
                        $talla = $this->getDoctrine()->getRepository('CarroiridianBundle:Talla')->find($talla_id);
                        $maduracion = $this->getDoctrine()->getRepository('AppBundle:Maduracion')->find($maduracion_id);
                        $inventario = $this->getDoctrine()->getRepository('CarroiridianBundle:Inventario')->findOneBy(array('producto'=>$producto,'talla'=>$talla));
                        $precio = $inventario->getPrecio()*$cant;
                        $compraitem = new Compraitem();
                        $compraitem->setProducto($producto);
                        $compraitem->setTalla($talla);
                        $compraitem->setCantidad($cant);
                        $compraitem->setPrecio($precio);
                        $compraitem->setCompra($compra);
                        $compraitem->setMaduracion($maduracion);
                        $em->persist($compraitem);
                        $em->flush();
                        $qi->saveFire($compraitem);
                        $compra->addCompraitem($compraitem);
                        $total += $precio;
                        $html_prods .= '<tr style=""> <td style="width: 130px; height: 25px">&nbsp;</td> <td style="width: 130px; font-weight: bold">'.$producto->getNombreEs().'</td> <td style="width: 130px; color: #5d4bff; font-weight: bold">'.$cant.'</td> <td style="width: 130px; color: #5d4bff; font-weight: bold">'.$maduracion->getNombre().'</td> <td style="width: 130px">COP $'.number_format($precio).'</td> <td style="width: 130px">&nbsp;</td> </tr>';
                    }
                    $html_prods .= '</tbody></table>';



                    $resultado= array("ok"=>true,"error_msg"=>"","status"=>4);
                    if($metodoId == 2){
                        list($res,$error) = $this->pago($em, $tarjeta, $user,$total,0.19*$total/1.19,$cvv,$compra,null);
                        if($res == null || $error != ''){
                            $resultado = array("ok"=>false,"error_msg"=>"Error en tarjeta: ".$error,"status"=>5);
                        }
                        if($res){
                            if($res->getRef() == 'DECLINED'){
                                $resultado = array("ok"=>false,"error_msg"=>"Transacción rechazada".$error,"status"=>6);
                            }else{
                                $resultado = array("ok"=>true,"error_msg"=>"Transacción aprobada".$error,"status"=>7);
                            }
                            $compra->setEstado($res);
                        }
                    }else{
                        $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>'CONTRA_ENTREGA'));
                        $compra->setEstado($estado);
                        $this->get('ci')->descontarInventarios($compra);
                    }

                    //$domicilio = $qi->getSettingDB('valor_domicilio');
                    $compra->setTotal($total);
                    $qi->saveFire($compra);
                    $em->flush();


                    $html = $qi->getTextoBigDB('mail_pedido');
                    $html = str_replace(['%productos%','%subtotal%','%total%','%envio%','%nombre%','%direccion%','%inicio%','%fin%','%metodo%',"%bolsa%","%propina%","%descripcion%"]
                        ,[
                            $html_prods,number_format($total),number_format($total + $domicilio),number_format($domicilio),$user->getNombre(),$direccion->getDireccion(),$inicio->format('d-m-Y H:i'),$fin->format('d-m-Y  H:i'),$metodos[$metodoId],
                            $compra->getBolsa()?'Si':'No',number_format($propina),$comentarios
                        ]
                        ,$html);
                    if($estado != null && $estado->getRef() != 'DECLINED'){
                        $qi->sendMailIB('Pedido en plaz',$user->getEmail(),$html);
                    }


                }
            }
        }

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
    }





    /**
     * @Route("/api/terminos")
     */
    public function terminosAction()
    {

        $qi=$this->get("qi");


        $resultado = array("ok" => true, "error_msg" => "", "terminos" => $qi->getTextoBigDB("terminos"));

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Route("/api/precios/{email}")
     */
    public function preciosAction($email)
    {

        $qi=$this->get("qi");
        $user  = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('email'=>$email));
        $estado= $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>"APPROVED"));
        $compra = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->findOneBy(array('usuario'=>$user,'estado'=>$estado),array('id'=>'desc'));


        $resultado = array("ok" => true, "error_msg" => "", "servicio"=>$qi->getSettingDB("servicio_listapp"),"fee"=>$qi->getSettingDB("fee_activacion"));
        if($compra != null ) {
            $resultado = array("ok" => true, "error_msg" => "", "servicio"=>$qi->getSettingDB("servicio_listapp"),"fee"=>0);
        }


        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }


    /**
     * @param $em
     * @param Tarjeta $tarjeta
     * @param Usuario $user
     * @param $total
     * @param $tax
     * @param $cuotas
     * @param Compra $compra
     * @param null $cvv
     * @param null $cupon_id
     * @return array
     * @throws \Exception
     */
    private function pago($em, Tarjeta $tarjeta, Usuario $user, $total, $tax, $cuotas,$compra,$cvv = null)
    {

        $deviceSessionId=uniqid();
        $ip =$_SERVER['REMOTE_ADDR'];
        $agent=$_SERVER['HTTP_USER_AGENT'];
        $payu = $this->getDoctrine()->getRepository('PagosPayuBundle:DatosPayu')->findOneBy(array("origen"=>"app"));


        $referenceCode = 'Plaz_'.$compra->getId();
        $taxReturnBase = number_format($total - $tax,2, '.', '');
        $tax = number_format($tax,2, '.', '');
        $total = number_format($total,2, '.', '');


        $sign = md5($payu->getApiKey() . "~" . $payu->getMerchantId() . "~" . $referenceCode . "~" . $total . "~" . $payu->getCurrency());
        $url = $payu->getUrlProduccion();
        if($payu->getTest()){
            $url = 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi';
        }
        //dump(1);
        try {
            $urlConfirmacion = $this->generateUrl(
                'pagar_payu_confirmacion', array(), UrlGeneratorInterface::ABSOLUTE_URL
            );
        }catch (\Exception $e) {
            //dump(2);
        }


        $fields = array(
            'language' => "es",
            'command' => "SUBMIT_TRANSACTION",
            'merchant' => [
                "apiLogin" => $payu->getApiLogin(),//"tI7P45k3FawXHpG",
                "apiKey" => $payu->getApiKey()//"23115978"
            ],
            'transaction' => [
                "order" => [
                    "accountId" => $payu->getAccountId(),
                    "referenceCode" => $referenceCode,
                    "description" => 'Compra en plaz',
                    "language" => "es",
                    "signature" => $sign,
                    "notifyUrl" =>  $urlConfirmacion,
                    "additionalValues" => [
                        "TX_VALUE" => [
                            "value" => $total,
                            "currency" => $payu->getCurrency()
                        ],
                        "TX_TAX" => [
                            "value" => $tax,
                            "currency" => $payu->getCurrency()
                        ],
                        "TX_TAX_RETURN_BASE" => [
                            "value" => $taxReturnBase,
                            "currency" => $payu->getCurrency()
                        ]
                    ],
                    "buyer"=> [
                        "merchantBuyerId" => (string)$user->getId(),
                        "fullName" => $user->getNombre() ,
                        "emailAddress" => $user->getEmail(),
                        "contactPhone" => $user->getTelefono(),
                        "dniNumber" => $tarjeta->getNumDoc(),
                        "shippingAddress" => [
                            "street1" => "",
                            "street2" => "",
                            "city" => "Bogota",
                            "state" => "Bogota DC",
                            "country" => "CO",
                            "postalCode" => "000000",
                            "phone" => $user->getTelefono()
                        ]
                    ],
                    "shippingAddress" => [
                        "street1" => "",
                        "street2" => "",
                        "city" => "Bogota",
                        "state" => "Bogota DC",
                        "country" => "CO",
                        "postalCode" => "000000",
                        "phone" => $user->getTelefono()
                    ]
                ],
                "payer" => [
                    "merchantPayerId" => (string)$tarjeta->getId(),
                    "fullName" => $tarjeta->getTitular(),
                    "emailAddress" => $user->getEmail(),
                    "contactPhone" => $user->getTelefono(),
                    "dniNumber" => $tarjeta->getNumDoc(),
                    "billingAddress" => [
                        "street1" => $user->getDireccion(),
                        "street2" => "",
                        "city" => "Bogota",
                        "state" => "Bogota DC",
                        "country" => "CO",
                        "postalCode" => "11001",
                        "phone" => $user->getTelefono()
                    ]
                ],
                "creditCardTokenId" => $tarjeta->getCreditCardTokenId(),
                "creditCard"=> [
                    "securityCode"=> $cvv
                  ],
                "extraParameters" => [
                    "INSTALLMENTS_NUMBER" => (int)$cuotas
                ],
                "type" => "AUTHORIZATION_AND_CAPTURE",
                "paymentMethod" => $tarjeta->getTipoTarjeta(),
                "paymentCountry" => "CO",
                "deviceSessionId" => $deviceSessionId,
                "ipAddress" => $ip,
                "cookie" => uniqid(),
                "userAgent" => $agent
            ],
            "test" => $payu->getTest()
        );

        if(!$cvv)
            unset($fields["transaction"]["creditCard"]);
        unset($fields["transaction"]["cookie"]);
        unset($fields["transaction"]["deviceSessionId"]);
        unset($fields["transaction"]["userAgent"]);
        //die(dump($fields));
        //dump($fields);
        //dump($url);

        $data_string = json_encode($fields);
        //die(dump($fields));
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; MSIE 9.0; WIndows NT 9.0; en-US)');
        //curl_setopt($ch, CURLOPT_SSLVERSION,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

        $result = curl_exec($ch);

        //dump($result);
        //die();
        if (FALSE === $result)
            throw new \Exception(curl_error($ch), curl_errno($ch));

        curl_close($ch);

        $xml = new \SimpleXMLElement($result);
        $error = '';
        if($xml->error){
            $resultado = array("ok"=>false,"error_msg"=>$xml->error->__toString());
            $error = $xml->error->__toString();
            $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>$xml->transactionResponse->state));
            $compra->setEstado($estado);
            $em->persist($compra);
            $em->flush();

        }else {
            if($xml->transactionResponse->state != 'APPROVED') {
                $resultado = array("ok" => false, "error_msg" => "Transacción en estado ".$xml->transactionResponse->state, "compra" => $compra->getId());
                $estado= $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>$xml->transactionResponse->state));
                $compra->setEstado($estado);
                $em->persist($compra);
                $em->flush();
            }else {
                $resultado = array("ok" => true, "error_msg" => "", "compra" => $compra->getId());
                $estado = $this->getDoctrine()->getRepository('CarroiridianBundle:EstadoCarrito')->findOneBy(array('ref'=>$xml->transactionResponse->state));
                $compra->setEstado($estado);
                $qi = $this->get('qi');

                /*
                $asunto = 'Bienvenido Listapper';
                $html = str_replace(array('[tipo]'),array($compra->getPlan()->getNombre()),$qi->getTextoBigDB('mail-compra-plan'));
                $to = $compra->getUsuario()->getEmail();
                $qi->sendMailIB($asunto,$to,$html);
                */

                $em->persist($compra);
                $em->flush();


            }
        }
        $this->get('qi')->saveFire($compra);
        return array($estado,$error);

        /*
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);
        */
    }





    /**
     * pagar una factura
     * @Route("/api/secure/calificar")
     * @Method({"POST"})
     */
    public function calificarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $email = $request->get('email');
        $plato_id = $request->get('plato_id');
        $valor = $request->get('valor');

        $request_get = [];
        if ($content = $request->getContent()) {
            $request_get = json_decode($content, true);
            $email     	= $request_get['email'];
            $plato_id     	= $request_get['plato_id'];
            $valor     	= $request_get['valor'];
        }
        $usuario = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('email'=>$email));
        if($usuario == null) {
            $resultado = array("ok" => false, "error_msg" => 'Usuario inválido', 'pos' => 0, 'show_error' => false);
            $serializer = $this->get('jms_serializer');
            $json = $serializer->serialize($resultado, 'json');
            return new Response($json);
        }else{
            $plato = $this->getDoctrine()->getRepository('ListappBundle:Plato')->find($plato_id);
            $calificacion = new Calificacion();
            $calificacion->setUsuario($usuario);
            $calificacion->setValor($valor);
            $calificacion->setPlato($plato);

            $cant = $plato->getCantcal();
            if(!is_numeric($cant))
                $cant = 0;
            $newcal = $plato->getCalificacion()*($cant)/($cant+1) + (int)$valor/($cant+1);
            $plato->setCalificacion($newcal);
            $plato->setCantcal($cant+1);

            $em->persist($calificacion);
            $em->persist($plato);
            $em->flush();
            $this->get('qi')->saveFire($calificacion);
            $this->get('qi')->saveFire($plato);



            $resultado= array("ok"=>true,"error_msg"=>'','pos'=>1,'show_error'=>false);
            $serializer = $this->get('jms_serializer');
            $json = $serializer->serialize($resultado,'json');
            return new Response($json);
        }
    }

    /**
     * pagar una factura
     * @Route("/api/registrar-usuario")
     * @Method({"POST"})
     */
    public function registrarUsuarioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $request_get = [];
        if ($content = $request->getContent()) {
            $request_get = json_decode($content, true);
            $email     	= $request_get['email'];
            $image     	= $request_get['image'];
            $name     	= $request_get['name'];
            $provider   = $request_get['provider'];
            $uid     	= $request_get['uid'];
        }
        $usuario = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('email'=>$email));
        if($usuario == null) {
            $usuario = new Usuario();
            $usuario->setEmail($email);
            //$usuario->setImage($eimage);
            $usuario->setNombre($name);
            $usuario->setProviderData($provider);
            $usuario->setUid($uid);
            $em->persist($usuario);
            $em->flush();
            $this->get('qi')->saveFire($usuario);

            $qi = $this->get('qi');
            $from = $qi->getSettingDB('mail_envio');
            $to = $qi->getSettingDB('mail_recepcion');
            $message = $qi->getTextoBigDB('mail_registro');
            $message = str_replace(array('%nombre%','%email%'),array($name,$email),$message);
            $qi->sendMailIB($qi->getTextoDB('mail_registro'), $email, $message);

            $resultado = array("ok" => true, "error_msg" => 'Usuario creado', 'pos' => 0, 'show_error' => false);
            $serializer = $this->get('jms_serializer');
            $json = $serializer->serialize($resultado, 'json');
            return new Response($json);
        }else{
            //$usuario->setImage($eimage);
            $usuario->setNombre($name);
            $usuario->setUid($uid);
            $em->persist($usuario);
            $em->flush();
            $this->get('qi')->saveFire($usuario);

            $resultado = array("ok" => true, "error_msg" => 'Usuario actualizado', 'pos' => 0, 'show_error' => false);
            $serializer = $this->get('jms_serializer');
            $json = $serializer->serialize($resultado,'json');
            return new Response($json);
        }
    }

    /**
     * update user
     * @Route("/api/update-usuario")
     * @Method({"POST"})
     */
    public function updateUsuarioAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qi = $this->get('qi');

        $content = $request->getContent();
        $request_get = json_decode($content, true);
        $uid = $request_get["uid"];

        $user  = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('uid'=>$uid));
        if($user){
            $user->setNombre($request_get["name"]);
            $user->setTelefono($request_get["telefono"]);
            $em->persist($user);
            $em->flush();
            $qi->saveFire($user);
            $resultado = array("ok" => true, "error_msg" => "Usuario actualizado", "status" => 1,"user_id"=>$user->getUid(),"code"=>"user_updated");

        }else{
            $resultado = array("ok" => false, "error_msg" => "No existe el usuario", "status" => 3,"code"=>"user_not_exists");

        }

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * pagar una factura
     * @Route("/api/add-direccion")
     * @Method({"POST"})
     */
    public function addDireccionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qi = $this->get('qi');

        $content = $request->getContent();
        $request_get = json_decode($content, true);
        $uid = $request_get["uid"];
        $user  = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('uid'=>$uid));
        if(!$user){
            $resultado = array("ok" => false, "error_msg" => "Lo sentimos, debes estar registrado.", "status" => 1,"code"=>"not_authenticated");
        }else{
            $existe = $this->getDoctrine()->getRepository("CarroiridianBundle:Envio")->findOneBy(array("usuario"=>$user,"direccion"=>$request_get["dir"],"adicionales"=>$request_get["adicional"]));
            if($existe){
                $resultado = array("ok" => false, "error_msg" => "Esta dirección ya esta registrada", "status" => 1,"code"=>"dir_already_exists");
            }else{
                $nueva = new Envio();
                $nueva->setDireccion($request_get["dir"]);
                $nueva->setAdicionales($request_get["adicional"]);
                $nueva->setLat($request_get["lat"]);
                $nueva->setLng($request_get["lng"]);
                $nueva->setUsuario($user);
                $nueva->setUid($request_get["uid"]);
                if ($request_get["predeterminada"]) {
                    $nueva->setPredeterminada(true);
                    $prede = $this->getDoctrine()->getRepository("CarroiridianBundle:Envio")->findOneBy(array("usuario" => $user, "predeterminada" => true));
                    if ($prede) {
                        $prede->setPredeterminada(false);
                        $em->persist($prede);
                        $em->flush();
                        $qi->saveFire($prede);
                    }
                }
                $em->persist($nueva);
                $em->flush();
                $qi->saveFire($nueva);

                $resultado = array("ok" => true, "error_msg" => "Dirección agregada", "status" => 3, "dir_id" => $nueva->getId(), "code" => "direccion_creada");
            }

        }
        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Route("/api/remove-direccion")
     * @Method({"POST"})
     */
    public function removeDireccionAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qi = $this->get('qi');

        $content = $request->getContent();
        $request_get = json_decode($content, true);
        $uid = $request_get["uid"];
        $id = $request_get["id"];

        $direccion = $this->getDoctrine()->getRepository("CarroiridianBundle:Envio")->findOneBy(array('uid'=>$uid,'id'=>$id));
        if($direccion){
            $compras = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->createQueryBuilder('c')->where('c.direccion = '.$id)->select('c.id')->getQuery()->getArrayResult();
            if(count($compras) > 0){
                $resultado = array("ok" => false, "error_msg" => "Lo sentimos, la dirección está asociada a compras anteriores y no la podemos borrar", "status" => 4,"code"=>"dir_sell");
            }else{
                $qi->removeFire($direccion);
                $em->remove($direccion);
                $em->flush();
                $resultado = array("ok" => true, "error_msg" => "Dirección eliminada", "status" => 3,"dir_id"=>$direccion->getId(),"code"=>"direccion_eliminada");
            }

        }else{
            $resultado = array("ok" => false, "error_msg" => "No existe la dirección", "status" => 1,"code"=>"dir_not_exists");

        }

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }

    /**
     * @Route("/api/remove-tarjeta")
     * @Method({"POST"})
     */
    public function removeTarjetaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $qi = $this->get('qi');

        $content = $request->getContent();
        $request_get = json_decode($content, true);
        $uid = $request_get["uid"];
        $id = $request_get["id"];

        $usuario = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->findOneBy(array('uid'=>$uid));
        //dump($usuario);
        //dump($id);
        $tarjeta = $this->getDoctrine()->getRepository("AppBundle:Tarjeta")->findOneBy(array('usuario'=>$usuario,'id'=>$id));
        //die(dump($tarjeta));
        if($tarjeta){
            $compras = $this->getDoctrine()->getRepository('CarroiridianBundle:Compra')->createQueryBuilder('c')->where('c.tarjeta = '.$id)->select('c.id')->getQuery()->getArrayResult();
            if(count($compras) > 0){
                $resultado = array("ok" => false, "error_msg" => "Lo sentimos, la tarjeta está asociada a compras anteriores y no la podemos borrar", "status" => 4,"code"=>"card_sell");
            }else{
                $qi->removeFireByUid($tarjeta,$uid);
                $em->remove($tarjeta);
                $em->flush();
                $resultado = array("ok" => true, "error_msg" => "Tarjeta eliminada", "status" => 3,"tarjeta_id"=>$tarjeta->getId(),"code"=>"tarjeta_eliminada");
            }

        }else{
            $resultado = array("ok" => false, "error_msg" => "No existe la tarjeta", "status" => 1,"code"=>"dir_not_exists");

        }

        $serializer = $this->get('jms_serializer');
        $json = $serializer->serialize($resultado,'json');
        return new Response($json);

    }
}
