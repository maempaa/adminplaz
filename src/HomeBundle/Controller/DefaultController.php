<?php

namespace HomeBundle\Controller;

use AppBundle\Entity\ContactList;
use AppBundle\Form\Type\ContactType;
use HomeBundle\Entity\Adicional;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use HomeBundle\Entity\Encuesta;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Service\QI;



class DefaultController extends Controller
{

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        //$compra = $this->getDoctrine()->getRepository('ListappBundle:Usuario')->find(1);
        //$this->get('qi')->saveFire($compra);
        return $this->render('HomeBundle:Default:index.html.twig', array());
    }

    /**
     * @Route("/aliados", name="aliados")
     */
    public function aliadosAction(Request $request)
    {
        $restaurantes = $this->getDoctrine()->getRepository('ListappBundle:Restaurante')->findBy(array('visible'=>true),array('orden'=>'asc'));
        return $this->render('HomeBundle:Default:aliados.html.twig', array('llave'=>'aliados','restaurantes'=>$restaurantes));
    }

    /**
     * @Route("/planes", name="planes")
     */
    public function planesAction(Request $request)
    {
        $planes = $this->getDoctrine()->getRepository('ListappBundle:Plan')->findBy(array('visible'=>true),array('orden'=>'asc'));
        return $this->render('HomeBundle:Default:planes.html.twig', array('llave'=>'planes','planes'=>$planes));
    }

    /**
     * @Route("/planes_listapp", name="checkout")
     */
    public function checkoutAction(Request $request)
    {
        $tarjeta = $this->getDoctrine()->getRepository('AppBundle:Tarjeta')->findOneBy(array());
        //$this->get('qi')->saveFireByUid($tarjeta);
        $deviceSessionId = md5(session_id().microtime());
        $planes = $this->getDoctrine()->getRepository('ListappBundle:Plan')->findBy(array('visible'=>true),array('orden'=>'asc'));

        return $this->render('HomeBundle:Default:checkout.html.twig', array('llave'=>'checkout','planes'=>$planes,'deviceSessionId'=>$deviceSessionId));
    }

    /**
     * @Route("/perfil", name="perfil")
     */
    public function perfilAction(Request $request)
    {
        return $this->render('HomeBundle:Default:perfil.html.twig', array('llave'=>'perfil'));
    }

    /**
     * @Route("/restaurantes", name="restaurantes")
     */
    public function restaurantesAction()
    {
        return $this->render('HomeBundle:Default:restaurants.html.twig', array('llave'=>'restaurantes'));
    }

    /**
     * @Route("/mapa", name="mapa")
     */
    public function mapaAction()
    {
        $restaurantes = $this->getDoctrine()->getRepository('ListappBundle:Restaurante')->findBy(array('visible'=>true),array('orden'=>'asc'));
        return $this->render('HomeBundle:Default:mapa.html.twig', array('llave'=>'mapa','restaurantes'=>$restaurantes));
    }
    
    /**
     * @Route("/insumos", name="insumos")
     */
    public function insumoAction(Request $request)
    {
        $lc = ucfirst($request->getLocale());
        $ingredientes = $this->getDoctrine()->getRepository('HomeBundle:Ingrediente')->findBy(array('visible'=>true),array('nombre'.$lc=>'asc'));
        $repo_prod = $this->getDoctrine()->getRepository('CarroiridianBundle:Producto');
        return $this->render('HomeBundle:Default:insumos.html.twig', array('ingredientes'=>$ingredientes,'repo_prod'=>$repo_prod));
    }
    /**
     * @Route("/terminos-y-condiciones", name="terminos_y_condiciones")
     */
    public function terminosAction()
    {
        return $this->render('HomeBundle:Default:terminos.html.twig',array('llave'=>'terminos'));
    }
    /**
     * @Route("/politicas", name="politicas")
     */
    public function politicasAction()
    {
        return $this->render('HomeBundle:Default:politicas.html.twig',array('llave'=>'politicas'));
    }

    /**
     * @Route("/domicilios", name="domicilios")
     */
    public function domiciliosAction(Request $request)
    {
        $sedes= $this->getDoctrine()->getRepository("AppBundle:Sede")->findAll();
        return $this->render('HomeBundle:Default:domicilios.html.twig',array("sedes"=>$sedes));
    }

    /**
     * @Route("/testmail", name="tesmail")
     */
    public function testEmailAction()
    {
        $qi = $this->get("qi");
        die(dump($qi->sendMailIB("Prueba","odproductor@hotmail.com","Hola")));
        return $this->render('HomeBundle:Default:politicas.html.twig',array('llave'=>'politicas'));
    }

    /**
     * @Route("/clientes-institucionales", name="clientes_institucionales")
     */
    public function clientesAction(Request $request)
    {
        $clientes= $this->getDoctrine()->getRepository("AppBundle:Cliente")->findBy(array("visible"=>"1"),array("orden"=>"asc"));
        $contacto = new ContactList();
        $lc = $this->get('translator')->getLocale();
        $form = $this->createForm(ContactType::class, $contacto, array('locale' => $request->getLocale()));
        $form->handleRequest($request);
        $gracias = false;
        if ($form->isValid()) {
            $gracias = true;
            $qi = $this->get('qi');
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($contacto);
            $em->flush();
            //ENVIAR MENSAJE

            $from = $qi->getSettingDB('mail_envio');
            $interno = $qi->getSettingDB('mail_recepcion');
            $mensaje_cliente = $qi->getTextoBigDB('mail_contacto_cliente');
            $qi->sendMail($qi->getTextoDB('mail_contacto_cliente_asunto'), $from, $contacto->getEmail(),array(), $mensaje_cliente );

            $contacto = new ContactList();
            $lc = $this->get('translator')->getLocale();
            $form = $this->createForm(ContactType::class, $contacto, array('locale' => $request->getLocale()));

        }
        return $this->render('HomeBundle:Default:clientes.html.twig',array("clientes"=>$clientes,"form" => $form->createView(), 'gracias'=> $gracias));
    }

    /**
     * @Route("/mapadelsitio", name="sitemap")
     */
    public function siteAction()
    {
        return $this->render('HomeBundle:Default:terminos.html.twig',array('llave'=>'sitemap'));
    }

    /**
     * @Route("/politica", name="politica")
     */
    public function politicaAction()
    {
        return $this->render('HomeBundle:Default:terminos.html.twig',array('llave'=>'politica'));
    }
}
