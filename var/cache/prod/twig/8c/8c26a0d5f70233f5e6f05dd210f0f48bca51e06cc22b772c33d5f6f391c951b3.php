<?php

/* @Home/Default/footer.html.twig */
class __TwigTemplate_e442607ce1a983535403d3905d78d5a022404e1abf7e8e68789437e797964deb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["ruta"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        $context["params"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method");
        // line 3
        $context["params"] = twig_slice($this->env, ($context["params"] ?? null), 1);
        // line 4
        $context["lc"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "locale", array());
        // line 5
        echo "
<!-- FOOTER -->
<footer class=\"section-padding footer text-center\">
    <div class=\"page-content\">
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "@Home/Default/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 5,  29 => 4,  27 => 3,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Home/Default/footer.html.twig", "E:\\dev\\plaz\\src\\HomeBundle\\Resources\\views\\Default\\footer.html.twig");
    }
}
