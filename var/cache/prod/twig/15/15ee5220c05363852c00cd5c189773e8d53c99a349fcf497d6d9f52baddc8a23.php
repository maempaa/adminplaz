<?php

/* lay_iridian.html.twig */
class __TwigTemplate_b8d7458edde96e8af35ae76f4757c42945968a104dc340cde3d3e21082795dae extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "locale", array()), "html", null, true);
        echo "\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    ";
        // line 9
        $context["seo_arr"] = twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "seo", array(0 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "getUri", array(), "method"), 1 => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("homepage")), "method");
        // line 10
        echo "    ";
        $context["seo"] = twig_get_attribute($this->env, $this->source, ($context["seo_arr"] ?? null), "seo", array());
        // line 11
        echo "    ";
        $context["url"] = twig_get_attribute($this->env, $this->source, ($context["seo_arr"] ?? null), "url", array());
        // line 12
        echo "    ";
        $context["homepage"] = twig_get_attribute($this->env, $this->source, ($context["seo_arr"] ?? null), "homepage", array());
        // line 13
        echo "    <title>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "titulo", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "descripcion", array()), "html", null, true);
        echo "\" />
    <link rel=\"image_src\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, ((($context["homepage"] ?? null) . "") . twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "imagen", array())), "html", null, true);
        echo "\"/>
    ";
        // line 16
        if (twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_autor"), "method")) {
            // line 17
            echo "        <meta name=\"author\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_autor"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 19
        echo "    <meta name=\"twitter:card\" content=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "descripcion", array()), "html", null, true);
        echo "\">
    ";
        // line 20
        if (twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_twitter:site"), "method")) {
            // line 21
            echo "        <meta name=\"twitter:site\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_twitter:site"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 23
        echo "    <meta name=\"twitter:title\" content=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "titulo", array()), "html", null, true);
        echo "\">
    <meta name=\"twitter:description\" content=\"";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "descripcion", array()), "html", null, true);
        echo "\">
    ";
        // line 25
        if (twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_twitter:creator"), "method")) {
            // line 26
            echo "        <meta name=\"twitter:creator\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_twitter:creator"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 28
        echo "    <meta name=\"twitter:image\" content=\"";
        echo twig_escape_filter($this->env, ((($context["homepage"] ?? null) . "") . twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "imagen", array())), "html", null, true);
        echo "\">
    <meta property=\"og:title\" content=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "titulo", array()), "html", null, true);
        echo "\" />
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:url\" content=\"";
        // line 31
        echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
        echo "\" />
    <meta property=\"og:image\" content=\"";
        // line 32
        echo twig_escape_filter($this->env, ((($context["homepage"] ?? null) . "") . twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "imagen", array())), "html", null, true);
        echo "\" />
    <meta property=\"og:description\" content=\"";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["seo"] ?? null), "descripcion", array()), "html", null, true);
        echo "\" />
    ";
        // line 34
        if (twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_og:site_name"), "method")) {
            // line 35
            echo "        <meta property=\"og:site_name\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_og:site_name"), "method"), "html", null, true);
            echo "\" />
    ";
        }
        // line 37
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_fb:admins"), "method")) {
            // line 38
            echo "        <meta property=\"fb:admins\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "getSettingDB", array(0 => "seo_fb:admins"), "method"), "html", null, true);
            echo "\" />
    ";
        }
        // line 40
        echo "


    <link rel=\"icon\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "imagen", array(0 => "favicon"), "method"), "html", null, true);
        echo "\">

    ";
        // line 45
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 58
        echo "
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->

    <link rel=\"canonical\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "uri", array()), "html", null, true);
        echo "\" />

</head>
<body>

    <div id=\"content\" class=\"remodal-bg\">
      ";
        // line 71
        $this->loadTemplate("@Home/Default/header.html.twig", "lay_iridian.html.twig", 71)->display($context);
        // line 72
        echo "      ";
        $this->displayBlock('content', $context, $blocks);
        // line 73
        echo "      ";
        $this->loadTemplate("@Home/Default/footer.html.twig", "lay_iridian.html.twig", 73)->display($context);
        // line 74
        echo "    </div>
    <div id=\"loader\" class=\"hidden\"></div>
    ";
        // line 76
        if (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", array()) != "dev") && false)) {
            // line 77
            echo "        ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "2c12a27_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_0") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.min_1.js");
                // line 93
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_1"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_1") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery-ui.min_2.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_2"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_2") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_lodash.min_3.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_3"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_3") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.mCustomScrollbar.concat.min_4.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_4"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_4") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_slideout_5.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_5"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_5") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_swiper.min_6.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_6"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_6") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_shuffle.min_7.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_7"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_7") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jssocials.min_8.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_8"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_8") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_remodal.min_9.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_9"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_9") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.rateyo.min_10.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_10"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_10") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_loadingoverlay.min_11.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_11"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_11") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_utils_12.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_12"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_12") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_carrito_13.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
            } else {
                // asset "2c12a27"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" async></script>
        ";
            }
            unset($context["asset_url"]);
            // line 95
            echo "    ";
        } else {
            // line 96
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/dist/jquery.min.js"), "html", null, true);
            echo "\"></script>
        ";
            // line 98
            echo "    ";
        }
        // line 99
        echo "

    ";
        // line 101
        $this->displayBlock('javascripts', $context, $blocks);
        // line 102
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Raleway:700\" rel=\"stylesheet\">
</body>
</html>";
    }

    // line 45
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 46
        echo "        ";
        if (((twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "environment", array()) != "dev") && false)) {
            // line 47
            echo "            ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "e2cd3da_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_e2cd3da_0") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/iridianv2_fontawesome-all_1.css");
                // line 51
                echo "            <link rel=\"stylesheet\" href=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" />
            ";
            } else {
                // asset "e2cd3da"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_e2cd3da") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/iridianv2.css");
                echo "            <link rel=\"stylesheet\" href=\"";
                echo twig_escape_filter($this->env, ($context["asset_url"] ?? null), "html", null, true);
                echo "\" />
            ";
            }
            unset($context["asset_url"]);
            // line 53
            echo "        ";
        } else {
            // line 54
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/web-fonts-with-css/css/fontawesome-all.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style.css?v=1.0.1"), "html", null, true);
            echo "\">
        ";
        }
        // line 57
        echo "    ";
    }

    // line 72
    public function block_content($context, array $blocks = array())
    {
    }

    // line 101
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "lay_iridian.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  345 => 101,  340 => 72,  336 => 57,  331 => 55,  326 => 54,  323 => 53,  309 => 51,  304 => 47,  301 => 46,  298 => 45,  292 => 102,  290 => 101,  286 => 99,  283 => 98,  278 => 96,  275 => 95,  189 => 93,  184 => 77,  182 => 76,  178 => 74,  175 => 73,  172 => 72,  170 => 71,  161 => 65,  152 => 58,  150 => 45,  145 => 43,  140 => 40,  134 => 38,  131 => 37,  125 => 35,  123 => 34,  119 => 33,  115 => 32,  111 => 31,  106 => 29,  101 => 28,  95 => 26,  93 => 25,  89 => 24,  84 => 23,  78 => 21,  76 => 20,  71 => 19,  65 => 17,  63 => 16,  59 => 15,  55 => 14,  50 => 13,  47 => 12,  44 => 11,  41 => 10,  39 => 9,  29 => 2,  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "lay_iridian.html.twig", "E:\\dev\\plaz\\app\\Resources\\views\\lay_iridian.html.twig");
    }
}
