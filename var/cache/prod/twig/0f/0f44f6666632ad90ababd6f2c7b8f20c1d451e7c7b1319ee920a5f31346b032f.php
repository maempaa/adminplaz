<?php

/* CarroiridianBundle:Default:datos.html.twig */
class __TwigTemplate_6106c41e1b589373a6e447e9bf72dc7d38514f30e5da1e7b71aa9f6e4e925382 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("lay_iridian.html.twig", "CarroiridianBundle:Default:datos.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "lay_iridian.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/register.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        // line 8
        echo "
    <main role=\"main\" class=\"section-padding page-content register\">
        <form class=\"register__form\" action=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_check");
        echo "\" method=\"post\" id=\"form_login\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\" />
            <h1 class=\"text-2 black text-center top-space bottom-space\">BIENVENIDO A PLAZ MANAGER</h1>
            <div>
                ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "                    ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "            </div>

            <label>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => "correo"), "method"), "html", null, true);
        echo "</label>
            <input id=\"username\" name=\"_username\" required/>
            <label>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => "contraseña"), "method"), "html", null, true);
        echo "</label>
            <input id=\"password\" name=\"_password\" type=\"password\" required/>

            <div class=\"register__form-remember text-center\">
                <input type=\"checkbox\" class=\"styled-checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\">
                <label for=\"remember_me\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => "recordarme"), "method"), "html", null, true);
        echo "</label>
            </div>

            <button class=\"register__form-submit cta-button cta-button--dark\" type=\"submit\">";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => "ingresar"), "method"), "html", null, true);
        echo "</button>
            <a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_resetting_request");
        echo "\" class=\"register__form-forgot\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => "¿Olvidaste tu nombre de usuario o contraseña?"), "method"), "html", null, true);
        echo "</a>
        </form>
    </main>


";
    }

    public function getTemplateName()
    {
        return "CarroiridianBundle:Default:datos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 30,  100 => 29,  94 => 26,  86 => 21,  81 => 19,  77 => 17,  68 => 15,  64 => 14,  58 => 11,  54 => 10,  50 => 8,  47 => 7,  41 => 4,  36 => 3,  33 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "CarroiridianBundle:Default:datos.html.twig", "C:\\xampp\\htdocs\\AdminPlaz2\\src\\CarroiridianBundle/Resources/views/Default/datos.html.twig");
    }
}
