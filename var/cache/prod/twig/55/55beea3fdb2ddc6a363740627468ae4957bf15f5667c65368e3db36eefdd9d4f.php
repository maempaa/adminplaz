<?php

/* FOSUserBundle:Resetting:request_content.html.twig */
class __TwigTemplate_0dc3f99eac875fe4ca3e8c73f2f267f5ccd3493d652fe986e94d0c5b160a6e40 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
<form class=\"register__form register__form--wider\" action=\"";
        // line 3
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_resetting_send_email");
        echo "\" method=\"POST\" id=\"form_reset\">
    <h1 class=\"text-2 black top-space bottom-space text-center\">RESTAURAR CONTRASEÑA</h1>

    <p class=\"text-5 text-center bottom-space-xs\">Escribe el correo electrónico que usaste para abrir tu cuenta.</p>
    ";
        // line 7
        if ((isset($context["invalid_username"]) || array_key_exists("invalid_username", $context))) {
            // line 8
            echo "        <p>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("resetting.request.invalid_username", array("%username%" => ($context["invalid_username"] ?? null)), "FOSUserBundle"), "html", null, true);
            echo "</p>
    ";
        }
        // line 10
        echo "    <input type=\"text\" id=\"username\" name=\"username\" required=\"required\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("resetting.request.username", array(), "FOSUserBundle"), "html", null, true);
        echo "\"/>

    <div class=\"register__form-block text-center\">
        <button class=\"register__form-submit cta-button cta-button--dark\" type=\"submit\">";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("resetting.request.submit", array(), "FOSUserBundle"), "html", null, true);
        echo "</button>
    </div>
</form>
";
    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:request_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 13,  41 => 10,  35 => 8,  33 => 7,  26 => 3,  23 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "FOSUserBundle:Resetting:request_content.html.twig", "C:\\xampp\\htdocs\\AdminPlaz2\\app/Resources/FOSUserBundle/views/Resetting/request_content.html.twig");
    }
}
