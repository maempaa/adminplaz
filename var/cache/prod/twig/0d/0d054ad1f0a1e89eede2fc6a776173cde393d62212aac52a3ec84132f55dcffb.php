<?php

/* @Twig/Exception/error404.html.twig */
class __TwigTemplate_698fe6b2d470215822528a4e65b5c48300555437fa62112f1761dcff483bce78 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("lay_iridian.html.twig", "@Twig/Exception/error404.html.twig", 1);
        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "lay_iridian.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    <div class=\"bannerGeneral\" style=\"background-image: url('";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "imagen", array(0 => "banner-nosotros"), "method"), "html", null, true);
        echo "')\"></div>
    <div class=\"container terminos\">
        <div class=\"row\" id=\"que\">
            <div class=\"descripcionPag\">
                <h1 class=\"ttlGrandGris\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "textoDB", array(0 => ($context["llave"] ?? null)), "method"), "html", null, true);
        echo "</h1>
                <hr class=\"ttlLine\"/>
                <a  class=\"scrollDown\" href=\"#smooth\">
                    <i class=\"fa fa-angle-down\" aria-hidden=\"true\"></i>
                </a>
                <div class=\"col-md-12\">
                    ";
        // line 14
        echo twig_get_attribute($this->env, $this->source, ($context["qi"] ?? null), "TextoBigDB", array(0 => "Error404"), "method");
        echo "
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error404.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 14,  43 => 8,  35 => 4,  32 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Twig/Exception/error404.html.twig", "C:\\xampp\\htdocs\\AdminPlaz2\\app\\Resources\\TwigBundle\\views\\Exception\\error404.html.twig");
    }
}
