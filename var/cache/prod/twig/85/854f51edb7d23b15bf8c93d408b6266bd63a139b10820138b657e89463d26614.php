<?php

/* AppBundle::inventarios.html.twig */
class __TwigTemplate_17f1ffd22870d82ab4dd87590196d4195134888c99fc0e097d727a57a88ee8be extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 5
        return $this->loadTemplate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "templates", array()), "layout", array()), "AppBundle::inventarios.html.twig", 5);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["_entity_config"] = $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->getEntityConfiguration("Inventario");
        // line 2
        $context["__internal_655c15721e84b8e315a394bdd3d9e0841aecb2da23e5baeff02c388d1962182a"] = twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "translation_domain", array());
        // line 3
        $context["_trans_parameters"] = array("%entity_name%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", array()), array(),         // line 2
($context["__internal_655c15721e84b8e315a394bdd3d9e0841aecb2da23e5baeff02c388d1962182a"] ?? null)), "%entity_label%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source,         // line 3
($context["_entity_config"] ?? null), "label", array()), array(),         // line 2
($context["__internal_655c15721e84b8e315a394bdd3d9e0841aecb2da23e5baeff02c388d1962182a"] ?? null)));
        // line 5
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_body_id($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ("easyadmin-new-" . twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", array())), "html", null, true);
    }

    // line 8
    public function block_body_class($context, array $blocks = array())
    {
        echo twig_escape_filter($this->env, ("new new-" . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->source, ($context["_entity_config"] ?? null), "name", array()))), "html", null, true);
    }

    // line 10
    public function block_content_title($context, array $blocks = array())
    {
        // line 11
        echo "    Inventarioss
";
    }

    // line 14
    public function block_main($context, array $blocks = array())
    {
        // line 15
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
    }

    public function block_entity_form($context, array $blocks = array())
    {
        // line 16
        echo "        ";
        $context["tam"] = twig_length_filter($this->env, ($context["tallas"] ?? null));
        // line 17
        echo "        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("excel_generador_inventarios");
        echo "\" class=\"btn btn-primary\">Descargar formato base</a>
        <br/><br/>
        <style>
            .content {overflow: auto;height: 84vh;}
        </style>
        <table class=\"table table-striped table-bordered\" id=\"inventarios\">
            <tr class=\"ta_center\">
                <th></th>
                <th></th>
                ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tallas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
            // line 27
            echo "                    <th ";
            if (($context["precio"] ?? null)) {
                echo "colspan=\"3\"";
            } else {
                echo "colspan=\"2\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, $context["talla"], "nombreEs", array())), "html", null, true);
            echo "</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "            </tr>
            <tr class=\"ta_center\">
                <th>SKU</th>
                <th>PRODUCTO</th>
                ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tallas"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
            // line 34
            echo "                    ";
            if (($context["precio"] ?? null)) {
                // line 35
                echo "                        <th>PRECIO</th>
                    ";
            }
            // line 37
            echo "                    <th colspan=\"2\">CANTIDAD</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "            </tr>
            ";
        // line 40
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["inventarios"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["fila"]) {
            // line 41
            echo "                <tr data-producto-id=\"";
            echo twig_escape_filter($this->env, (($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 = $context["fila"]) && is_array($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5) || $__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5 instanceof ArrayAccess ? ($__internal_7cd7461123377b8c9c1b6a01f46c7bbd94bd12e59266005df5e93029ddbc0ec5["producto_id"] ?? null) : null), "html", null, true);
            echo "\">
                    <td>";
            // line 42
            echo twig_escape_filter($this->env, (($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a = $context["fila"]) && is_array($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a) || $__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a instanceof ArrayAccess ? ($__internal_3e28b7f596c58d7729642bcf2acc6efc894803703bf5fa7e74cd8d2aa1f8c68a["sku"] ?? null) : null), "html", null, true);
            echo "</td>
                    <td>";
            // line 43
            echo twig_escape_filter($this->env, (($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 = $context["fila"]) && is_array($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57) || $__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57 instanceof ArrayAccess ? ($__internal_b0b3d6199cdf4d15a08b3fb98fe017ecb01164300193d18d78027218d843fc57["nombre"] ?? null) : null), "html", null, true);
            echo "</td>
                    ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (($context["tam"] ?? null) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 45
                echo "                        ";
                if ((($context["precio"] ?? null) == 1)) {
                    // line 46
                    echo "                            <td>
                                <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                                <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                                <input data-tipo=\"precio\"   data-talla-id=\"";
                    // line 49
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9 = ($context["tallas"] ?? null)) && is_array($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9) || $__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9 instanceof ArrayAccess ? ($__internal_81ccf322d0988ca0aa9ae9943d772c435c5ff01fb50b956278e245e40ae66ab9[$context["i"]] ?? null) : null), "id", array()), "html", null, true);
                    echo "\" class=\"form-control precio talla_";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217 = ($context["tallas"] ?? null)) && is_array($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217) || $__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217 instanceof ArrayAccess ? ($__internal_add9db1f328aaed12ef1a33890510da978cc9cf3e50f6769d368473a9c90c217[$context["i"]] ?? null) : null), "id", array()), "html", null, true);
                    echo "\"   type=\"text\" value=\"";
                    echo twig_escape_filter($this->env, (($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105 = $context["fila"]) && is_array($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105) || $__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105 instanceof ArrayAccess ? ($__internal_128c19eb75d89ae9acc1294da2e091b433005202cb9b9351ea0c5dd5f69ee105[("precio_" . ($context["i"] + 1))] ?? null) : null), "html", null, true);
                    echo "\">
                            </td>
                        ";
                }
                // line 52
                echo "                        <td>
                            <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                            <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                            <input data-tipo=\"cantidad\" data-talla-id=\"";
                // line 55
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779 = ($context["tallas"] ?? null)) && is_array($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779) || $__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779 instanceof ArrayAccess ? ($__internal_921de08f973aabd87ecb31654784e2efda7404f12bd27e8e56991608c76e7779[$context["i"]] ?? null) : null), "id", array()), "html", null, true);
                echo "\" class=\"form-control cantidad talla_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1 = ($context["tallas"] ?? null)) && is_array($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1) || $__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1 instanceof ArrayAccess ? ($__internal_3e040fa9f9bcf48a8b054d0953f4fffdaf331dc44bc1d96f1bb45abb085e61d1[$context["i"]] ?? null) : null), "id", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, (($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0 = $context["fila"]) && is_array($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0) || $__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0 instanceof ArrayAccess ? ($__internal_bd1cf16c37e30917ff4f54b7320429bcc2bb63615cd8a735bfe06a3f1b5c82a0[("cantidad_" . ($context["i"] + 1))] ?? null) : null), "html", null, true);
                echo "\"></td>
                        <td class=\"vm\" data-talla-id=\"";
                // line 56
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866 = ($context["tallas"] ?? null)) && is_array($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866) || $__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866 instanceof ArrayAccess ? ($__internal_602f93ae9072ac758dc9cd47ca50516bbc1210f73d2a40b01287f102c3c40866[$context["i"]] ?? null) : null), "id", array()), "html", null, true);
                echo "\"><a href=\"#\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i></a></td>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fila'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 60
        echo "        </table>
    ";
    }

    // line 64
    public function block_body_javascript($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "
    ";
        // line 66
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
    }

    public function getTemplateName()
    {
        return "AppBundle::inventarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  218 => 66,  213 => 65,  210 => 64,  205 => 60,  198 => 58,  190 => 56,  182 => 55,  177 => 52,  167 => 49,  162 => 46,  159 => 45,  155 => 44,  151 => 43,  147 => 42,  142 => 41,  138 => 40,  135 => 39,  128 => 37,  124 => 35,  121 => 34,  117 => 33,  111 => 29,  96 => 27,  92 => 26,  79 => 17,  76 => 16,  69 => 15,  66 => 14,  61 => 11,  58 => 10,  52 => 8,  46 => 7,  42 => 5,  40 => 2,  39 => 3,  38 => 2,  37 => 3,  35 => 2,  33 => 1,  27 => 5,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "AppBundle::inventarios.html.twig", "C:\\xampp\\htdocs\\AdminPlaz2\\src\\AppBundle/Resources/views/inventarios.html.twig");
    }
}
