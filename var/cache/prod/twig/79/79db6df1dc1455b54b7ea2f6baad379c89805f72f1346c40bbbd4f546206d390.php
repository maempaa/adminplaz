<?php

/* @Home/Default/header.html.twig */
class __TwigTemplate_db3d59574dabb5bcc9976c88d0db0c1d7116aecea478462f992c50d8fe9ea3fb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["ruta"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        $context["params"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method");
        // line 3
        $context["params"] = twig_slice($this->env, ($context["params"] ?? null), 1);
        // line 4
        $context["lc"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "locale", array());
        // line 5
        $context["categoria"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["app"] ?? null), "request", array()), "get", array(0 => "categoria"), "method");
        // line 6
        echo "

<header id=\"menu\" class=\"menu\" role=\"Navigation\">
    <img class=\"menu__mobile-menu-btn md-hidden\" src=\"/images/mobile-menu-btn.svg\"  alt=\"mobile menu buttonl\"/>

    <div class=\"page-content menu__content\">
        <a class=\"menu__logo\" href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\"><img src=\"/images/plaz.png\"  alt=\"logo\"/></a>
    </div>
</header>
<!-- / HEADER -->
";
    }

    public function getTemplateName()
    {
        return "@Home/Default/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 12,  33 => 6,  31 => 5,  29 => 4,  27 => 3,  25 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Home/Default/header.html.twig", "C:\\xampp\\htdocs\\AdminPlaz2\\src\\HomeBundle\\Resources\\views\\Default\\header.html.twig");
    }
}
