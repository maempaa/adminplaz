<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/a')) {
            if (0 === strpos($pathinfo, '/api')) {
                // es__RG__api_default_index
                if ('/api/home' === $pathinfo) {
                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_index',);
                }

                // es__RG__api_default_test
                if ('/api/test' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_es__RG__api_default_test;
                    }

                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::testAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_test',);
                }
                not_es__RG__api_default_test:

                // es__RG__api_default_terminos
                if ('/api/terminos' === $pathinfo) {
                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::terminosAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_terminos',);
                }

                // es__RG__api_default_codigo
                if (0 === strpos($pathinfo, '/api/cupon') && preg_match('#^/api/cupon/(?P<email>[^/]++)/(?P<codigo>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_es__RG__api_default_codigo;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__api_default_codigo')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::codigoAction',  '_locale' => 'es',));
                }
                not_es__RG__api_default_codigo:

                // es__RG__api_default_vigencia
                if (0 === strpos($pathinfo, '/api/vigencia') && preg_match('#^/api/vigencia/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_es__RG__api_default_vigencia;
                    }

                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__api_default_vigencia')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::vigenciaAction',  '_locale' => 'es',));
                }
                not_es__RG__api_default_vigencia:

                // es__RG__api_default_agregartarjeta
                if ('/api/agregar-tarjeta' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__api_default_agregartarjeta;
                    }

                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::agregarTarjetaAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_agregartarjeta',);
                }
                not_es__RG__api_default_agregartarjeta:

                // es__RG__api_default_adddireccion
                if ('/api/add-direccion' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__api_default_adddireccion;
                    }

                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::addDireccionAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_adddireccion',);
                }
                not_es__RG__api_default_adddireccion:

                if (0 === strpos($pathinfo, '/api/secure')) {
                    // es__RG__api_default_pagar
                    if ('/api/secure/pagar' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_es__RG__api_default_pagar;
                        }

                        return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::pagarAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_pagar',);
                    }
                    not_es__RG__api_default_pagar:

                    // es__RG__api_default_pedido
                    if ('/api/secure/pedido' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_es__RG__api_default_pedido;
                        }

                        return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::pedidoAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_pedido',);
                    }
                    not_es__RG__api_default_pedido:

                    // es__RG__api_default_calificar
                    if ('/api/secure/calificar' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_es__RG__api_default_calificar;
                        }

                        return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::calificarAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_calificar',);
                    }
                    not_es__RG__api_default_calificar:

                }

                // es__RG__api_default_precios
                if (0 === strpos($pathinfo, '/api/precios') && preg_match('#^/api/precios/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__api_default_precios')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::preciosAction',  '_locale' => 'es',));
                }

                // es__RG__api_default_registrarusuario
                if ('/api/registrar-usuario' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__api_default_registrarusuario;
                    }

                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::registrarUsuarioAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_registrarusuario',);
                }
                not_es__RG__api_default_registrarusuario:

                // es__RG__api_default_updateusuario
                if ('/api/update-usuario' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__api_default_updateusuario;
                    }

                    return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::updateUsuarioAction',  '_locale' => 'es',  '_route' => 'es__RG__api_default_updateusuario',);
                }
                not_es__RG__api_default_updateusuario:

            }

            elseif (0 === strpos($pathinfo, '/add')) {
                // es__RG__add_tarjeta
                if ('/add_tarjeta' === $pathinfo) {
                    return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::add_tarjetaAction',  '_locale' => 'es',  '_route' => 'es__RG__add_tarjeta',);
                }

                if (0 === strpos($pathinfo, '/add-carrito')) {
                    // es__RG__add_carrito
                    if (preg_match('#^/add\\-carrito/(?P<id>[^/]++)/(?P<cant>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__add_carrito')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::addCarritoAction',  '_locale' => 'es',));
                    }

                    // es__RG__add_carrito_talla
                    if (0 === strpos($pathinfo, '/add-carrito-talla') && preg_match('#^/add\\-carrito\\-talla/(?P<id>[^/]++)/(?P<cant>[^/]++)/(?P<id_talla>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__add_carrito_talla')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::addCarritoTallaAction',  '_locale' => 'es',));
                    }

                }

                // es__RG__add_wish
                if (0 === strpos($pathinfo, '/add-wish') && preg_match('#^/add\\-wish/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__add_wish')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::wishAction',  '_locale' => 'es',));
                }

            }

            elseif (0 === strpos($pathinfo, '/admin')) {
                if (0 === strpos($pathinfo, '/admin/inventarios')) {
                    if (0 === strpos($pathinfo, '/admin/inventarios_')) {
                        // es__RG__update_inventarios
                        if (0 === strpos($pathinfo, '/admin/inventarios_add') && preg_match('#^/admin/inventarios_add/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<cantidad>[^/]++)/(?P<precio>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__update_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::updateInventarioAction',  '_locale' => 'es',));
                        }

                        // es__RG__cantidad_inventarios
                        if (0 === strpos($pathinfo, '/admin/inventarios_cantidad') && preg_match('#^/admin/inventarios_cantidad/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<cantidad>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__cantidad_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::cantidadInventarioAction',  '_locale' => 'es',));
                        }

                        // es__RG__precio_inventarios
                        if (0 === strpos($pathinfo, '/admin/inventarios_precio') && preg_match('#^/admin/inventarios_precio/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<precio>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__precio_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::precioInventarioAction',  '_locale' => 'es',));
                        }

                    }

                    // es__RG__admin_inventarios
                    if ('/admin/inventarios' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioAction',  '_locale' => 'es',  '_route' => 'es__RG__admin_inventarios',);
                    }

                    if (0 === strpos($pathinfo, '/admin/inventarios-excel')) {
                        // es__RG__excel_inventarios
                        if (preg_match('#^/admin/inventarios\\-excel/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__excel_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioExcelAction',  '_locale' => 'es',));
                        }

                        // es__RG__excel_generador_inventarios
                        if ('/admin/inventarios-excel-generador' === $pathinfo) {
                            return array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioExcelGeneradorAction',  '_locale' => 'es',  '_route' => 'es__RG__excel_generador_inventarios',);
                        }

                    }

                }

                // es__RG__ordenargen
                if (0 === strpos($pathinfo, '/admin/ordenargen') && preg_match('#^/admin/ordenargen/(?P<tabla>[^/]++)/(?P<campo>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__ordenargen')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::ordenargenAction',  '_locale' => 'es',));
                }

                // es__RG__easyadmin
                if ('/admin' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__easyadmin');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\AdminController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__easyadmin',);
                }

                // es__RG__admin
                if ('/admin' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'es__RG__admin');
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\AdminController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__admin',);
                }

            }

            // es__RG__aviso
            if ('/aviso' === $pathinfo) {
                return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::avisoAction',  '_locale' => 'es',  '_route' => 'es__RG__aviso',);
            }

            // es__RG__aliados
            if ('/aliados' === $pathinfo) {
                return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::aliadosAction',  '_locale' => 'es',  '_route' => 'es__RG__aliados',);
            }

        }

        elseif (0 === strpos($pathinfo, '/e')) {
            if (0 === strpos($pathinfo, '/en')) {
                if (0 === strpos($pathinfo, '/en/a')) {
                    if (0 === strpos($pathinfo, '/en/api')) {
                        // en__RG__api_default_index
                        if ('/en/api/home' === $pathinfo) {
                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_index',);
                        }

                        // en__RG__api_default_test
                        if ('/en/api/test' === $pathinfo) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__api_default_test;
                            }

                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::testAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_test',);
                        }
                        not_en__RG__api_default_test:

                        // en__RG__api_default_terminos
                        if ('/en/api/terminos' === $pathinfo) {
                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::terminosAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_terminos',);
                        }

                        // en__RG__api_default_codigo
                        if (0 === strpos($pathinfo, '/en/api/cupon') && preg_match('#^/en/api/cupon/(?P<email>[^/]++)/(?P<codigo>[^/]++)$#s', $pathinfo, $matches)) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__api_default_codigo;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__api_default_codigo')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::codigoAction',  '_locale' => 'en',));
                        }
                        not_en__RG__api_default_codigo:

                        // en__RG__api_default_vigencia
                        if (0 === strpos($pathinfo, '/en/api/vigencia') && preg_match('#^/en/api/vigencia/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__api_default_vigencia;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__api_default_vigencia')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::vigenciaAction',  '_locale' => 'en',));
                        }
                        not_en__RG__api_default_vigencia:

                        // en__RG__api_default_agregartarjeta
                        if ('/en/api/agregar-tarjeta' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__api_default_agregartarjeta;
                            }

                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::agregarTarjetaAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_agregartarjeta',);
                        }
                        not_en__RG__api_default_agregartarjeta:

                        // en__RG__api_default_adddireccion
                        if ('/en/api/add-direccion' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__api_default_adddireccion;
                            }

                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::addDireccionAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_adddireccion',);
                        }
                        not_en__RG__api_default_adddireccion:

                        if (0 === strpos($pathinfo, '/en/api/secure')) {
                            // en__RG__api_default_pagar
                            if ('/en/api/secure/pagar' === $pathinfo) {
                                if ('POST' !== $canonicalMethod) {
                                    $allow[] = 'POST';
                                    goto not_en__RG__api_default_pagar;
                                }

                                return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::pagarAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_pagar',);
                            }
                            not_en__RG__api_default_pagar:

                            // en__RG__api_default_pedido
                            if ('/en/api/secure/pedido' === $pathinfo) {
                                if ('POST' !== $canonicalMethod) {
                                    $allow[] = 'POST';
                                    goto not_en__RG__api_default_pedido;
                                }

                                return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::pedidoAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_pedido',);
                            }
                            not_en__RG__api_default_pedido:

                            // en__RG__api_default_calificar
                            if ('/en/api/secure/calificar' === $pathinfo) {
                                if ('POST' !== $canonicalMethod) {
                                    $allow[] = 'POST';
                                    goto not_en__RG__api_default_calificar;
                                }

                                return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::calificarAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_calificar',);
                            }
                            not_en__RG__api_default_calificar:

                        }

                        // en__RG__api_default_precios
                        if (0 === strpos($pathinfo, '/en/api/precios') && preg_match('#^/en/api/precios/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__api_default_precios')), array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::preciosAction',  '_locale' => 'en',));
                        }

                        // en__RG__api_default_registrarusuario
                        if ('/en/api/registrar-usuario' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__api_default_registrarusuario;
                            }

                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::registrarUsuarioAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_registrarusuario',);
                        }
                        not_en__RG__api_default_registrarusuario:

                        // en__RG__api_default_updateusuario
                        if ('/en/api/update-usuario' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__api_default_updateusuario;
                            }

                            return array (  '_controller' => 'ApiBundle\\Controller\\DefaultController::updateUsuarioAction',  '_locale' => 'en',  '_route' => 'en__RG__api_default_updateusuario',);
                        }
                        not_en__RG__api_default_updateusuario:

                    }

                    elseif (0 === strpos($pathinfo, '/en/add')) {
                        // en__RG__add_tarjeta
                        if ('/en/add_tarjeta' === $pathinfo) {
                            return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::add_tarjetaAction',  '_locale' => 'en',  '_route' => 'en__RG__add_tarjeta',);
                        }

                        if (0 === strpos($pathinfo, '/en/add-carrito')) {
                            // en__RG__add_carrito
                            if (preg_match('#^/en/add\\-carrito/(?P<id>[^/]++)/(?P<cant>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__add_carrito')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::addCarritoAction',  '_locale' => 'en',));
                            }

                            // en__RG__add_carrito_talla
                            if (0 === strpos($pathinfo, '/en/add-carrito-talla') && preg_match('#^/en/add\\-carrito\\-talla/(?P<id>[^/]++)/(?P<cant>[^/]++)/(?P<id_talla>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__add_carrito_talla')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::addCarritoTallaAction',  '_locale' => 'en',));
                            }

                        }

                        // en__RG__add_wish
                        if (0 === strpos($pathinfo, '/en/add-wish') && preg_match('#^/en/add\\-wish/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__add_wish')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::wishAction',  '_locale' => 'en',));
                        }

                    }

                    // en__RG__aviso
                    if ('/en/aviso' === $pathinfo) {
                        return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::avisoAction',  '_locale' => 'en',  '_route' => 'en__RG__aviso',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/en/l')) {
                    // en__RG__listapp_default_index
                    if ('/en/listapp' === $pathinfo) {
                        return array (  '_controller' => 'ListappBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__listapp_default_index',);
                    }

                    // en__RG__listapp_default_load
                    if ('/en/load' === $pathinfo) {
                        return array (  '_controller' => 'ListappBundle\\Controller\\DefaultController::loadAction',  '_locale' => 'en',  '_route' => 'en__RG__listapp_default_load',);
                    }

                    // en__RG__registro_login_inicial
                    if ('/en/login' === $pathinfo) {
                        return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerNewAction',  '_locale' => 'en',  '_route' => 'en__RG__registro_login_inicial',);
                    }

                }

                // en__RG__experiencias
                if (0 === strpos($pathinfo, '/en/experiencia') && preg_match('#^/en/experiencia/(?P<id>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__experiencias')), array (  '_controller' => 'ExperienciasBundle\\Controller\\DefaultController::experienciaAction',  '_locale' => 'en',));
                }

                // en__RG__nosotros
                if ('/en/nosotros' === $pathinfo) {
                    return array (  '_controller' => 'NosotrosBundle\\Controller\\DefaultController::nosotrosAction',  '_locale' => 'en',  '_route' => 'en__RG__nosotros',);
                }

                if (0 === strpos($pathinfo, '/en/t')) {
                    // en__RG__test_mail
                    if ('/en/test-mail' === $pathinfo) {
                        return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::testMailAction',  '_locale' => 'en',  '_route' => 'en__RG__test_mail',);
                    }

                    // en__RG__tarjetas
                    if ('/en/tarjetas' === $pathinfo) {
                        return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::tarjetasAction',  '_locale' => 'en',  '_route' => 'en__RG__tarjetas',);
                    }

                    // en__RG__tarjeta-omnitural
                    if ('/en/tarjeta-omnitural' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::tarjetaAction',  '_locale' => 'en',  '_route' => 'en__RG__tarjeta-omnitural',);
                    }

                    // en__RG__tokenize
                    if ('/en/tokenize' === $pathinfo) {
                        return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::tokenCardAction',  '_locale' => 'en',  '_route' => 'en__RG__tokenize',);
                    }

                    // en__RG__home
                    if ('/en/the_store' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::homeAction',  '_locale' => 'en',  '_route' => 'en__RG__home',);
                    }

                    // en__RG__trabaje
                    if ('/en/trabaje-con-nosotros' === $pathinfo) {
                        return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::trabajeAction',  '_locale' => 'en',  '_route' => 'en__RG__trabaje',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/en/re')) {
                    if (0 === strpos($pathinfo, '/en/reporte-')) {
                        if (0 === strpos($pathinfo, '/en/reporte-restaurante')) {
                            // en__RG__reporte_restaurantes
                            if ('/en/reporte-restaurantes' === $pathinfo) {
                                return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteRestaurantesAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_restaurantes',);
                            }

                            // en__RG__reporte_restaurante
                            if ('/en/reporte-restaurante' === $pathinfo) {
                                return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteCategoriacAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_restaurante',);
                            }

                        }

                        // en__RG__reporte_pedidos_hoy
                        if ('/en/reporte-pedido-hoy' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reportePedidosHoyAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_pedidos_hoy',);
                        }

                        // en__RG__reporte_producto
                        if ('/en/reporte-producto' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteProductcAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_producto',);
                        }

                        // en__RG__reporte_total_vendido
                        if ('/en/reporte-total-vendido' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteTotalAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_total_vendido',);
                        }

                        // en__RG__reporte_completo
                        if ('/en/reporte-completo' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteCompletoAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_completo',);
                        }

                        if (0 === strpos($pathinfo, '/en/reporte-dia')) {
                            // en__RG__reporte_diario
                            if ('/en/reporte-diario' === $pathinfo) {
                                return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteDirarioAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_diario',);
                            }

                            // en__RG__reporte_dia
                            if ('/en/reporte-dia' === $pathinfo) {
                                return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteDiaAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_dia',);
                            }

                        }

                        // en__RG__reporte_mensual
                        if ('/en/reporte-mensual' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteMensualAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_mensual',);
                        }

                        // en__RG__reporte_estado
                        if ('/en/reporte-estados' === $pathinfo) {
                            return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteEstadoAction',  '_locale' => 'en',  '_route' => 'en__RG__reporte_estado',);
                        }

                    }

                    // en__RG__registro_login
                    if ('/en/registro' === $pathinfo) {
                        return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'en',  '_route' => 'en__RG__registro_login',);
                    }

                    // en__RG__recetas_prod
                    if (0 === strpos($pathinfo, '/en/recetas-producto') && preg_match('#^/en/recetas\\-producto/(?P<productoId>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__recetas_prod')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::recetasprodAction',  '_locale' => 'en',));
                    }

                    // en__RG__remove_carrito_bono
                    if (0 === strpos($pathinfo, '/en/remove-carrito-bono') && preg_match('#^/en/remove\\-carrito\\-bono/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__remove_carrito_bono')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::removeCarritoBonoAction',  '_locale' => 'en',));
                    }

                    // en__RG__resumen
                    if ('/en/resumen' === $pathinfo) {
                        return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::resumenAction',  '_locale' => 'en',  '_route' => 'en__RG__resumen',);
                    }

                }

                // en__RG__mailprueba
                if ('/en/mailprueba' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::mailpruebaAction',  '_locale' => 'en',  '_route' => 'en__RG__mailprueba',);
                }

                if (0 === strpos($pathinfo, '/en/me')) {
                    if (0 === strpos($pathinfo, '/en/media/cache/resolve')) {
                        // en__RG__liip_imagine_filter_runtime
                        if (preg_match('#^/en/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/rc/(?P<hash>[^/]++)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__liip_imagine_filter_runtime;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__liip_imagine_filter_runtime')), array (  '_controller' => 'liip_imagine.controller:filterRuntimeAction',  '_locale' => 'en',));
                        }
                        not_en__RG__liip_imagine_filter_runtime:

                        // en__RG__liip_imagine_filter
                        if (preg_match('#^/en/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__liip_imagine_filter;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__liip_imagine_filter')), array (  '_controller' => 'liip_imagine.controller:filterAction',  '_locale' => 'en',));
                        }
                        not_en__RG__liip_imagine_filter:

                    }

                    // en__RG__mensaje
                    if ('/en/mensaje' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::mensajeAction',  '_locale' => 'en',  '_route' => 'en__RG__mensaje',);
                    }

                    // en__RG__message
                    if ('/en/message' === $pathinfo) {
                        return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::messageAction',  '_locale' => 'en',  '_route' => 'en__RG__message',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/en/p')) {
                    if (0 === strpos($pathinfo, '/en/pagar-payu')) {
                        // en__RG__pagar_payu
                        if ('/en/pagar-payu' === $pathinfo) {
                            return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__pagar_payu',);
                        }

                        // en__RG__pagar_payu_respuesta
                        if ('/en/pagar-payu/respuesta' === $pathinfo) {
                            return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::respuestaAction',  '_locale' => 'en',  '_route' => 'en__RG__pagar_payu_respuesta',);
                        }

                        // en__RG__pagar_payu_confirmacion
                        if ('/en/pagar-payu/confirmacion' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__pagar_payu_confirmacion;
                            }

                            return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::confirmacionAction',  '_locale' => 'en',  '_route' => 'en__RG__pagar_payu_confirmacion',);
                        }
                        not_en__RG__pagar_payu_confirmacion:

                    }

                    // en__RG__pay
                    if (0 === strpos($pathinfo, '/en/pay') && preg_match('#^/en/pay/(?P<id_token>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__pay')), array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::PayAction',  '_locale' => 'en',));
                    }

                    // en__RG__post
                    if (0 === strpos($pathinfo, '/en/post') && preg_match('#^/en/post/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__post')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::postAction',  '_locale' => 'en',));
                    }

                    if (0 === strpos($pathinfo, '/en/product')) {
                        // en__RG__productos
                        if ('/en/productos' === $pathinfo) {
                            return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosAction',  '_locale' => 'en',  '_route' => 'en__RG__productos',);
                        }

                        if (0 === strpos($pathinfo, '/en/productos_por_')) {
                            // en__RG__productos_por_categoria
                            if (0 === strpos($pathinfo, '/en/productos_por_categoria') && preg_match('#^/en/productos_por_categoria/(?P<categoria>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__productos_por_categoria')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosCategoriaAction',  '_locale' => 'en',));
                            }

                            // en__RG__productos_por_genero
                            if (0 === strpos($pathinfo, '/en/productos_por_genero') && preg_match('#^/en/productos_por_genero/(?P<genero>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__productos_por_genero')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosGenAction',  '_locale' => 'en',));
                            }

                            // en__RG__productos_por_gen_cat
                            if (0 === strpos($pathinfo, '/en/productos_por_gen_cat') && preg_match('#^/en/productos_por_gen_cat/(?P<genero>[^/]++)/(?P<categoria>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__productos_por_gen_cat')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosCatGenAction',  '_locale' => 'en',));
                            }

                        }

                        // en__RG__product
                        if (preg_match('#^/en/product/(?P<id>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__product')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productAction',  '_locale' => 'en',));
                        }

                    }

                }

                // en__RG__untokenize
                if (0 === strpos($pathinfo, '/en/untokenize') && preg_match('#^/en/untokenize/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__untokenize')), array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::unTokenCardAction',  '_locale' => 'en',));
                }

                // en__RG__useriridian_default_index
                if ('/en/useriridian' === $pathinfo) {
                    return array (  '_controller' => 'UserIridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__useriridian_default_index',);
                }

                // en__RG__the_blog
                if ('/en/blog' === $pathinfo) {
                    return array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__the_blog',);
                }

                // en__RG__busca_productos
                if ('/en/buscador-productos' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::buscadorAction',  '_locale' => 'en',  '_route' => 'en__RG__busca_productos',);
                }

                // en__RG__video-post
                if (0 === strpos($pathinfo, '/en/video-post') && preg_match('#^/en/video\\-post/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__video-post')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::videoPostAction',  '_locale' => 'en',));
                }

                if (0 === strpos($pathinfo, '/en/c')) {
                    // en__RG__carrito
                    if ('/en/carrito-de-compras' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__carrito',);
                    }

                    // en__RG__carrito-completo
                    if ('/en/carrito-completo' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::cartAction',  '_locale' => 'en',  '_route' => 'en__RG__carrito-completo',);
                    }

                    // en__RG__ciudadesByDept
                    if (0 === strpos($pathinfo, '/en/ciudades-dept') && preg_match('#^/en/ciudades\\-dept/(?P<id>[^/]++)(?:/(?P<id_ciudad>[^/]++))?$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__ciudadesByDept')), array (  'id_ciudad' => 'a',  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::ciudadesByDeptAction',  '_locale' => 'en',));
                    }

                    // en__RG__contacto
                    if ('/en/contacto' === $pathinfo) {
                        return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::contactoAction',  '_locale' => 'en',  '_route' => 'en__RG__contacto',);
                    }

                }

                // en__RG__datos
                if ('/en/datos' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::datosAction',  '_locale' => 'en',  '_route' => 'en__RG__datos',);
                }

                if (0 === strpos($pathinfo, '/en/direccion')) {
                    // en__RG__direccion
                    if ('/en/direccion' === $pathinfo) {
                        return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::direccionAction',  '_locale' => 'en',  '_route' => 'en__RG__direccion',);
                    }

                    // en__RG__direccion_sesion
                    if (0 === strpos($pathinfo, '/en/direccion-sesion') && preg_match('#^/en/direccion\\-sesion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__direccion_sesion')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::direccionSesionAction',  '_locale' => 'en',));
                    }

                }

                // en__RG__set_carrito_talla
                if (0 === strpos($pathinfo, '/en/set-carrito-talla') && preg_match('#^/en/set\\-carrito\\-talla/(?P<id>[^/]++)/(?P<cant>[^/]++)/(?P<id_talla>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__set_carrito_talla')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::setCarritoTallaAction',  '_locale' => 'en',));
                }

                // en__RG__wishlist
                if ('/en/wishlist' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::wishlistAction',  '_locale' => 'en',  '_route' => 'en__RG__wishlist',);
                }

                // en__RG__gift
                if ('/en/gift' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::giftAction',  '_locale' => 'en',  '_route' => 'en__RG__gift',);
                }

                // en__RG__homepage
                if ('/en' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', 'en__RG__homepage');
                    }

                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__homepage',);
                }

                // en__RG__aliados
                if ('/en/aliados' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::aliadosAction',  '_locale' => 'en',  '_route' => 'en__RG__aliados',);
                }

                if (0 === strpos($pathinfo, '/en/admin')) {
                    if (0 === strpos($pathinfo, '/en/admin/inventarios')) {
                        if (0 === strpos($pathinfo, '/en/admin/inventarios_')) {
                            // en__RG__update_inventarios
                            if (0 === strpos($pathinfo, '/en/admin/inventarios_add') && preg_match('#^/en/admin/inventarios_add/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<cantidad>[^/]++)/(?P<precio>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__update_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::updateInventarioAction',  '_locale' => 'en',));
                            }

                            // en__RG__cantidad_inventarios
                            if (0 === strpos($pathinfo, '/en/admin/inventarios_cantidad') && preg_match('#^/en/admin/inventarios_cantidad/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<cantidad>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__cantidad_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::cantidadInventarioAction',  '_locale' => 'en',));
                            }

                            // en__RG__precio_inventarios
                            if (0 === strpos($pathinfo, '/en/admin/inventarios_precio') && preg_match('#^/en/admin/inventarios_precio/(?P<producto_id>[^/]++)/(?P<talla_id>[^/]++)/(?P<precio>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__precio_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::precioInventarioAction',  '_locale' => 'en',));
                            }

                        }

                        // en__RG__admin_inventarios
                        if ('/en/admin/inventarios' === $pathinfo) {
                            return array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioAction',  '_locale' => 'en',  '_route' => 'en__RG__admin_inventarios',);
                        }

                        if (0 === strpos($pathinfo, '/en/admin/inventarios-excel')) {
                            // en__RG__excel_inventarios
                            if (preg_match('#^/en/admin/inventarios\\-excel/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__excel_inventarios')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioExcelAction',  '_locale' => 'en',));
                            }

                            // en__RG__excel_generador_inventarios
                            if ('/en/admin/inventarios-excel-generador' === $pathinfo) {
                                return array (  '_controller' => 'AppBundle\\Controller\\AdminController::inventarioExcelGeneradorAction',  '_locale' => 'en',  '_route' => 'en__RG__excel_generador_inventarios',);
                            }

                        }

                    }

                    // en__RG__ordenargen
                    if (0 === strpos($pathinfo, '/en/admin/ordenargen') && preg_match('#^/en/admin/ordenargen/(?P<tabla>[^/]++)/(?P<campo>[^/]++)/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__ordenargen')), array (  '_controller' => 'AppBundle\\Controller\\AdminController::ordenargenAction',  '_locale' => 'en',));
                    }

                    // en__RG__easyadmin
                    if ('/en/admin' === $trimmedPathinfo) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'en__RG__easyadmin');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\AdminController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__easyadmin',);
                    }

                    // en__RG__admin
                    if ('/en/admin' === $trimmedPathinfo) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($pathinfo.'/', 'en__RG__admin');
                        }

                        return array (  '_controller' => 'AppBundle\\Controller\\AdminController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__admin',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/en/p')) {
                    if (0 === strpos($pathinfo, '/en/planes')) {
                        // en__RG__planes
                        if ('/en/planes' === $pathinfo) {
                            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::planesAction',  '_locale' => 'en',  '_route' => 'en__RG__planes',);
                        }

                        // en__RG__checkout
                        if ('/en/planes_listapp' === $pathinfo) {
                            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::checkoutAction',  '_locale' => 'en',  '_route' => 'en__RG__checkout',);
                        }

                    }

                    // en__RG__perfil
                    if ('/en/perfil' === $pathinfo) {
                        return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::perfilAction',  '_locale' => 'en',  '_route' => 'en__RG__perfil',);
                    }

                    if (0 === strpos($pathinfo, '/en/politica')) {
                        // en__RG__politicas
                        if ('/en/politicas' === $pathinfo) {
                            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::politicasAction',  '_locale' => 'en',  '_route' => 'en__RG__politicas',);
                        }

                        // en__RG__politica
                        if ('/en/politica' === $pathinfo) {
                            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::politicaAction',  '_locale' => 'en',  '_route' => 'en__RG__politica',);
                        }

                    }

                    elseif (0 === strpos($pathinfo, '/en/profile')) {
                        // en__RG__fos_user_profile_show
                        if ('/en/profile/' === $pathinfo) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__fos_user_profile_show;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_profile_show',);
                        }
                        not_en__RG__fos_user_profile_show:

                        // en__RG__fos_user_profile_edit
                        if ('/en/profile/edit' === $pathinfo) {
                            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                                $allow = array_merge($allow, array('GET', 'POST'));
                                goto not_en__RG__fos_user_profile_edit;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_profile_edit',);
                        }
                        not_en__RG__fos_user_profile_edit:

                        // en__RG__fos_user_change_password
                        if ('/en/profile/change-password' === $pathinfo) {
                            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                                $allow = array_merge($allow, array('GET', 'POST'));
                                goto not_en__RG__fos_user_change_password;
                            }

                            return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_change_password',);
                        }
                        not_en__RG__fos_user_change_password:

                    }

                }

                elseif (0 === strpos($pathinfo, '/en/re')) {
                    // en__RG__restaurantes
                    if ('/en/restaurantes' === $pathinfo) {
                        return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::restaurantesAction',  '_locale' => 'en',  '_route' => 'en__RG__restaurantes',);
                    }

                    if (0 === strpos($pathinfo, '/en/resetting')) {
                        // en__RG__fos_user_resetting_request
                        if ('/en/resetting/request' === $pathinfo) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__fos_user_resetting_request;
                            }

                            return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::requestAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_request',);
                        }
                        not_en__RG__fos_user_resetting_request:

                        // en__RG__fos_user_resetting_reset
                        if (0 === strpos($pathinfo, '/en/resetting/reset') && preg_match('#^/en/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                                $allow = array_merge($allow, array('GET', 'POST'));
                                goto not_en__RG__fos_user_resetting_reset;
                            }

                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__fos_user_resetting_reset')), array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::resetAction',  '_locale' => 'en',));
                        }
                        not_en__RG__fos_user_resetting_reset:

                        // en__RG__fos_user_resetting_send_email
                        if ('/en/resetting/send-email' === $pathinfo) {
                            if ('POST' !== $canonicalMethod) {
                                $allow[] = 'POST';
                                goto not_en__RG__fos_user_resetting_send_email;
                            }

                            return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::sendEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_send_email',);
                        }
                        not_en__RG__fos_user_resetting_send_email:

                        // en__RG__fos_user_resetting_check_email
                        if ('/en/resetting/check-email' === $pathinfo) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__fos_user_resetting_check_email;
                            }

                            return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::checkEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_resetting_check_email',);
                        }
                        not_en__RG__fos_user_resetting_check_email:

                    }

                    elseif (0 === strpos($pathinfo, '/en/register')) {
                        // en__RG__fos_user_registration_register
                        if ('/en/register/' === $pathinfo) {
                            if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                                $allow = array_merge($allow, array('GET', 'POST'));
                                goto not_en__RG__fos_user_registration_register;
                            }

                            return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_register',);
                        }
                        not_en__RG__fos_user_registration_register:

                        // en__RG__fos_user_registration_check_email
                        if ('/en/register/check-email' === $pathinfo) {
                            if ('GET' !== $canonicalMethod) {
                                $allow[] = 'GET';
                                goto not_en__RG__fos_user_registration_check_email;
                            }

                            return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::checkEmailAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_check_email',);
                        }
                        not_en__RG__fos_user_registration_check_email:

                        if (0 === strpos($pathinfo, '/en/register/confirm')) {
                            // en__RG__fos_user_registration_confirm
                            if (preg_match('#^/en/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                                if ('GET' !== $canonicalMethod) {
                                    $allow[] = 'GET';
                                    goto not_en__RG__fos_user_registration_confirm;
                                }

                                return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__fos_user_registration_confirm')), array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::confirmAction',  '_locale' => 'en',));
                            }
                            not_en__RG__fos_user_registration_confirm:

                            // en__RG__fos_user_registration_confirmed
                            if ('/en/register/confirmed' === $pathinfo) {
                                if ('GET' !== $canonicalMethod) {
                                    $allow[] = 'GET';
                                    goto not_en__RG__fos_user_registration_confirmed;
                                }

                                return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::confirmedAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_registration_confirmed',);
                            }
                            not_en__RG__fos_user_registration_confirmed:

                        }

                    }

                }

                // en__RG__ruta_imagen
                if (0 === strpos($pathinfo, '/en/ruta_imagen') && preg_match('#^/en/ruta_imagen/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__ruta_imagen')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::rutaImagenAction',  '_locale' => 'en',));
                }

                if (0 === strpos($pathinfo, '/en/mapa')) {
                    // en__RG__mapa
                    if ('/en/mapa' === $pathinfo) {
                        return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::mapaAction',  '_locale' => 'en',  '_route' => 'en__RG__mapa',);
                    }

                    // en__RG__sitemap
                    if ('/en/mapadelsitio' === $pathinfo) {
                        return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::siteAction',  '_locale' => 'en',  '_route' => 'en__RG__sitemap',);
                    }

                }

                // en__RG__modalimage
                if (0 === strpos($pathinfo, '/en/modalimage') && preg_match('#^/en/modalimage/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__modalimage')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::modalImageAction',  '_locale' => 'en',));
                }

                // en__RG__insumos
                if ('/en/insumos' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::insumoAction',  '_locale' => 'en',  '_route' => 'en__RG__insumos',);
                }

                // en__RG__terminos_y_condiciones
                if ('/en/terminos-y-condiciones' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::terminosAction',  '_locale' => 'en',  '_route' => 'en__RG__terminos_y_condiciones',);
                }

                // en__RG__test
                if ('/en/test' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::testAction',  '_locale' => 'en',  '_route' => 'en__RG__test',);
                }

                // en__RG__domicilios
                if ('/en/domicilios' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::domiciliosAction',  '_locale' => 'en',  '_route' => 'en__RG__domicilios',);
                }

                // en__RG__clientes_institucionales
                if ('/en/clientes-institucionales' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::clientesAction',  '_locale' => 'en',  '_route' => 'en__RG__clientes_institucionales',);
                }

                // en__RG__homepageold
                if ('/en/homeold' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'en',  '_route' => 'en__RG__homepageold',);
                }

                // en__RG__newsletter
                if (0 === strpos($pathinfo, '/en/newsletter') && preg_match('#^/en/newsletter/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__newsletter')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::newsletterAction',  '_locale' => 'en',));
                }

                // en__RG__ordenar
                if ('/en/ordenar' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::ordenarAction',  '_locale' => 'en',  '_route' => 'en__RG__ordenar',);
                }

                // en__RG__gracias
                if ('/en/gracias' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::graciasAction',  '_locale' => 'en',  '_route' => 'en__RG__gracias',);
                }

                if (0 === strpos($pathinfo, '/en/login')) {
                    // en__RG__fos_user_security_login
                    if ('/en/login' === $pathinfo) {
                        if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                            $allow = array_merge($allow, array('GET', 'POST'));
                            goto not_en__RG__fos_user_security_login;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_login',);
                    }
                    not_en__RG__fos_user_security_login:

                    // en__RG__fos_user_security_check
                    if ('/en/login_check' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_en__RG__fos_user_security_check;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_check',);
                    }
                    not_en__RG__fos_user_security_check:

                }

                elseif (0 === strpos($pathinfo, '/en/logout')) {
                    // en__RG__fos_user_security_logout
                    if ('/en/logout' === $pathinfo) {
                        if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                            $allow = array_merge($allow, array('GET', 'POST'));
                            goto not_en__RG__fos_user_security_logout;
                        }

                        return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'en',  '_route' => 'en__RG__fos_user_security_logout',);
                    }
                    not_en__RG__fos_user_security_logout:

                    // en__RG__user_logout
                    if ('/en/logout' === $pathinfo) {
                        return array (  '_locale' => 'en',  '_route' => 'en__RG__user_logout',);
                    }

                }

                // en__RG__ef_connect
                if (0 === strpos($pathinfo, '/en/efconnect') && preg_match('#^/en/efconnect(?:/(?P<instance>[^/]++)(?:/(?P<homeFolder>[^/]++))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__ef_connect')), array (  '_controller' => 'FM\\ElfinderBundle\\Controller\\ElFinderController::loadAction',  'instance' => 'default',  'homeFolder' => '',  '_locale' => 'en',));
                }

                // en__RG__elfinder
                if (0 === strpos($pathinfo, '/en/elfinder') && preg_match('#^/en/elfinder(?:/(?P<instance>[^/]++)(?:/(?P<homeFolder>[^/]++))?)?$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'en__RG__elfinder')), array (  '_controller' => 'FM\\ElfinderBundle\\Controller\\ElFinderController::showAction',  'instance' => 'default',  'homeFolder' => '',  '_locale' => 'en',));
                }

            }

            // es__RG__experiencias
            if (0 === strpos($pathinfo, '/experiencia') && preg_match('#^/experiencia/(?P<id>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__experiencias')), array (  '_controller' => 'ExperienciasBundle\\Controller\\DefaultController::experienciaAction',  '_locale' => 'es',));
            }

            // es__RG__ef_connect
            if (0 === strpos($pathinfo, '/efconnect') && preg_match('#^/efconnect(?:/(?P<instance>[^/]++)(?:/(?P<homeFolder>[^/]++))?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__ef_connect')), array (  '_controller' => 'FM\\ElfinderBundle\\Controller\\ElFinderController::loadAction',  'instance' => 'default',  'homeFolder' => '',  '_locale' => 'es',));
            }

            // es__RG__elfinder
            if (0 === strpos($pathinfo, '/elfinder') && preg_match('#^/elfinder(?:/(?P<instance>[^/]++)(?:/(?P<homeFolder>[^/]++))?)?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__elfinder')), array (  '_controller' => 'FM\\ElfinderBundle\\Controller\\ElFinderController::showAction',  'instance' => 'default',  'homeFolder' => '',  '_locale' => 'es',));
            }

        }

        // es__RG__listapp_default_index
        if ('/listapp' === $pathinfo) {
            return array (  '_controller' => 'ListappBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__listapp_default_index',);
        }

        if (0 === strpos($pathinfo, '/lo')) {
            // es__RG__listapp_default_load
            if ('/load' === $pathinfo) {
                return array (  '_controller' => 'ListappBundle\\Controller\\DefaultController::loadAction',  '_locale' => 'es',  '_route' => 'es__RG__listapp_default_load',);
            }

            if (0 === strpos($pathinfo, '/login')) {
                // es__RG__registro_login_inicial
                if ('/login' === $pathinfo) {
                    return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerNewAction',  '_locale' => 'es',  '_route' => 'es__RG__registro_login_inicial',);
                }

                // es__RG__fos_user_security_login
                if ('/login' === $pathinfo) {
                    if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                        $allow = array_merge($allow, array('GET', 'POST'));
                        goto not_es__RG__fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_login',);
                }
                not_es__RG__fos_user_security_login:

                // es__RG__fos_user_security_check
                if ('/login_check' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_check',);
                }
                not_es__RG__fos_user_security_check:

            }

            elseif (0 === strpos($pathinfo, '/logout')) {
                // es__RG__fos_user_security_logout
                if ('/logout' === $pathinfo) {
                    if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                        $allow = array_merge($allow, array('GET', 'POST'));
                        goto not_es__RG__fos_user_security_logout;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_security_logout',);
                }
                not_es__RG__fos_user_security_logout:

                // es__RG__user_logout
                if ('/logout' === $pathinfo) {
                    return array (  '_locale' => 'es',  '_route' => 'es__RG__user_logout',);
                }

            }

        }

        // es__RG__nosotros
        if ('/nosotros' === $pathinfo) {
            return array (  '_controller' => 'NosotrosBundle\\Controller\\DefaultController::nosotrosAction',  '_locale' => 'es',  '_route' => 'es__RG__nosotros',);
        }

        // es__RG__newsletter
        if (0 === strpos($pathinfo, '/newsletter') && preg_match('#^/newsletter/(?P<email>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__newsletter')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::newsletterAction',  '_locale' => 'es',));
        }

        if (0 === strpos($pathinfo, '/t')) {
            if (0 === strpos($pathinfo, '/test')) {
                // es__RG__test_mail
                if ('/test-mail' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::testMailAction',  '_locale' => 'es',  '_route' => 'es__RG__test_mail',);
                }

                // es__RG__test
                if ('/test' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::testAction',  '_locale' => 'es',  '_route' => 'es__RG__test',);
                }

            }

            // es__RG__terminos_y_condiciones
            if ('/terminos-y-condiciones' === $pathinfo) {
                return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::terminosAction',  '_locale' => 'es',  '_route' => 'es__RG__terminos_y_condiciones',);
            }

            // es__RG__tarjetas
            if ('/tarjetas' === $pathinfo) {
                return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::tarjetasAction',  '_locale' => 'es',  '_route' => 'es__RG__tarjetas',);
            }

            // es__RG__tarjeta-omnitural
            if ('/tarjeta-omnitural' === $pathinfo) {
                return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::tarjetaAction',  '_locale' => 'es',  '_route' => 'es__RG__tarjeta-omnitural',);
            }

            // es__RG__tokenize
            if ('/tokenize' === $pathinfo) {
                return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::tokenCardAction',  '_locale' => 'es',  '_route' => 'es__RG__tokenize',);
            }

            // es__RG__home
            if ('/the_store' === $pathinfo) {
                return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::homeAction',  '_locale' => 'es',  '_route' => 'es__RG__home',);
            }

            // es__RG__trabaje
            if ('/trabaje-con-nosotros' === $pathinfo) {
                return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::trabajeAction',  '_locale' => 'es',  '_route' => 'es__RG__trabaje',);
            }

        }

        elseif (0 === strpos($pathinfo, '/re')) {
            if (0 === strpos($pathinfo, '/reporte-')) {
                if (0 === strpos($pathinfo, '/reporte-restaurante')) {
                    // es__RG__reporte_restaurantes
                    if ('/reporte-restaurantes' === $pathinfo) {
                        return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteRestaurantesAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_restaurantes',);
                    }

                    // es__RG__reporte_restaurante
                    if ('/reporte-restaurante' === $pathinfo) {
                        return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteCategoriacAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_restaurante',);
                    }

                }

                // es__RG__reporte_pedidos_hoy
                if ('/reporte-pedido-hoy' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reportePedidosHoyAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_pedidos_hoy',);
                }

                // es__RG__reporte_producto
                if ('/reporte-producto' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteProductcAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_producto',);
                }

                // es__RG__reporte_total_vendido
                if ('/reporte-total-vendido' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteTotalAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_total_vendido',);
                }

                // es__RG__reporte_completo
                if ('/reporte-completo' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteCompletoAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_completo',);
                }

                if (0 === strpos($pathinfo, '/reporte-dia')) {
                    // es__RG__reporte_diario
                    if ('/reporte-diario' === $pathinfo) {
                        return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteDirarioAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_diario',);
                    }

                    // es__RG__reporte_dia
                    if ('/reporte-dia' === $pathinfo) {
                        return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteDiaAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_dia',);
                    }

                }

                // es__RG__reporte_mensual
                if ('/reporte-mensual' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteMensualAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_mensual',);
                }

                // es__RG__reporte_estado
                if ('/reporte-estados' === $pathinfo) {
                    return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::reporteEstadoAction',  '_locale' => 'es',  '_route' => 'es__RG__reporte_estado',);
                }

            }

            // es__RG__registro_login
            if ('/registro' === $pathinfo) {
                return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'es',  '_route' => 'es__RG__registro_login',);
            }

            if (0 === strpos($pathinfo, '/register')) {
                // es__RG__fos_user_registration_register
                if ('/register/' === $pathinfo) {
                    if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                        $allow = array_merge($allow, array('GET', 'POST'));
                        goto not_es__RG__fos_user_registration_register;
                    }

                    return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::registerAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_register',);
                }
                not_es__RG__fos_user_registration_register:

                // es__RG__fos_user_registration_check_email
                if ('/register/check-email' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_es__RG__fos_user_registration_check_email;
                    }

                    return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::checkEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_check_email',);
                }
                not_es__RG__fos_user_registration_check_email:

                if (0 === strpos($pathinfo, '/register/confirm')) {
                    // es__RG__fos_user_registration_confirm
                    if (preg_match('#^/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__fos_user_registration_confirm;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__fos_user_registration_confirm')), array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::confirmAction',  '_locale' => 'es',));
                    }
                    not_es__RG__fos_user_registration_confirm:

                    // es__RG__fos_user_registration_confirmed
                    if ('/register/confirmed' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__fos_user_registration_confirmed;
                        }

                        return array (  '_controller' => 'UserIridianBundle\\Controller\\RegistrationController::confirmedAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_registration_confirmed',);
                    }
                    not_es__RG__fos_user_registration_confirmed:

                }

            }

            // es__RG__recetas_prod
            if (0 === strpos($pathinfo, '/recetas-producto') && preg_match('#^/recetas\\-producto/(?P<productoId>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__recetas_prod')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::recetasprodAction',  '_locale' => 'es',));
            }

            // es__RG__remove_carrito_bono
            if (0 === strpos($pathinfo, '/remove-carrito-bono') && preg_match('#^/remove\\-carrito\\-bono/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__remove_carrito_bono')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::removeCarritoBonoAction',  '_locale' => 'es',));
            }

            if (0 === strpos($pathinfo, '/res')) {
                // es__RG__resumen
                if ('/resumen' === $pathinfo) {
                    return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::resumenAction',  '_locale' => 'es',  '_route' => 'es__RG__resumen',);
                }

                // es__RG__restaurantes
                if ('/restaurantes' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::restaurantesAction',  '_locale' => 'es',  '_route' => 'es__RG__restaurantes',);
                }

                if (0 === strpos($pathinfo, '/resetting')) {
                    // es__RG__fos_user_resetting_request
                    if ('/resetting/request' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__fos_user_resetting_request;
                        }

                        return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::requestAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_request',);
                    }
                    not_es__RG__fos_user_resetting_request:

                    // es__RG__fos_user_resetting_reset
                    if (0 === strpos($pathinfo, '/resetting/reset') && preg_match('#^/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                        if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                            $allow = array_merge($allow, array('GET', 'POST'));
                            goto not_es__RG__fos_user_resetting_reset;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__fos_user_resetting_reset')), array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::resetAction',  '_locale' => 'es',));
                    }
                    not_es__RG__fos_user_resetting_reset:

                    // es__RG__fos_user_resetting_send_email
                    if ('/resetting/send-email' === $pathinfo) {
                        if ('POST' !== $canonicalMethod) {
                            $allow[] = 'POST';
                            goto not_es__RG__fos_user_resetting_send_email;
                        }

                        return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::sendEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_send_email',);
                    }
                    not_es__RG__fos_user_resetting_send_email:

                    // es__RG__fos_user_resetting_check_email
                    if ('/resetting/check-email' === $pathinfo) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__fos_user_resetting_check_email;
                        }

                        return array (  '_controller' => 'UserIridianBundle\\Controller\\ResettingController::checkEmailAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_resetting_check_email',);
                    }
                    not_es__RG__fos_user_resetting_check_email:

                }

            }

        }

        // es__RG__ruta_imagen
        if (0 === strpos($pathinfo, '/ruta_imagen') && preg_match('#^/ruta_imagen/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__ruta_imagen')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::rutaImagenAction',  '_locale' => 'es',));
        }

        if (0 === strpos($pathinfo, '/m')) {
            // es__RG__mailprueba
            if ('/mailprueba' === $pathinfo) {
                return array (  '_controller' => 'ReportesBundle\\Controller\\DefaultController::mailpruebaAction',  '_locale' => 'es',  '_route' => 'es__RG__mailprueba',);
            }

            if (0 === strpos($pathinfo, '/mapa')) {
                // es__RG__mapa
                if ('/mapa' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::mapaAction',  '_locale' => 'es',  '_route' => 'es__RG__mapa',);
                }

                // es__RG__sitemap
                if ('/mapadelsitio' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::siteAction',  '_locale' => 'es',  '_route' => 'es__RG__sitemap',);
                }

            }

            elseif (0 === strpos($pathinfo, '/me')) {
                if (0 === strpos($pathinfo, '/media/cache/resolve')) {
                    // es__RG__liip_imagine_filter_runtime
                    if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/rc/(?P<hash>[^/]++)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__liip_imagine_filter_runtime;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__liip_imagine_filter_runtime')), array (  '_controller' => 'liip_imagine.controller:filterRuntimeAction',  '_locale' => 'es',));
                    }
                    not_es__RG__liip_imagine_filter_runtime:

                    // es__RG__liip_imagine_filter
                    if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                        if ('GET' !== $canonicalMethod) {
                            $allow[] = 'GET';
                            goto not_es__RG__liip_imagine_filter;
                        }

                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__liip_imagine_filter')), array (  '_controller' => 'liip_imagine.controller:filterAction',  '_locale' => 'es',));
                    }
                    not_es__RG__liip_imagine_filter:

                }

                // es__RG__mensaje
                if ('/mensaje' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::mensajeAction',  '_locale' => 'es',  '_route' => 'es__RG__mensaje',);
                }

                // es__RG__message
                if ('/message' === $pathinfo) {
                    return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::messageAction',  '_locale' => 'es',  '_route' => 'es__RG__message',);
                }

            }

            // es__RG__modalimage
            if (0 === strpos($pathinfo, '/modalimage') && preg_match('#^/modalimage/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__modalimage')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::modalImageAction',  '_locale' => 'es',));
            }

        }

        elseif (0 === strpos($pathinfo, '/p')) {
            if (0 === strpos($pathinfo, '/pagar-payu')) {
                // es__RG__pagar_payu
                if ('/pagar-payu' === $pathinfo) {
                    return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__pagar_payu',);
                }

                // es__RG__pagar_payu_respuesta
                if ('/pagar-payu/respuesta' === $pathinfo) {
                    return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::respuestaAction',  '_locale' => 'es',  '_route' => 'es__RG__pagar_payu_respuesta',);
                }

                // es__RG__pagar_payu_confirmacion
                if ('/pagar-payu/confirmacion' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_es__RG__pagar_payu_confirmacion;
                    }

                    return array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::confirmacionAction',  '_locale' => 'es',  '_route' => 'es__RG__pagar_payu_confirmacion',);
                }
                not_es__RG__pagar_payu_confirmacion:

            }

            // es__RG__pay
            if (0 === strpos($pathinfo, '/pay') && preg_match('#^/pay/(?P<id_token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__pay')), array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::PayAction',  '_locale' => 'es',));
            }

            // es__RG__post
            if (0 === strpos($pathinfo, '/post') && preg_match('#^/post/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__post')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::postAction',  '_locale' => 'es',));
            }

            if (0 === strpos($pathinfo, '/politica')) {
                // es__RG__politicas
                if ('/politicas' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::politicasAction',  '_locale' => 'es',  '_route' => 'es__RG__politicas',);
                }

                // es__RG__politica
                if ('/politica' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::politicaAction',  '_locale' => 'es',  '_route' => 'es__RG__politica',);
                }

            }

            elseif (0 === strpos($pathinfo, '/product')) {
                // es__RG__productos
                if ('/productos' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosAction',  '_locale' => 'es',  '_route' => 'es__RG__productos',);
                }

                if (0 === strpos($pathinfo, '/productos_por_')) {
                    // es__RG__productos_por_categoria
                    if (0 === strpos($pathinfo, '/productos_por_categoria') && preg_match('#^/productos_por_categoria/(?P<categoria>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__productos_por_categoria')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosCategoriaAction',  '_locale' => 'es',));
                    }

                    // es__RG__productos_por_genero
                    if (0 === strpos($pathinfo, '/productos_por_genero') && preg_match('#^/productos_por_genero/(?P<genero>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__productos_por_genero')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosGenAction',  '_locale' => 'es',));
                    }

                    // es__RG__productos_por_gen_cat
                    if (0 === strpos($pathinfo, '/productos_por_gen_cat') && preg_match('#^/productos_por_gen_cat/(?P<genero>[^/]++)/(?P<categoria>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__productos_por_gen_cat')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productosCatGenAction',  '_locale' => 'es',));
                    }

                }

                // es__RG__product
                if (preg_match('#^/product/(?P<id>[^/]++)/(?P<nombre>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__product')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::productAction',  '_locale' => 'es',));
                }

            }

            elseif (0 === strpos($pathinfo, '/profile')) {
                // es__RG__fos_user_profile_show
                if ('/profile/' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_es__RG__fos_user_profile_show;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_profile_show',);
                }
                not_es__RG__fos_user_profile_show:

                // es__RG__fos_user_profile_edit
                if ('/profile/edit' === $pathinfo) {
                    if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                        $allow = array_merge($allow, array('GET', 'POST'));
                        goto not_es__RG__fos_user_profile_edit;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_profile_edit',);
                }
                not_es__RG__fos_user_profile_edit:

                // es__RG__fos_user_change_password
                if ('/profile/change-password' === $pathinfo) {
                    if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                        $allow = array_merge($allow, array('GET', 'POST'));
                        goto not_es__RG__fos_user_change_password;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',  '_locale' => 'es',  '_route' => 'es__RG__fos_user_change_password',);
                }
                not_es__RG__fos_user_change_password:

            }

            elseif (0 === strpos($pathinfo, '/planes')) {
                // es__RG__planes
                if ('/planes' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::planesAction',  '_locale' => 'es',  '_route' => 'es__RG__planes',);
                }

                // es__RG__checkout
                if ('/planes_listapp' === $pathinfo) {
                    return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::checkoutAction',  '_locale' => 'es',  '_route' => 'es__RG__checkout',);
                }

            }

            // es__RG__perfil
            if ('/perfil' === $pathinfo) {
                return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::perfilAction',  '_locale' => 'es',  '_route' => 'es__RG__perfil',);
            }

        }

        // es__RG__untokenize
        if (0 === strpos($pathinfo, '/untokenize') && preg_match('#^/untokenize/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__untokenize')), array (  '_controller' => 'PagosPayuBundle\\Controller\\DefaultController::unTokenCardAction',  '_locale' => 'es',));
        }

        // es__RG__useriridian_default_index
        if ('/useriridian' === $pathinfo) {
            return array (  '_controller' => 'UserIridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__useriridian_default_index',);
        }

        // es__RG__the_blog
        if ('/blog' === $pathinfo) {
            return array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__the_blog',);
        }

        // es__RG__busca_productos
        if ('/buscador-productos' === $pathinfo) {
            return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::buscadorAction',  '_locale' => 'es',  '_route' => 'es__RG__busca_productos',);
        }

        // es__RG__video-post
        if (0 === strpos($pathinfo, '/video-post') && preg_match('#^/video\\-post/(?P<id>[^/]++)/(?P<name>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__video-post')), array (  '_controller' => 'BlogiridianBundle\\Controller\\DefaultController::videoPostAction',  '_locale' => 'es',));
        }

        if (0 === strpos($pathinfo, '/c')) {
            // es__RG__carrito
            if ('/carrito-de-compras' === $pathinfo) {
                return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__carrito',);
            }

            // es__RG__carrito-completo
            if ('/carrito-completo' === $pathinfo) {
                return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::cartAction',  '_locale' => 'es',  '_route' => 'es__RG__carrito-completo',);
            }

            // es__RG__ciudadesByDept
            if (0 === strpos($pathinfo, '/ciudades-dept') && preg_match('#^/ciudades\\-dept/(?P<id>[^/]++)(?:/(?P<id_ciudad>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__ciudadesByDept')), array (  'id_ciudad' => 'a',  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::ciudadesByDeptAction',  '_locale' => 'es',));
            }

            // es__RG__contacto
            if ('/contacto' === $pathinfo) {
                return array (  '_controller' => 'ContactoBundle\\Controller\\DefaultController::contactoAction',  '_locale' => 'es',  '_route' => 'es__RG__contacto',);
            }

            // es__RG__clientes_institucionales
            if ('/clientes-institucionales' === $pathinfo) {
                return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::clientesAction',  '_locale' => 'es',  '_route' => 'es__RG__clientes_institucionales',);
            }

        }

        elseif (0 === strpos($pathinfo, '/d')) {
            // es__RG__datos
            if ('/datos' === $pathinfo) {
                return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::datosAction',  '_locale' => 'es',  '_route' => 'es__RG__datos',);
            }

            if (0 === strpos($pathinfo, '/direccion')) {
                // es__RG__direccion
                if ('/direccion' === $pathinfo) {
                    return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::direccionAction',  '_locale' => 'es',  '_route' => 'es__RG__direccion',);
                }

                // es__RG__direccion_sesion
                if (0 === strpos($pathinfo, '/direccion-sesion') && preg_match('#^/direccion\\-sesion/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__direccion_sesion')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::direccionSesionAction',  '_locale' => 'es',));
                }

            }

            // es__RG__domicilios
            if ('/domicilios' === $pathinfo) {
                return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::domiciliosAction',  '_locale' => 'es',  '_route' => 'es__RG__domicilios',);
            }

        }

        // es__RG__set_carrito_talla
        if (0 === strpos($pathinfo, '/set-carrito-talla') && preg_match('#^/set\\-carrito\\-talla/(?P<id>[^/]++)/(?P<cant>[^/]++)/(?P<id_talla>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'es__RG__set_carrito_talla')), array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::setCarritoTallaAction',  '_locale' => 'es',));
        }

        // es__RG__wishlist
        if ('/wishlist' === $pathinfo) {
            return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::wishlistAction',  '_locale' => 'es',  '_route' => 'es__RG__wishlist',);
        }

        // es__RG__gift
        if ('/gift' === $pathinfo) {
            return array (  '_controller' => 'CarroiridianBundle\\Controller\\DefaultController::giftAction',  '_locale' => 'es',  '_route' => 'es__RG__gift',);
        }

        // es__RG__gracias
        if ('/gracias' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::graciasAction',  '_locale' => 'es',  '_route' => 'es__RG__gracias',);
        }

        // es__RG__homepage
        if ('' === $trimmedPathinfo) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'es__RG__homepage');
            }

            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__homepage',);
        }

        // es__RG__insumos
        if ('/insumos' === $pathinfo) {
            return array (  '_controller' => 'HomeBundle\\Controller\\DefaultController::insumoAction',  '_locale' => 'es',  '_route' => 'es__RG__insumos',);
        }

        // es__RG__homepageold
        if ('/homeold' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_locale' => 'es',  '_route' => 'es__RG__homepageold',);
        }

        // es__RG__ordenar
        if ('/ordenar' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::ordenarAction',  '_locale' => 'es',  '_route' => 'es__RG__ordenar',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
