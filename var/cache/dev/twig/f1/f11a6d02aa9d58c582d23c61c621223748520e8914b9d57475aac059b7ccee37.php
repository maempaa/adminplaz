<?php

/* ReportesBundle:Default:reporte.html.twig */
class __TwigTemplate_10369d6b58937a0d46a7843b5541672898ea5ec09db1466e86798d77b1f3f368 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = array(
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return $this->loadTemplate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 2, $this->source); })()), "templates", array()), "layout", array()), "ReportesBundle:Default:reporte.html.twig", 2);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ReportesBundle:Default:reporte.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ReportesBundle:Default:reporte.html.twig"));

        // line 1
        $context["_entity_config"] = $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->getEntityConfiguration("Inventario");
        // line 6
        $context["ruta"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 6, $this->source); })()), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 4
        echo "    Reportes
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_main($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 9
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 10
        echo "        <h1>";
        echo twig_escape_filter($this->env, (isset($context["titulo"]) || array_key_exists("titulo", $context) ? $context["titulo"] : (function () { throw new Twig_Error_Runtime('Variable "titulo" does not exist.', 10, $this->source); })()), "html", null, true);
        echo "</h1>
        <br/><br/>
        <div class=\"row\">
            <form action=\"";
        // line 13
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 13, $this->source); })()), "request", array()), "get", array(0 => "_route"), "method"));
        echo "\" method=\"get\" id=\"rango\" style=\"display: none\">
                <input type=\"text\" id=\"start\" name=\"start\">
                <input type=\"text\" id=\"end\" name=\"end\">
                <input type=\"text\" name=\"menuIndex\" value=\"11\">
                <input type=\"text\" name=\"submenuIndex\" value=\"6\">
            </form>
            ";
        // line 19
        if ((((isset($context["ruta"]) || array_key_exists("ruta", $context) ? $context["ruta"] : (function () { throw new Twig_Error_Runtime('Variable "ruta" does not exist.', 19, $this->source); })()) == "reporte_diario") || ((isset($context["ruta"]) || array_key_exists("ruta", $context) ? $context["ruta"] : (function () { throw new Twig_Error_Runtime('Variable "ruta" does not exist.', 19, $this->source); })()) == "reporte_mensual"))) {
            // line 20
            echo "                <div class=\"col-sm-12\">
                    <div id=\"piechart\" style=\"width: 100%; height: 500px;\"></div>
                </div>
                <div class=\"col-sm-12\">
                    <br>
                    <div id=\"reportrange\" class=\"pull-right\" style=\"background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto;margin: 0 0 20px\">
                        <i class=\"glyphicon glyphicon-calendar fa fa-calendar\"></i>&nbsp;
                        <span></span> <b class=\"caret\"></b>
                    </div>
                    <br>
                </div>

            ";
        }
        // line 33
        echo "            <div class=\"col-sm-3\">
                <table class=\"table table-striped table-bordered\" id=\"reporte_bar\">
                    <thead>
                    <tr>
                        <td><strong>Fecha</strong></td>
                        <td><strong>Precio</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reporte"]) || array_key_exists("reporte", $context) ? $context["reporte"] : (function () { throw new Twig_Error_Runtime('Variable "reporte" does not exist.', 42, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 43
            echo "                        <tr>

                            <td>";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "fecha", array()), "html", null, true);
            echo "</td>
                            <td>\$";
            // line 46
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["item"], "precio", array()), 0, "."), "html", null, true);
            echo "</td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 49
        echo "                    </tbody>
                </table>
            </div>
        </div>

    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 57
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 58
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "
    ";
        // line 59
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ReportesBundle:Default:reporte.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 59,  188 => 58,  179 => 57,  164 => 49,  155 => 46,  151 => 45,  147 => 43,  143 => 42,  132 => 33,  117 => 20,  115 => 19,  106 => 13,  99 => 10,  80 => 9,  71 => 8,  60 => 4,  51 => 3,  41 => 2,  39 => 6,  37 => 1,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity('Inventario') %}
{% extends _entity_config.templates.layout %}
{% block content_title %}
    Reportes
{% endblock %}
{% set ruta = app.request.get('_route') %}

{% block main %}
    {% block entity_form %}
        <h1>{{ titulo }}</h1>
        <br/><br/>
        <div class=\"row\">
            <form action=\"{{ path(app.request.get(\"_route\")) }}\" method=\"get\" id=\"rango\" style=\"display: none\">
                <input type=\"text\" id=\"start\" name=\"start\">
                <input type=\"text\" id=\"end\" name=\"end\">
                <input type=\"text\" name=\"menuIndex\" value=\"11\">
                <input type=\"text\" name=\"submenuIndex\" value=\"6\">
            </form>
            {% if ruta == 'reporte_diario' or ruta == 'reporte_mensual'  %}
                <div class=\"col-sm-12\">
                    <div id=\"piechart\" style=\"width: 100%; height: 500px;\"></div>
                </div>
                <div class=\"col-sm-12\">
                    <br>
                    <div id=\"reportrange\" class=\"pull-right\" style=\"background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: auto;margin: 0 0 20px\">
                        <i class=\"glyphicon glyphicon-calendar fa fa-calendar\"></i>&nbsp;
                        <span></span> <b class=\"caret\"></b>
                    </div>
                    <br>
                </div>

            {% endif %}
            <div class=\"col-sm-3\">
                <table class=\"table table-striped table-bordered\" id=\"reporte_bar\">
                    <thead>
                    <tr>
                        <td><strong>Fecha</strong></td>
                        <td><strong>Precio</strong></td>
                    </tr>
                    </thead>
                    <tbody>
                    {% for item in reporte %}
                        <tr>

                            <td>{{ item.fecha }}</td>
                            <td>\${{ item.precio | number_format(0,'.') }}</td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>

    {% endblock entity_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}
    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "ReportesBundle:Default:reporte.html.twig", "E:\\dev\\plaz\\src\\ReportesBundle/Resources/views/Default/reporte.html.twig");
    }
}
