<?php

/* ReportesBundle:Default:reporte_pedidos_hoy.html.twig */
class __TwigTemplate_b5355820cb350350e535ba3fa1b8470f8e807d72c3d36a8729bead470b63759d extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = array(
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return $this->loadTemplate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 2, $this->source); })()), "templates", array()), "layout", array()), "ReportesBundle:Default:reporte_pedidos_hoy.html.twig", 2);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ReportesBundle:Default:reporte_pedidos_hoy.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ReportesBundle:Default:reporte_pedidos_hoy.html.twig"));

        // line 1
        $context["_entity_config"] = $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->getEntityConfiguration("Inventario");
        // line 2
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 4
        echo "    Reportes
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 9
        $context["actual"] = 0;
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["pedidos"]) || array_key_exists("pedidos", $context) ? $context["pedidos"] : (function () { throw new Twig_Error_Runtime('Variable "pedidos" does not exist.', 10, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["pedido"]) {
            // line 11
            echo "    ";
            if (((isset($context["actual"]) || array_key_exists("actual", $context) ? $context["actual"] : (function () { throw new Twig_Error_Runtime('Variable "actual" does not exist.', 11, $this->source); })()) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["pedido"], "plato", array()), "restaurante", array()), "id", array()))) {
                // line 12
                echo "
        <h1 style='font-family: Helvetica, Arial, sans-serif;color: #2ca397'>Restaurante: ";
                // line 13
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["pedido"], "plato", array()), "restaurante", array()), "nombre", array()), "html", null, true);
                echo "</h1>
        <p style='font-family: Helvetica, Arial, sans-serif;'>Tiene Listapps para hoy ";
                // line 14
                echo twig_escape_filter($this->env, (isset($context["hoy"]) || array_key_exists("hoy", $context) ? $context["hoy"] : (function () { throw new Twig_Error_Runtime('Variable "hoy" does not exist.', 14, $this->source); })()), "html", null, true);
                echo " (AAAA-MM-DD)</p><br>
        <table  cellspacing='0' cellpadding='10' border='0' style='border: 2px solid #2ca397;'>
            <thead style='background-color: #2ca397;color: #fff;font-family: Helvetica, Arial, sans-serif;'>
            <tr style='padding: 13px;text-align: left;'>
                <th style='padding: 13px;text-align: left;'>Id</th>
                <th style='padding: 13px;text-align: left;'>Plato</th>
                <th style='padding: 13px;text-align: left;'>Hora</th>
                <th style='padding: 13px;text-align: left;'>Cliente</th>
            </tr></thead>
            <tbody>
    ";
            }
            // line 25
            echo "
    <tr>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pedido"], "id", array()), "html", null, true);
            echo "</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pedido"], "plato", array()), "html", null, true);
            echo "</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>";
            // line 29
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pedido"], "fecha", array()), "H:i"), "html", null, true);
            echo "</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>";
            // line 30
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["pedido"], "usuario", array()), "html", null, true);
            echo "</td>
    </tr>
    ";
            // line 32
            if ((((isset($context["actual"]) || array_key_exists("actual", $context) ? $context["actual"] : (function () { throw new Twig_Error_Runtime('Variable "actual" does not exist.', 32, $this->source); })()) != twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["pedido"], "plato", array()), "restaurante", array()), "id", array())) && ((isset($context["actual"]) || array_key_exists("actual", $context) ? $context["actual"] : (function () { throw new Twig_Error_Runtime('Variable "actual" does not exist.', 32, $this->source); })()) != 0))) {
                // line 33
                echo "            </tbody>
        </table><br><br>
    ";
            }
            // line 36
            echo "    ";
            $context["actual"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["pedido"], "plato", array()), "restaurante", array()), "id", array());
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pedido'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 41
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 42
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "ReportesBundle:Default:reporte_pedidos_hoy.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 42,  172 => 41,  162 => 38,  155 => 36,  150 => 33,  148 => 32,  143 => 30,  139 => 29,  135 => 28,  131 => 27,  127 => 25,  113 => 14,  109 => 13,  106 => 12,  103 => 11,  99 => 10,  97 => 9,  78 => 8,  69 => 7,  58 => 4,  49 => 3,  39 => 2,  37 => 1,  25 => 2,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity('Inventario') %}
{% extends _entity_config.templates.layout %}
{% block content_title %}
    Reportes
{% endblock %}

{% block main %}
    {% block entity_form %}
{% set actual=0 %}
{% for pedido in pedidos %}
    {% if actual != pedido.plato.restaurante.id %}

        <h1 style='font-family: Helvetica, Arial, sans-serif;color: #2ca397'>Restaurante: {{ pedido.plato.restaurante.nombre}}</h1>
        <p style='font-family: Helvetica, Arial, sans-serif;'>Tiene Listapps para hoy {{ hoy }} (AAAA-MM-DD)</p><br>
        <table  cellspacing='0' cellpadding='10' border='0' style='border: 2px solid #2ca397;'>
            <thead style='background-color: #2ca397;color: #fff;font-family: Helvetica, Arial, sans-serif;'>
            <tr style='padding: 13px;text-align: left;'>
                <th style='padding: 13px;text-align: left;'>Id</th>
                <th style='padding: 13px;text-align: left;'>Plato</th>
                <th style='padding: 13px;text-align: left;'>Hora</th>
                <th style='padding: 13px;text-align: left;'>Cliente</th>
            </tr></thead>
            <tbody>
    {% endif %}

    <tr>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>{{ pedido.id }}</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>{{ pedido.plato }}</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>{{ pedido.fecha|date('H:i') }}</td>
        <td style='padding: 13px;text-align: left;font-family: Helvetica, Arial, sans-serif;border-bottom: 2px solid #2ca397;'>{{ pedido.usuario }}</td>
    </tr>
    {% if actual != pedido.plato.restaurante.id and actual != 0 %}
            </tbody>
        </table><br><br>
    {% endif %}
    {% set actual = pedido.plato.restaurante.id  %}
{% endfor %}
    {% endblock entity_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}
{% endblock %}", "ReportesBundle:Default:reporte_pedidos_hoy.html.twig", "E:\\dev\\plaz\\src\\ReportesBundle/Resources/views/Default/reporte_pedidos_hoy.html.twig");
    }
}
