<?php

/* AppBundle::inventarios.html.twig */
class __TwigTemplate_1d075346d4728a80c1201df1636a7e1547c10a4b766f3bdbe26ceb02716caa0b extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'body_class' => array($this, 'block_body_class'),
            'content_title' => array($this, 'block_content_title'),
            'main' => array($this, 'block_main'),
            'entity_form' => array($this, 'block_entity_form'),
            'body_javascript' => array($this, 'block_body_javascript'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 5
        return $this->loadTemplate(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 5, $this->source); })()), "templates", array()), "layout", array()), "AppBundle::inventarios.html.twig", 5);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::inventarios.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle::inventarios.html.twig"));

        // line 1
        $context["_entity_config"] = $this->extensions['EasyCorp\Bundle\EasyAdminBundle\Twig\EasyAdminTwigExtension']->getEntityConfiguration("Inventario");
        // line 2
        $context["__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217"] = twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 2, $this->source); })()), "translation_domain", array());
        // line 3
        $context["_trans_parameters"] = array("%entity_name%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 3, $this->source); })()), "name", array()), array(),         // line 2
(isset($context["__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217"]) || array_key_exists("__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217", $context) ? $context["__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217" does not exist.', 2, $this->source); })())), "%entity_label%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(twig_get_attribute($this->env, $this->source,         // line 3
(isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 3, $this->source); })()), "label", array()), array(),         // line 2
(isset($context["__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217"]) || array_key_exists("__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217", $context) ? $context["__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217"] : (function () { throw new Twig_Error_Runtime('Variable "__internal_75d5e9946e531356f71bd24676671f0766823aeecf166da792d9d1649e31f217" does not exist.', 2, $this->source); })())));
        // line 5
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 7
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo twig_escape_filter($this->env, ("easyadmin-new-" . twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 7, $this->source); })()), "name", array())), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 8
    public function block_body_class($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo twig_escape_filter($this->env, ("new new-" . twig_lower_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["_entity_config"]) || array_key_exists("_entity_config", $context) ? $context["_entity_config"] : (function () { throw new Twig_Error_Runtime('Variable "_entity_config" does not exist.', 8, $this->source); })()), "name", array()))), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 10
    public function block_content_title($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content_title"));

        // line 11
        echo "    Inventarios
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 14
    public function block_main($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 15
        echo "    ";
        $this->displayBlock('entity_form', $context, $blocks);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function block_entity_form($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "entity_form"));

        // line 16
        echo "        ";
        $context["tam"] = twig_length_filter($this->env, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 16, $this->source); })()));
        // line 17
        echo "        <a href=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("excel_generador_inventarios");
        echo "\" class=\"btn btn-primary\">Descargar formato base</a>
        <br/><br/>
        <table class=\"table table-striped table-bordered\" id=\"inventarios\">
            <tr class=\"ta_center\">
                <th></th>
                <th></th>
                ";
        // line 23
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 23, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
            // line 24
            echo "                    <th ";
            if ((isset($context["precio"]) || array_key_exists("precio", $context) ? $context["precio"] : (function () { throw new Twig_Error_Runtime('Variable "precio" does not exist.', 24, $this->source); })())) {
                echo "colspan=\"3\"";
            } else {
                echo "colspan=\"2\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, twig_get_attribute($this->env, $this->source, $context["talla"], "nombreEs", array())), "html", null, true);
            echo "</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "            </tr>
            <tr class=\"ta_center\">
                <th>SKU</th>
                <th>PRODUCTO</th>
                ";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 30, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["talla"]) {
            // line 31
            echo "                    ";
            if ((isset($context["precio"]) || array_key_exists("precio", $context) ? $context["precio"] : (function () { throw new Twig_Error_Runtime('Variable "precio" does not exist.', 31, $this->source); })())) {
                // line 32
                echo "                        <th>PRECIO</th>
                    ";
            }
            // line 34
            echo "                    <th colspan=\"2\">CANTIDAD</th>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['talla'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "            </tr>
            ";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["inventarios"]) || array_key_exists("inventarios", $context) ? $context["inventarios"] : (function () { throw new Twig_Error_Runtime('Variable "inventarios" does not exist.', 37, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["fila"]) {
            // line 38
            echo "                <tr data-producto-id=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["fila"], "producto_id", array(), "array"), "html", null, true);
            echo "\">
                    <td>";
            // line 39
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["fila"], "sku", array(), "array"), "html", null, true);
            echo "</td>
                    <td>";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["fila"], "nombre", array(), "array"), "html", null, true);
            echo "</td>
                    ";
            // line 41
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, ((isset($context["tam"]) || array_key_exists("tam", $context) ? $context["tam"] : (function () { throw new Twig_Error_Runtime('Variable "tam" does not exist.', 41, $this->source); })()) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 42
                echo "                        ";
                if (((isset($context["precio"]) || array_key_exists("precio", $context) ? $context["precio"] : (function () { throw new Twig_Error_Runtime('Variable "precio" does not exist.', 42, $this->source); })()) == 1)) {
                    // line 43
                    echo "                            <td>
                                <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                                <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                                <input data-tipo=\"precio\"   data-talla-id=\"";
                    // line 46
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 46, $this->source); })()), $context["i"], array(), "array"), "id", array()), "html", null, true);
                    echo "\" class=\"form-control precio talla_";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 46, $this->source); })()), $context["i"], array(), "array"), "id", array()), "html", null, true);
                    echo "\"   type=\"text\" value=\"";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["fila"], ("precio_" . ($context["i"] + 1)), array(), "array"), "html", null, true);
                    echo "\">
                            </td>
                        ";
                }
                // line 49
                echo "                        <td>
                            <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                            <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                            <input data-tipo=\"cantidad\" data-talla-id=\"";
                // line 52
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 52, $this->source); })()), $context["i"], array(), "array"), "id", array()), "html", null, true);
                echo "\" class=\"form-control cantidad talla_";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 52, $this->source); })()), $context["i"], array(), "array"), "id", array()), "html", null, true);
                echo "\" type=\"text\" value=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["fila"], ("cantidad_" . ($context["i"] + 1)), array(), "array"), "html", null, true);
                echo "\"></td>
                        <td class=\"vm\" data-talla-id=\"";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["tallas"]) || array_key_exists("tallas", $context) ? $context["tallas"] : (function () { throw new Twig_Error_Runtime('Variable "tallas" does not exist.', 53, $this->source); })()), $context["i"], array(), "array"), "id", array()), "html", null, true);
                echo "\"><a href=\"#\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i></a></td>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 55
            echo "                </tr>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fila'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 57
        echo "        </table>
    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 61
    public function block_body_javascript($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_javascript"));

        // line 62
        echo "    ";
        $this->displayParentBlock("body_javascript", $context, $blocks);
        echo "
    ";
        // line 63
        echo twig_include($this->env, $context, "@EasyAdmin/default/includes/_select2_widget.html.twig");
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle::inventarios.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  293 => 63,  288 => 62,  279 => 61,  268 => 57,  261 => 55,  253 => 53,  245 => 52,  240 => 49,  230 => 46,  225 => 43,  222 => 42,  218 => 41,  214 => 40,  210 => 39,  205 => 38,  201 => 37,  198 => 36,  191 => 34,  187 => 32,  184 => 31,  180 => 30,  174 => 26,  159 => 24,  155 => 23,  145 => 17,  142 => 16,  123 => 15,  114 => 14,  103 => 11,  94 => 10,  76 => 8,  58 => 7,  48 => 5,  46 => 2,  45 => 3,  44 => 2,  43 => 3,  41 => 2,  39 => 1,  27 => 5,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set _entity_config = easyadmin_entity('Inventario') %}
{% trans_default_domain _entity_config.translation_domain %}
{% set _trans_parameters = { '%entity_name%': _entity_config.name|trans, '%entity_label%': _entity_config.label|trans } %}

{% extends _entity_config.templates.layout %}

{% block body_id 'easyadmin-new-' ~ _entity_config.name %}
{% block body_class 'new new-' ~ _entity_config.name|lower %}

{% block content_title %}
    Inventarios
{% endblock %}

{% block main %}
    {% block entity_form %}
        {% set tam = tallas | length %}
        <a href=\"{{ path('excel_generador_inventarios') }}\" class=\"btn btn-primary\">Descargar formato base</a>
        <br/><br/>
        <table class=\"table table-striped table-bordered\" id=\"inventarios\">
            <tr class=\"ta_center\">
                <th></th>
                <th></th>
                {% for talla in tallas %}
                    <th {% if precio %}colspan=\"3\"{% else %}colspan=\"2\"{% endif %}>{{ talla.nombreEs | upper }}</th>
                {% endfor %}
            </tr>
            <tr class=\"ta_center\">
                <th>SKU</th>
                <th>PRODUCTO</th>
                {% for talla in tallas %}
                    {% if precio %}
                        <th>PRECIO</th>
                    {% endif %}
                    <th colspan=\"2\">CANTIDAD</th>
                {% endfor %}
            </tr>
            {% for fila in inventarios %}
                <tr data-producto-id=\"{{ fila['producto_id'] }}\">
                    <td>{{ fila['sku'] }}</td>
                    <td>{{ fila['nombre'] }}</td>
                    {% for i in 0..(tam-1) %}
                        {% if precio == 1 %}
                            <td>
                                <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                                <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                                <input data-tipo=\"precio\"   data-talla-id=\"{{ tallas[i].id }}\" class=\"form-control precio talla_{{ tallas[i].id }}\"   type=\"text\" value=\"{{ fila['precio_'~(i+1)] }}\">
                            </td>
                        {% endif %}
                        <td>
                            <span class=\"error\"><i class=\"fa fa-exclamation-circle\" aria-hidden=\"true\">Campo inválido</i></span>
                            <span class=\"guardando\"><i class=\"fa fa-spinner fa-pulse fa-fw\" aria-hidden=\"true\"></i>Guardando</span>
                            <input data-tipo=\"cantidad\" data-talla-id=\"{{ tallas[i].id }}\" class=\"form-control cantidad talla_{{ tallas[i].id }}\" type=\"text\" value=\"{{ fila['cantidad_'~(i+1)] }}\"></td>
                        <td class=\"vm\" data-talla-id=\"{{ tallas[i].id }}\"><a href=\"#\"><i class=\"fa fa-floppy-o\" aria-hidden=\"true\"></i></a></td>
                    {% endfor %}
                </tr>
            {% endfor %}
        </table>
    {% endblock entity_form %}
{% endblock %}

{% block body_javascript %}
    {{ parent() }}
    {{ include('@EasyAdmin/default/includes/_select2_widget.html.twig') }}
{% endblock %}
", "AppBundle::inventarios.html.twig", "E:\\dev\\plaz\\src\\AppBundle/Resources/views/inventarios.html.twig");
    }
}
