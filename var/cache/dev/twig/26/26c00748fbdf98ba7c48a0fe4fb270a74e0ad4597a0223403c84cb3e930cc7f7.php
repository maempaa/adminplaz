<?php

/* @Home/Default/header.html.twig */
class __TwigTemplate_a32fd32af8009c1261ad42b82a53998d4fe995248b538e37e3cb6c7da331b5e7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Home/Default/header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Home/Default/header.html.twig"));

        // line 1
        $context["ruta"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 1, $this->source); })()), "request", array()), "get", array(0 => "_route"), "method");
        // line 2
        $context["params"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 2, $this->source); })()), "request", array()), "attributes", array()), "get", array(0 => "_route_params"), "method");
        // line 3
        $context["params"] = twig_slice($this->env, (isset($context["params"]) || array_key_exists("params", $context) ? $context["params"] : (function () { throw new Twig_Error_Runtime('Variable "params" does not exist.', 3, $this->source); })()), 1);
        // line 4
        $context["lc"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 4, $this->source); })()), "request", array()), "locale", array());
        // line 5
        $context["categoria"] = twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 5, $this->source); })()), "request", array()), "get", array(0 => "categoria"), "method");
        // line 6
        echo "

<header id=\"menu\" class=\"menu\" role=\"Navigation\">
    <img class=\"menu__mobile-menu-btn md-hidden\" src=\"/images/mobile-menu-btn.svg\"  alt=\"mobile menu buttonl\"/>

    <div class=\"page-content menu__content\">
        <a class=\"menu__logo\" href=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\"><img src=\"/images/plaz.png\"  alt=\"logo\"/></a>
    </div>
</header>
<!-- / HEADER -->
";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "@Home/Default/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 12,  39 => 6,  37 => 5,  35 => 4,  33 => 3,  31 => 2,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% set ruta = app.request.get('_route') %}
{% set params = app.request.attributes.get('_route_params')  %}
{% set params = params |slice(1) %}
{% set lc = app.request.locale %}
{% set categoria = app.request.get('categoria') %}


<header id=\"menu\" class=\"menu\" role=\"Navigation\">
    <img class=\"menu__mobile-menu-btn md-hidden\" src=\"/images/mobile-menu-btn.svg\"  alt=\"mobile menu buttonl\"/>

    <div class=\"page-content menu__content\">
        <a class=\"menu__logo\" href=\"{{ path('homepage') }}\"><img src=\"/images/plaz.png\"  alt=\"logo\"/></a>
    </div>
</header>
<!-- / HEADER -->
", "@Home/Default/header.html.twig", "E:\\dev\\plaz\\src\\HomeBundle\\Resources\\views\\Default\\header.html.twig");
    }
}
