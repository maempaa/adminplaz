<?php

/* CarroiridianBundle:Default:datos.html.twig */
class __TwigTemplate_a2de76f124823b83a9fc2ea2aa35cc0e980b024650466ca25e362598bd2861bb extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("lay_iridian.html.twig", "CarroiridianBundle:Default:datos.html.twig", 1);
        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "lay_iridian.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CarroiridianBundle:Default:datos.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "CarroiridianBundle:Default:datos.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 2
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 3
        echo "    ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
    <link href=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/register.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 8
        echo "
    <main role=\"main\" class=\"section-padding page-content register\">
        <form class=\"register__form\" action=\"";
        // line 10
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_check");
        echo "\" method=\"post\" id=\"form_login\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) || array_key_exists("csrf_token", $context) ? $context["csrf_token"] : (function () { throw new Twig_Error_Runtime('Variable "csrf_token" does not exist.', 11, $this->source); })()), "html", null, true);
        echo "\" />
            <h1 class=\"text-2 black text-center top-space bottom-space\">BIENVENIDO A PLAZ MANAGER</h1>
            <div>
                ";
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 14, $this->source); })()), "session", array()), "flashbag", array()), "get", array(0 => "error"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["flashMessage"]) {
            // line 15
            echo "                    ";
            echo twig_escape_filter($this->env, $context["flashMessage"], "html", null, true);
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['flashMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "            </div>

            <label>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 19, $this->source); })()), "textoDB", array(0 => "correo"), "method"), "html", null, true);
        echo "</label>
            <input id=\"username\" name=\"_username\" required/>
            <label>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 21, $this->source); })()), "textoDB", array(0 => "contraseña"), "method"), "html", null, true);
        echo "</label>
            <input id=\"password\" name=\"_password\" type=\"password\" required/>

            <div class=\"register__form-remember text-center\">
                <input type=\"checkbox\" class=\"styled-checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\">
                <label for=\"remember_me\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 26, $this->source); })()), "textoDB", array(0 => "recordarme"), "method"), "html", null, true);
        echo "</label>
            </div>

            <button class=\"register__form-submit cta-button cta-button--dark\" type=\"submit\">";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 29, $this->source); })()), "textoDB", array(0 => "ingresar"), "method"), "html", null, true);
        echo "</button>
            <a href=\"";
        // line 30
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_resetting_request");
        echo "\" class=\"register__form-forgot\">";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 30, $this->source); })()), "textoDB", array(0 => "¿Olvidaste tu nombre de usuario o contraseña?"), "method"), "html", null, true);
        echo "</a>
        </form>
    </main>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "CarroiridianBundle:Default:datos.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 30,  130 => 29,  124 => 26,  116 => 21,  111 => 19,  107 => 17,  98 => 15,  94 => 14,  88 => 11,  84 => 10,  80 => 8,  71 => 7,  59 => 4,  54 => 3,  45 => 2,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'lay_iridian.html.twig' %}
{% block stylesheets %}
    {{ parent() }}
    <link href=\"{{ asset('css/register.css') }}\" rel=\"stylesheet\">
{% endblock %}

{% block content %}

    <main role=\"main\" class=\"section-padding page-content register\">
        <form class=\"register__form\" action=\"{{ path(\"fos_user_security_check\") }}\" method=\"post\" id=\"form_login\">
            <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token }}\" />
            <h1 class=\"text-2 black text-center top-space bottom-space\">BIENVENIDO A PLAZ MANAGER</h1>
            <div>
                {% for flashMessage in app.session.flashbag.get('error') %}
                    {{ flashMessage }}
                {% endfor %}
            </div>

            <label>{{ qi.textoDB('correo') }}</label>
            <input id=\"username\" name=\"_username\" required/>
            <label>{{ qi.textoDB('contraseña') }}</label>
            <input id=\"password\" name=\"_password\" type=\"password\" required/>

            <div class=\"register__form-remember text-center\">
                <input type=\"checkbox\" class=\"styled-checkbox\" id=\"remember_me\" name=\"_remember_me\" value=\"on\">
                <label for=\"remember_me\">{{ qi.textoDB('recordarme') }}</label>
            </div>

            <button class=\"register__form-submit cta-button cta-button--dark\" type=\"submit\">{{ qi.textoDB('ingresar') }}</button>
            <a href=\"{{ path('fos_user_resetting_request') }}\" class=\"register__form-forgot\">{{ qi.textoDB('¿Olvidaste tu nombre de usuario o contraseña?') }}</a>
        </form>
    </main>


{% endblock %}", "CarroiridianBundle:Default:datos.html.twig", "E:\\dev\\plaz\\src\\CarroiridianBundle/Resources/views/Default/datos.html.twig");
    }
}
