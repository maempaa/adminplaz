<?php

/* lay_iridian.html.twig */
class __TwigTemplate_b1eedc2f9dbd66d0e27c18e327c08aff638b0cbf7d09484d921b223d42e7f14f extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'content' => array($this, 'block_content'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lay_iridian.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "lay_iridian.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 2, $this->source); })()), "request", array()), "locale", array()), "html", null, true);
        echo "\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    ";
        // line 9
        $context["seo_arr"] = twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 9, $this->source); })()), "seo", array(0 => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 9, $this->source); })()), "request", array()), "getUri", array(), "method"), 1 => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("homepage")), "method");
        // line 10
        echo "    ";
        $context["seo"] = twig_get_attribute($this->env, $this->source, (isset($context["seo_arr"]) || array_key_exists("seo_arr", $context) ? $context["seo_arr"] : (function () { throw new Twig_Error_Runtime('Variable "seo_arr" does not exist.', 10, $this->source); })()), "seo", array());
        // line 11
        echo "    ";
        $context["url"] = twig_get_attribute($this->env, $this->source, (isset($context["seo_arr"]) || array_key_exists("seo_arr", $context) ? $context["seo_arr"] : (function () { throw new Twig_Error_Runtime('Variable "seo_arr" does not exist.', 11, $this->source); })()), "url", array());
        // line 12
        echo "    ";
        $context["homepage"] = twig_get_attribute($this->env, $this->source, (isset($context["seo_arr"]) || array_key_exists("seo_arr", $context) ? $context["seo_arr"] : (function () { throw new Twig_Error_Runtime('Variable "seo_arr" does not exist.', 12, $this->source); })()), "homepage", array());
        // line 13
        echo "    <title>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 13, $this->source); })()), "titulo", array()), "html", null, true);
        echo "</title>
    <meta name=\"description\" content=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 14, $this->source); })()), "descripcion", array()), "html", null, true);
        echo "\" />
    <link rel=\"image_src\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, (((isset($context["homepage"]) || array_key_exists("homepage", $context) ? $context["homepage"] : (function () { throw new Twig_Error_Runtime('Variable "homepage" does not exist.', 15, $this->source); })()) . "") . twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 15, $this->source); })()), "imagen", array())), "html", null, true);
        echo "\"/>
    ";
        // line 16
        if (twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 16, $this->source); })()), "getSettingDB", array(0 => "seo_autor"), "method")) {
            // line 17
            echo "        <meta name=\"author\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 17, $this->source); })()), "getSettingDB", array(0 => "seo_autor"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 19
        echo "    <meta name=\"twitter:card\" content=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 19, $this->source); })()), "descripcion", array()), "html", null, true);
        echo "\">
    ";
        // line 20
        if (twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 20, $this->source); })()), "getSettingDB", array(0 => "seo_twitter:site"), "method")) {
            // line 21
            echo "        <meta name=\"twitter:site\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 21, $this->source); })()), "getSettingDB", array(0 => "seo_twitter:site"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 23
        echo "    <meta name=\"twitter:title\" content=\"";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 23, $this->source); })()), "titulo", array()), "html", null, true);
        echo "\">
    <meta name=\"twitter:description\" content=\"";
        // line 24
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 24, $this->source); })()), "descripcion", array()), "html", null, true);
        echo "\">
    ";
        // line 25
        if (twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 25, $this->source); })()), "getSettingDB", array(0 => "seo_twitter:creator"), "method")) {
            // line 26
            echo "        <meta name=\"twitter:creator\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 26, $this->source); })()), "getSettingDB", array(0 => "seo_twitter:creator"), "method"), "html", null, true);
            echo "\">
    ";
        }
        // line 28
        echo "    <meta name=\"twitter:image\" content=\"";
        echo twig_escape_filter($this->env, (((isset($context["homepage"]) || array_key_exists("homepage", $context) ? $context["homepage"] : (function () { throw new Twig_Error_Runtime('Variable "homepage" does not exist.', 28, $this->source); })()) . "") . twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 28, $this->source); })()), "imagen", array())), "html", null, true);
        echo "\">
    <meta property=\"og:title\" content=\"";
        // line 29
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 29, $this->source); })()), "titulo", array()), "html", null, true);
        echo "\" />
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:url\" content=\"";
        // line 31
        echo twig_escape_filter($this->env, (isset($context["url"]) || array_key_exists("url", $context) ? $context["url"] : (function () { throw new Twig_Error_Runtime('Variable "url" does not exist.', 31, $this->source); })()), "html", null, true);
        echo "\" />
    <meta property=\"og:image\" content=\"";
        // line 32
        echo twig_escape_filter($this->env, (((isset($context["homepage"]) || array_key_exists("homepage", $context) ? $context["homepage"] : (function () { throw new Twig_Error_Runtime('Variable "homepage" does not exist.', 32, $this->source); })()) . "") . twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 32, $this->source); })()), "imagen", array())), "html", null, true);
        echo "\" />
    <meta property=\"og:description\" content=\"";
        // line 33
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["seo"]) || array_key_exists("seo", $context) ? $context["seo"] : (function () { throw new Twig_Error_Runtime('Variable "seo" does not exist.', 33, $this->source); })()), "descripcion", array()), "html", null, true);
        echo "\" />
    ";
        // line 34
        if (twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 34, $this->source); })()), "getSettingDB", array(0 => "seo_og:site_name"), "method")) {
            // line 35
            echo "        <meta property=\"og:site_name\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 35, $this->source); })()), "getSettingDB", array(0 => "seo_og:site_name"), "method"), "html", null, true);
            echo "\" />
    ";
        }
        // line 37
        echo "    ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 37, $this->source); })()), "getSettingDB", array(0 => "seo_fb:admins"), "method")) {
            // line 38
            echo "        <meta property=\"fb:admins\" content=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 38, $this->source); })()), "getSettingDB", array(0 => "seo_fb:admins"), "method"), "html", null, true);
            echo "\" />
    ";
        }
        // line 40
        echo "


    <link rel=\"icon\" href=\"";
        // line 43
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["qi"]) || array_key_exists("qi", $context) ? $context["qi"] : (function () { throw new Twig_Error_Runtime('Variable "qi" does not exist.', 43, $this->source); })()), "imagen", array(0 => "favicon"), "method"), "html", null, true);
        echo "\">

    ";
        // line 45
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 58
        echo "
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->

    <link rel=\"canonical\" href=\"";
        // line 65
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 65, $this->source); })()), "request", array()), "uri", array()), "html", null, true);
        echo "\" />

</head>
<body>

    <div id=\"content\" class=\"remodal-bg\">
      ";
        // line 71
        $this->loadTemplate("@Home/Default/header.html.twig", "lay_iridian.html.twig", 71)->display($context);
        // line 72
        echo "      ";
        $this->displayBlock('content', $context, $blocks);
        // line 73
        echo "      ";
        $this->loadTemplate("@Home/Default/footer.html.twig", "lay_iridian.html.twig", 73)->display($context);
        // line 74
        echo "    </div>
    <div id=\"loader\" class=\"hidden\"></div>
    ";
        // line 76
        if (((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 76, $this->source); })()), "environment", array()) != "dev") && false)) {
            // line 77
            echo "        ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "2c12a27_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_0") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.min_1.js");
                // line 93
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_1"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_1") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery-ui.min_2.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_2"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_2") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_lodash.min_3.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_3"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_3") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.mCustomScrollbar.concat.min_4.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_4"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_4") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_slideout_5.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_5"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_5") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_swiper.min_6.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_6"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_6") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_shuffle.min_7.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_7"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_7") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jssocials.min_8.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_8"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_8") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_remodal.min_9.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_9"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_9") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_jquery.rateyo.min_10.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_10"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_10") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_loadingoverlay.min_11.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_11"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_11") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_utils_12.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
                // asset "2c12a27_12"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27_12") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3_carrito_13.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
            } else {
                // asset "2c12a27"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_2c12a27") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("js/iridian_3.js");
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 93, $this->source); })()), "html", null, true);
                echo "\" async></script>
        ";
            }
            unset($context["asset_url"]);
            // line 95
            echo "    ";
        } else {
            // line 96
            echo "        <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/jquery/dist/jquery.min.js"), "html", null, true);
            echo "\"></script>
        ";
            // line 98
            echo "    ";
        }
        // line 99
        echo "

    ";
        // line 101
        $this->displayBlock('javascripts', $context, $blocks);
        // line 102
        echo "    <link href=\"https://fonts.googleapis.com/css?family=Raleway:700\" rel=\"stylesheet\">
</body>
</html>";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 45
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 46
        echo "        ";
        if (((twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 46, $this->source); })()), "environment", array()) != "dev") && false)) {
            // line 47
            echo "            ";
            if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
                // asset "e2cd3da_0"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_e2cd3da_0") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/iridianv2_fontawesome-all_1.css");
                // line 51
                echo "            <link rel=\"stylesheet\" href=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 51, $this->source); })()), "html", null, true);
                echo "\" />
            ";
            } else {
                // asset "e2cd3da"
                $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("_assetic_e2cd3da") : $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/iridianv2.css");
                echo "            <link rel=\"stylesheet\" href=\"";
                echo twig_escape_filter($this->env, (isset($context["asset_url"]) || array_key_exists("asset_url", $context) ? $context["asset_url"] : (function () { throw new Twig_Error_Runtime('Variable "asset_url" does not exist.', 51, $this->source); })()), "html", null, true);
                echo "\" />
            ";
            }
            unset($context["asset_url"]);
            // line 53
            echo "        ";
        } else {
            // line 54
            echo "            <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("assets/vendor/font-awesome/web-fonts-with-css/css/fontawesome-all.css"), "html", null, true);
            echo "\">
            <link rel=\"stylesheet\" href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("css/style.css?v=1.0.1"), "html", null, true);
            echo "\">
        ";
        }
        // line 57
        echo "    ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 72
    public function block_content($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 101
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "lay_iridian.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  381 => 101,  364 => 72,  354 => 57,  349 => 55,  344 => 54,  341 => 53,  327 => 51,  322 => 47,  319 => 46,  310 => 45,  298 => 102,  296 => 101,  292 => 99,  289 => 98,  284 => 96,  281 => 95,  195 => 93,  190 => 77,  188 => 76,  184 => 74,  181 => 73,  178 => 72,  176 => 71,  167 => 65,  158 => 58,  156 => 45,  151 => 43,  146 => 40,  140 => 38,  137 => 37,  131 => 35,  129 => 34,  125 => 33,  121 => 32,  117 => 31,  112 => 29,  107 => 28,  101 => 26,  99 => 25,  95 => 24,  90 => 23,  84 => 21,  82 => 20,  77 => 19,  71 => 17,  69 => 16,  65 => 15,  61 => 14,  56 => 13,  53 => 12,  50 => 11,  47 => 10,  45 => 9,  35 => 2,  32 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
<head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    {% set seo_arr = qi.seo(app.request.getUri(),url('homepage')) %}
    {% set seo = seo_arr.seo %}
    {% set url = seo_arr.url %}
    {% set homepage = seo_arr.homepage %}
    <title>{{ seo.titulo }}</title>
    <meta name=\"description\" content=\"{{ seo.descripcion }}\" />
    <link rel=\"image_src\" href=\"{{ homepage~''~seo.imagen }}\"/>
    {% if qi.getSettingDB('seo_autor') %}
        <meta name=\"author\" content=\"{{ qi.getSettingDB('seo_autor') }}\">
    {% endif %}
    <meta name=\"twitter:card\" content=\"{{ seo.descripcion }}\">
    {% if qi.getSettingDB('seo_twitter:site') %}
        <meta name=\"twitter:site\" content=\"{{ qi.getSettingDB('seo_twitter:site') }}\">
    {% endif %}
    <meta name=\"twitter:title\" content=\"{{ seo.titulo }}\">
    <meta name=\"twitter:description\" content=\"{{ seo.descripcion }}\">
    {% if qi.getSettingDB('seo_twitter:creator') %}
        <meta name=\"twitter:creator\" content=\"{{ qi.getSettingDB('seo_twitter:creator') }}\">
    {% endif %}
    <meta name=\"twitter:image\" content=\"{{ homepage~''~seo.imagen }}\">
    <meta property=\"og:title\" content=\"{{ seo.titulo }}\" />
    <meta property=\"og:type\" content=\"article\" />
    <meta property=\"og:url\" content=\"{{ url }}\" />
    <meta property=\"og:image\" content=\"{{ homepage~''~seo.imagen }}\" />
    <meta property=\"og:description\" content=\"{{ seo.descripcion }}\" />
    {% if qi.getSettingDB('seo_og:site_name') %}
        <meta property=\"og:site_name\" content=\"{{ qi.getSettingDB('seo_og:site_name') }}\" />
    {% endif %}
    {% if qi.getSettingDB('seo_fb:admins') %}
        <meta property=\"fb:admins\" content=\"{{ qi.getSettingDB('seo_fb:admins') }}\" />
    {% endif %}



    <link rel=\"icon\" href=\"{{ qi.imagen('favicon') }}\">

    {% block stylesheets %}
        {% if app.environment != \"dev\" and false %}
            {% stylesheets
            'assets/vendor/font-awesome/web-fonts-with-css/css/fontawesome-all.css'
            output='css/iridianv2.css'
            filter='cssrewrite' %}
            <link rel=\"stylesheet\" href=\"{{ asset_url }}\" />
            {% endstylesheets %}
        {% else %}
            <link rel=\"stylesheet\" href=\"{{ asset('assets/vendor/font-awesome/web-fonts-with-css/css/fontawesome-all.css') }}\">
            <link rel=\"stylesheet\" href=\"{{ asset('css/style.css?v=1.0.1') }}\">
        {% endif %}
    {% endblock %}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src=\"https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js\"></script>
    <script src=\"https://oss.maxcdn.com/respond/1.4.2/respond.min.js\"></script>
    <![endif]-->

    <link rel=\"canonical\" href=\"{{ app.request.uri }}\" />

</head>
<body>

    <div id=\"content\" class=\"remodal-bg\">
      {% include('@Home/Default/header.html.twig') %}
      {% block content %}{% endblock %}
      {% include('@Home/Default/footer.html.twig') %}
    </div>
    <div id=\"loader\" class=\"hidden\"></div>
    {% if app.environment != \"dev\" and false %}
        {% javascripts
            'assets/vendor/jquery/dist/jquery.min.js'
            'assets/vendor/jquery-ui/jquery-ui.min.js'
            'assets/vendor/lodash/dist/lodash.min.js'
            'assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js'
            'assets/vendor/slideout.js/dist/slideout.js'
            'assets/vendor/swiper/dist/js/swiper.min.js'
            'assets/vendor/shufflejs/dist/shuffle.min.js'
            'assets/vendor/jssocials/dist/jssocials.min.js'
            'assets/vendor/remodal/dist/remodal.min.js'
            'assets/vendor/rateyo/min/jquery.rateyo.min.js'
            'js/jquery-loading-overlay/src/loadingoverlay.min.js'
            'js/utils.js'
            'js/cart/carrito.js'
        output='js/iridian_3.js'
        filter='?uglifyjs2' %}
        <script src=\"{{ asset_url }}\" async></script>
        {% endjavascripts %}
    {% else %}
        <script src=\"{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}\"></script>
        {#<script src=\"{{ asset('js/cart/carrito.js') }}\"></script>#}
    {% endif %}


    {% block javascripts %}{% endblock %}
    <link href=\"https://fonts.googleapis.com/css?family=Raleway:700\" rel=\"stylesheet\">
</body>
</html>", "lay_iridian.html.twig", "E:\\dev\\plaz\\app\\Resources\\views\\lay_iridian.html.twig");
    }
}
