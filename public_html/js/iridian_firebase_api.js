var restaurantes = [], platos = [];
function getRestaurantes(){
    var query = database.ref("/restaurante");
    query.once("value")
    .then(function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var key = childSnapshot.key;
            var childData = childSnapshot.val();
            restaurantes.push(childData);
        });
        console.log(restaurantes);
    });
}
function getPlatos(){
    var query = database.ref("/plato");
    query.once("value")
    .then(function(snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var key = childSnapshot.key;
            var childData = childSnapshot.val();
            platos.push(childData);
        });
        console.log(platos);
    });
}

$(function(){
    getRestaurantes();
    getPlatos();
});