var shuffleRestaurantTemplate = function(data) {
    /*var occasionsFormatted = _.map(data.tiposOcasion, function(item) {
        return item.nombre.toLowerCase().trim().replace(/\s/g,'');
    }).join(',');*/

    var occasions = _.map(data.tiposOcasion, function(item) {
        return item.nombre.trim();
    }).join(',');

    return '<figure class="restaurants-list__item picture-item" data-groups="' +  occasions + '" data-price="' + data.precio.identificador + '" data-title="' + data.nombre + '">' +
        '<img src="/images/food-plate.jpg" alt="">' +
        '<div class="restaurants-list__item-details">' +
        '<p class="restaurants-list__item-title picture-item__title">' + data.nombre  + '</p>' +
        '<div class="rating__container rating__container--white restaurants-list__item-rating-container text-left">' +
        '<div class="rating rating--small rating--white"><p>' + data.calificacionfinal.toFixed(1) + '<span>BogoEats</span></p></div>' +
        '<div class="rating rating--small rating--white"><p>' + data.calificacionusuarios.toFixed(1) + '<span>Usuarios</span></p></div>' +
        '<div class="rating rating--small rating--white rating--price"><p>' + '$'.repeat(data.precio.identificador) + '<span>Precio</span></p></div>' +
        '<p class="restaurants-list__item-tags">' + occasions.replace(/,/g , ", ") + '</p>'
        '</div>'+
        '</figure>';
}