var planes = [], restaurantes = [], platos = [];
$(function(){
    if(window.location.href.indexOf("perfil") > -1) {
        console.log(getUser().email);

        var ref = database.ref("Plan");
        ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                planes.push(elem.val());
            });
        })
        var ref = database.ref("Restaurante");
        ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                restaurantes.push(elem.val());
            });
        })
        var ref = database.ref("Plato");
        ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                platos.push(elem.val());
            });
        })

        var ref = database.ref("Usuario");
        ref.orderByChild("email").equalTo(getUser().email).limitToFirst(1).once("value")
        .then(function(snapshot) {
            if (!snapshot.exists()) {
                console.log('No hay usuario asociado');
            }
            else {
                //console.log('usuario existente: '+snapshot.val());
                snapshot.forEach(function(userSnapshot) {
                    console.log(userSnapshot.val().id);

                    $.ajax({
                        url: $raiz+"/api/vigencia/"+getUser().email
                    }).done(function(data) {
                        var datos = JSON.parse(data);
                        if(datos.ok) {
                            var inicio = new Date(datos.inicio);
                            console.log(inicio);
                            inicio.setMonth(inicio.getMonth() + 1);
                            $('#por_mes').html(datos.mes);
                            $('#restantes').html(datos.listapps_restantes);
                            $('#hasta').html('Listapps hasta el ' + inicio.getDate() + '/' + (inicio.getMonth() + 1));
                        }else{
                            $('#por_mes').html("");
                            $('#por_mes').next().html("No tienes ningun plan activo");
                            $('#restantes').html("0");
                        }

                    });
                    var ahora= calcTime();

                    if(ahora.getHours() >= 16){
                        ahora.setDate(ahora.getDate()+1);
                    }
                    var mes = ahora.getMonth()+1;
                    mes = ('0'+mes).slice(-2);
                    var hora = ahora.getHours();
                    var dia = ('0'+ahora.getDate()).slice(-2);
                    var hoy =ahora.getFullYear()+'-'+mes+'-'+dia+' '+hora;

                    datos={
                        "email":getUser().email,
                        "fecha": hoy
                    }

                    $.ajax({
                        url: $raiz+"/api/secure/proximo-pedido",
                        method: "POST",
                        data: JSON.stringify(datos)
                    }).done(function(data) {
                        var datos = JSON.parse(data);
                        console.log(datos);
                        if(datos.ok && datos.pedidos.length > 0 ) {
                            var hora=datos.pedidos[0].hora.split(" ");
                            $('#nombre_plato').html(datos.pedidos[0].plato);
                            $('#id_pedido').html("Código: "+datos.pedidos[0].id);
                            $('#hora_pedido').html(hora[1]);
                            $('#fecha_pedido').html(hora[0]);
                            $('#restaurante_direccion').html(datos.pedidos[0].direccion);
                            $('#restaurante_nombre').html(datos.pedidos[0].restaurante);


                        }

                    });

                    /*
                    var ref = database.ref("Compra");
                    ref.orderByChild("usuario").equalTo(userSnapshot.val().id).limitToLast(1).once("value")
                    .then(function(compras) {
                        var comp;
                        var cant_comp = 0;
                        compras.forEach(function(compra) {
                            if(compra.val().estado == 2){
                                comp = compra.val();
                                cant_comp++;
                                console.log(comp);
                            }
                        });
                        var hay_pedidos = false;
                        var ref = database.ref("Pedido");
                        ref.orderByChild("compra").equalTo(comp.id).once("value")
                        .then(function(pedidos) {
                            var usados = 0;

                            plan = $.grep( planes, function( n, i ) {return n.id === comp.plan;})[0];
                            var pedido;
                            pedidos.forEach(function(pedidoSnap) {
                                pedido = pedidoSnap.val();
                                if(pedido.usuario = userSnapshot.val().id ){
                                    usados++;
                                    hay_pedidos = true;
                                }
                            });
                            console.log(usados);
                            if(usados > 0){
                                console.log(pedido);
                                plato = $.grep( platos, function( n, i ) {return n.id === pedido.plato;})[0];
                                restaurante = $.grep( restaurantes, function( n, i ) {return n.id === plato.restaurante;})[0];
                                console.log(plan);
                                $('#nombre_plato').html(plato.nombre);
                                var fecha = new Date(pedido.fecha);
                                var hora = fecha.getHours();
                                if(hora != 12 || hora < 12){
                                    hora = parseInt(hora) - 12;
                                }
                                $('#hora_pedido').html(hora+':'+('0'+fecha.getMinutes()).slice(-2)+' PM');
                                weekDays = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
                                $('#fecha_pedido').html(weekDays[fecha.getDay()]+', '+fecha.getDate()+'/'+(fecha.getMonth() + 1));
                                $('#restaurante_nombre').html(restaurante.nombre);
                                $('#restaurante_direccion').html(restaurante.direccion);
                                $('#restaurante_calificacion').data('rateyo-rating', restaurante.calificacion);


                                var inicio = new Date(comp.inicio);
                                console.log(inicio);
                                inicio.setDate(inicio.getDate() + plan.dias);
                                //inicio.setMonth(inicio.getMonth() + 1);
                                console.log(inicio);

                                $('#hasta').html('Listapps hasta el '+inicio.getDate()+'/'+(inicio.getMonth()+1))
                            }
                            if(!hay_pedidos){
                                var inicio = new Date(comp.inicio);
                                console.log(inicio);
                                inicio.setMonth(inicio.getMonth() + 1);
                                console.log(inicio);

                                $('#por_mes').html(plan.num_pedidos);
                                $('#restantes').html(plan.num_pedidos - usados);
                                $('#hasta').html('Listapps hasta el '+inicio.getDate()+'/'+(inicio.getMonth()+1))
                            }
                        })
                    })
                    */
                });
                /*
                */
            }
        })
    }
});