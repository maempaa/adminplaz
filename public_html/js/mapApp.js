var horarios = [];
$(function(){
    if(window.location.href.indexOf("mapa") > -1) {
        $('#hora').html('');
        var ref = database.ref("Horario");
        ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                horarios.push(elem.val());
                $('#hora').append('<option value="'+elem.val().nombre+'">'+elem.val().nombre+'</option>');
            });
        })
    }
});
function initMap() {
    $(function(){
        new mapApp('restaurants__map');
    });
}

function mapApp() {
    this.mapId = document.getElementById('restaurants__map');
    this.$map = $('.restaurants__map');
    this.$restaurants = $('.restaurants');
    this.$restaurantList = $('#restaurants__list');
    this.$toggleListBtn = $('.restaurants__toggle-list-btn');
    this.$toggleDetailBtn = $('.restaurants__toggle-detail-btn');
    this.$restaurantDetailModal = $('[data-remodal-id=restaurant-detail]').remodal();
    this.$restaurantDetail = $('.restaurants__detail');
    this.$detailPlate = this.$restaurantDetail.find('.app-plate');
    this.$detailPlatedesc = this.$restaurantDetail.find('.app-plate-desc');
    this.$detailName = this.$restaurantDetail.find('.app-name');
    this.$detailImg = this.$restaurantDetail.find('.app-img');
    this.$detailDate = this.$restaurantDetail.find('.app-date');
    this.$detailAddress = this.$restaurantDetail.find('.app-address');
    this.$detailPickUpTime = this.$restaurantDetail.find('.app-pick-up-time');
    this.$detailBook = this.$restaurantDetail.find('.app-book');
    this.$detailStarRating = this.$restaurantDetail.find('.app-star-rating');
    this.$bookModal = $('[data-remodal-id=book-modal]').remodal();
    this.mapStyleArray = mapStyleArray;
    this.userLocation = {
        lat: 4.673676, // Default latitude Bogota
        lng: -74.047617 // Default longitude Bogota
    };
    this.restaurants = [];
    this.plates = [];
    this.date = new Date( Date.now() );
    this.weekDays = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado'];
    this.obtainUserLocation();
    this.addEventListeners();
}

mapApp.prototype.addEventListeners = function() {
    this.$toggleListBtn.on('click', function() {
        this.$restaurants.toggleClass('restaurants--opened');
    }.bind(this));

    this.$toggleDetailBtn.on('click', function() {
        this.$map.toggleClass('restaurants__map-detail-open');
    }.bind(this));

    $( window ).resize(function() {
        if (this.itemSwiper) {
            setTimeout(function() {
                this.itemSwiper.update();
            }.bind(this), 2000);
        }

        if (this.map) {
            //this.map.checkResize();
        }
    }.bind(this));

};

mapApp.prototype.obtainUserLocation = function() {
    /*
    if ("geolocation" in navigator) {
        navigator.geolocation.getCurrentPosition(function(position) {
            this.userLocation.latitude = Number(position.coords.latitude);
            this.userLocation.longitude = Number(position.coords.longitude);

            this.fetchRestaurantsData();
        }.bind(this));
    }*/

    // NO GEO LOCATION ACTIVATED
    this.fetchRestaurantsData();
};

mapApp.prototype.fetchRestaurantsData = function() {
    var query = database.ref("/Restaurante");
    query.orderByChild("visible").equalTo(true);

    $('#loader').removeClass('hidden');

    query.once("value")
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                this.restaurants.push(childData);
            }.bind(this));

            this.fetchPlatesData();
        }.bind(this));
};

mapApp.prototype.fetchPlatesData = function() {
    var query = database.ref("/Plato");
    query.orderByChild("visible").equalTo(true);
    query.once("value")
        .then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childData = childSnapshot.val();
                this.plates.push(childData);
            }.bind(this));

            this.renderMap();
        }.bind(this));
};

mapApp.prototype.renderMap = function() {
    this.map = new google.maps.Map(this.mapId, {
        center: {
            lat: this.userLocation.lat,
            lng: this.userLocation.lng
        },
        scrollwheel: true,
        styles: this.mapStyleArray,
        zoom: 15
    });

    //this.addUserMarker();

    this.addRestaurantsMarkers();

    createRateyo();

    this.map.fitBounds(this.bounds);

    $('#loader').addClass('hidden');
};

mapApp.prototype.addUserMarker = function() {
    this.bounds =  new google.maps.LatLngBounds();

    this.userMarker = new google.maps.Marker({
        map: this.map,
        position: {
            lat: this.userLocation.lat,
            lng: this.userLocation.lng
        },
        title: 'User Marker',
        icon: {
            url: '/images/user-location.svg',
            scaledSize: new google.maps.Size(32, 32)
        }
    });

    this.bounds.extend( this.userMarker.getPosition() );
};

mapApp.prototype.addRestaurantsMarkers = function() {
    this.restaurantsMarkers = [];
    this.bounds =  new google.maps.LatLngBounds();

    _.each(this.restaurants, function(restaurant, index) {
        let result = this.plates.filter(function (entry) {
            return entry.restaurante === restaurant.id
        });
        var platos = result;
        platos = platos.filter(function(plato){
            return diaPlato(plato);
        });

        if(platos.length > 0) {
            var marker = new google.maps.Marker({
                map: this.map,
                position: {
                    lat: Number(restaurant.lat),
                    lng: Number(restaurant.lng)
                },
                title: restaurant.nombre,
                icon: {
                    url: '/images/package-small.png',
                    scaledSize: new google.maps.Size(20, 20)
                }
            });
            
            marker.addListener('click', function() {
                this.map.setZoom(14);
                this.map.setCenter(marker.getPosition());
                //this.showRestaurantDetail(restaurant);
                let result = this.plates.filter(function (entry) {
                    return entry.restaurante === restaurant.id && diaPlato(entry)
                });
                var platos = result;
                if(platos.length > 0){
                    var plato = platos[0];
                    this.showPlateDetail(plato);
                }else{

                }
            }.bind(this));
            

            this.restaurantsMarkers.push(marker);
            this.bounds.extend( marker.getPosition() );

            restaurant.marker = marker;
            this.renderRestaurant(restaurant);
        }
    }.bind(this));
    console.log("original", this.plates);
    this.plates=this.plates.sort(function( a,b){
        return a.orden - b.orden;
    });
    console.log("ordenado",this.plates.length);
    var me = this;
    var vacio = true;
    this.plates.forEach(function(plate){
        console.log("1");
        me.renderPlate(plate);
    });

    if($(".restaurants__item").length == 0 ){
        $("#titulo").html("LA COCINA ESTA CERRADA");
        $("#restaurants__list").html("<h3>Recuerda que Listapp solo opera de Lunes a Viernes</h3>")
    }

};

mapApp.prototype.renderRestaurant = function(restaurant) {

    _.each(this.plates, function(plate) {
        if (plate.restaurante == restaurant.id) {
            plate.restauranteObject = restaurant;
			if(!$.isNumeric(plate.calificacion))
				plate.calificacion = 5;
            //this.renderPlate(plate);
        }
    }.bind(this));
};

mapApp.prototype.renderPlate = function(plate) {
    console.log(plate,diaPlato(plate));
    if( diaPlato(plate) && plate.restauranteObject) {

        //vacio=false;
        var $plateItem = $(restaurantItemTemplate(plate));

        this.$restaurantList.append($plateItem);
        $plateItem.on('click', function () {
            this.showPlateDetail(plate);
        }.bind(this));
        return false;
    }
    return true;
};

function diaPlato(plate){
    var ahora = calcTime();
    if(ahora.getHours() >= 10){
        ahora.setDate(ahora.getDate() + 1);

    }
    var hoy = ahora.getDay();
    if(hoy == 0)
        hoy=7;
    return hoy == plate.dia;

}

mapApp.prototype.showPlateDetail = function(plate) {
    var ahora = calcTime();
    console.log(ahora.getDate());
    if(ahora.getHours() >= 10){
        ahora.setDate(ahora.getDate() + 1);
        console.log(ahora.getDate());
    }
    var mes = ahora.getMonth() + 1;
    mes = ('0'+mes).slice(-2);
    var dia = ('0'+ahora.getDate()).slice(-2);
    var fecha = ahora.getFullYear()+'-'+mes+'-'+dia;
    this.$restaurantDetailModal.open();

    // Data field replacement
    this.$map.addClass('restaurants__map-detail-open');

    this.$detailPlatedesc.html("");
    if(plate.descripcion)
        this.$detailPlatedesc.html(plate.descripcion);
    this.$detailPlate.html(plate.nombre);
    this.$detailName.html(plate.restauranteObject.nombre);
    this.$detailImg.attr('src', plate.foto);
    this.$detailAddress.html(plate.restauranteObject.direccion);
    this.$detailDate.html(this.weekDays[ahora.getDay()] + ' '+ ahora.getDate() + ', ' + (ahora.getMonth()+1) + '/' + ahora.getFullYear());
    this.$detailBook.data('id', plate.restauranteObject.nombre); // TODO use id instead
    this.$detailStarRating.rateYo("rating", plate.calificacion);
    $('#plato').val(plate.id);
    $('#fecha_temp').val(fecha);
    $('#uid').val(getUser().uid);

    this.$restaurantDetail.submit(function(event) {
        event.preventDefault();
        $('#fecha').val($('#form_pedido').val()+' '+$('#hora').val());
        var json = objectifyForm($('#form_pedido').serializeArray());
        console.log(json);
        $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
            var jqxhr = $.post( $raiz+"/api/secure/pedido",json, function() {})
            .done(function(data) {
                $.LoadingOverlay("hide");
                data = JSON.parse(data);
                console.log(data);
                if(data.ok){
                    //this.book(plate);
                    $('#hora_rec').html($('#cad_fecha').html()+' - '+$('#hora').val());
                    var inst = $('[data-remodal-id=book-modal]').remodal();
                    inst.open();
                    //this.$bookModal.open();
                }else{
                    var mens = data.error_msg;
                    console.log(mens);
                    alertar(mens);
                }
            })
            .fail(function() {
            })
            .always(function() {
            });
    }.bind(this));
};

mapApp.prototype.book = function(plate) {
    this.$detailPickUpTime.html('1:30 PM');
    this.$bookModal.open();
    console.log(plate);
};

function calcTime() {
    var offset = -5;
    var d = new Date();
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    var nd = new Date(utc + (3600000*offset));
    return nd;
}

function calcTimeTomorrow() {
    var offset = -5;
    var today = new Date();
    var d = today.add(1).day();
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    var nd = new Date(utc + (3600000*offset));
    return nd;
}