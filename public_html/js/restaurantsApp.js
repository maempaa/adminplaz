function restaurantsList() {
    this.gridId = document.getElementById('grid');
    this.$grid = $('#grid');
    this.$grid = $('#grid');
    this.$tools = $('.restaurants-list__tools');
    this.$filtersBtn = $('.restaurants-list__filters-btn');
    this.$filterOptions = $('.filter-options');
    this.$restaurantModal = $('[data-remodal-id=restaurant-modal]').remodal();
    this.addEventListeners();
    this.fetchRestaurantsData();
}

restaurantsList.prototype.addEventListeners = function() {
    this.$filtersBtn.on('click', function() {
        this.$tools.toggleClass('restaurants-list__tools--filters-opened');
    }.bind(this));
};

restaurantsList.prototype.fetchRestaurantsData = function() {
    fetchRestaurantsData(function(restaurants) {
        this.restaurants = restaurants;

        _.each(this.restaurants, function(restaurant) {
            this.renderRestaurant(restaurant)
        }.bind(this));

        this.allOccasions = _.chain(restaurants)
            .map(function(restaurant) {
                return _.map(restaurant.tiposOcasion, function(item) {
                    return item.nombre.trim();
                });
            })
            .flatten()
            .uniq()
            .valueOf();

        this.renderOccasionsFilters();

        setTimeout(function() {
            this.shuffle = new ShuffleApp(document.getElementById('grid'), {
                mode: 'additive'
            });
        }.bind(this), 200);
    }.bind(this));
};


restaurantsList.prototype.renderRestaurant = function(restaurant) {
    var $restaurantItem = $( shuffleRestaurantTemplate(restaurant) );

    this.$grid.prepend( $restaurantItem );
    $restaurantItem.on('click', function() {
        this.showRestaurantDetail(restaurant);
    }.bind(this));
};

restaurantsList.prototype.renderOccasionsFilters = function() {
    var filterTemplate = function(data) {
        return '<button class="cta-button-2" data-group="' + data + '">' + data + '</button>';
    };

    _.each(this.allOccasions, function(value) {
        this.$filterOptions.append( filterTemplate(value) );
    }.bind(this));
};

restaurantsList.prototype.showRestaurantDetail = function(restaurant) {
    this.$restaurantModal.open();
    console.log(restaurant);
};

function initRestaurants() {
    new restaurantsList();

    //var _kCord = new google.maps.LatLng(-36.874694, 174.735292);
    //var _pCord = new google.maps.LatLng(-36.858317, 174.782284);

    //google.maps.geometry.spherical.computeDistanceBetween(_pCord, _kCord);
    //https://developers.google.com/maps/documentation/javascript/reference/3/geometry
}