var fee = $("#fee_listapp").val();
$(function(){
    if(window.location.href.indexOf("planes_listapp") > -1) {
        getTarjetas();
        addTarjeta();
        validExpDate();
        validCCV();
        cleanDoc();

        checkPlan();
        console.log("la respuestaaa de obrar");
        $.ajax({
            url: "/cobrarfee/"+getUser().email,
        }).done(function(data) {
            data = JSON.parse(data);
            console.log("la respuestaaa de obrar fee",data);
            if(data.cobrar!="si"){
                fee=0;
                $(".class_fee").hide();
                console.log("no cobrar fee");

            }
        })
            .fail(function() {
                console.log("failk");
            })
            .always(function() {
                console.log("always");
                plan();
            });
    }
});
var t_valid = false, d_valid = false, c_valid = false, cant = 0,user;
var descuento_porcentaje =0;
var descuento_valor =0;

function codigoForm() {
    $("#form_codigo").toggle("slow");
}

function validarCupon(){
    descuento_valor=0;
    descuento_porcentaje=0;
    $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
    $.ajax({
        url: "/api/cupon/"+getUser().email+"/"+$("#cupon").val(),
    }).done(function(data) {
        data = JSON.parse(data);
        console.log("la respuesta de cupon",data);
        if(data.ok){
            alertar("Código válido");

            if(data.valor){
                descuento_valor =parseInt(data.valor);
            }
            if(data.porcentaje){
                descuento_porcentaje =parseInt(data.porcentaje);
            }

        }else{
            alertar("Código NO válido");
        }
    })
    .fail(function() {
        console.log("failk");
        alertar("Código NO válido");
    })
    .always(function() {
        console.log("always");
        totales();
        $.LoadingOverlay("hide");
    });
}


function getTarjetas(){
    user = getUser();
    if(user == null){
        alertify.alert('Mensaje','Debes registrarte para poder comprar un plan').set('onok', function(closeEvent){ window.location = $raiz+'/login';} );;
    }else{
        var userId = user.uid;
        console.log(userId);
        console.log("fire user",firebase.auth().currentUser);
        var pass= localStorage.getItem("pass");


            firebase.database().ref('/Tarjeta/' + userId).once('value',function(snapshot) {
                console.log(snapshot.val());
                if(snapshot.val() != null){
                    cant = 0;
                    snapshot.forEach(function(elem){
                        item= elem.val();
                        if(item){
                            cant++;
                            var tarj = $('#inputGroup').clone();
                            tarj.attr('id','input_'+item.id)
                            tarj.find('input').val(item.creditCardTokenId).attr('id','tarj_'+item.id);
                            tarj.find('label').html('**** **** **** '+item.ultimos).attr('for','tarj_'+item.id);
                            $('.tarjetas').append(tarj);
                        }
                    });
                    $('#inputGroup').remove();
                    if(cant > 0){
                        $('.tarjetas').show();
                        $('.tarjetas').append($('#cuotas'));
                    }
                }
            },function(error){
                console.log(error);
            });


    }
}
function addTarjeta(){

    $('#num_tarjeta').validateCreditCard(function(result)
    {
        //console.log(result);
        t_valid = false;
        if(result.card_type){
            t_valid= true;
            $('#tipo_tarjeta').val(result.card_type.name.toUpperCase());
        }else{
            $('#tipo_tarjeta').val("error");
        }

    });
    $('#new_tarjeta').submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        email = user.email;
        uid = user.uid;
        $('#email').val(email);
        $('#uid').val(uid);
        $('#exp_date').val($('#anio').val()+'/'+$('#mes').val());
        validarFecha();
        var json = objectifyForm($(this).serializeArray());
        console.log(json);
        if(json.tarjeta){
            console.log('Enviar pago');
            $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
            var jqxhr = $.post( $raiz+"/api/secure/pagar",json, function() {})
                .done(function(data) {
                    $.LoadingOverlay("hide");
                    data = JSON.parse(data);
                    console.log(data);
                    if(data.ok){
                        var inst = $('[data-remodal-id=confirmation-modal]').remodal();
                        inst.open();
                        /*
                        if(data.compra.estado.nombre == 'APROBADA'){
                            var inst = $('[data-remodal-id=confirmation-modal]').remodal();
                            inst.open();
                        }else{
                            alertar('Lo sentimos, la transacción no ha sido aprobada');
                        }
                        */
                    }else{
                        var mens = data.error_msg;
                        console.log(mens);
                        alertar(mens);
                    }
                })
                .fail(function() {
                })
                .always(function() {
                });
        }else{
            if(!validateForm()){
                alertar('Debes ingresar todos los campos');
            }else if(!t_valid){
                alertar('El número de tarjeta es inválido');
            }else if(!d_valid){
                alertar('La fecha es inválida');
            }else if(!validoCVC()){
                alertar('El código de verificación es inválido');
            }else if(!titularValido()){
                alertar('El titular solo debe contener letras');
            }else{
                $.LoadingOverlay("show",{ zIndex: 99999, image: '/js/jquery-loading-overlay/src/loading.gif'});
                var jqxhr = $.post( $raiz+"/api/secure/agregar-tarjeta",json, function() {
                })
                    .done(function(data) {
                        $.LoadingOverlay("hide");
                        data = JSON.parse(data);
                        console.log(data);
                        if(data.ok){
                            var inst = $('[data-remodal-id=confirmation-modal]').remodal();
                            inst.open();
                        }else{
                            var mens = data.error_msg;
                            console.log(mens);
                            mens = mens.replace('property: ','');
                            mens = mens.replace('expirationDate,','Fecha de expiración:');
                            mens = mens.replace('message:','');
                            mens = mens.replace('paymentMethod,','Método de pago');
                            mens = mens.replace('number','Código de seguridad');
                            console.log(mens);
                            alertar(mens);
                        }
                    })
                    .fail(function() {
                    })
                    .always(function() {
                    });
            }
        }
    });
}

function validoCVC(){
    var str = $('#ccv').val();
    var patt = new RegExp("[0-9]{3}$");
    c_valid = patt.test(str);
    c_valid = c_valid && str.length  == 3;
    if($('#tipo_tarjeta').val()=="AMEX"){
        patt = new RegExp("[0-9]{4}$");
        c_valid = patt.test(str);
        c_valid = c_valid && str.length  == 4;
    }
    return c_valid;
}

function titularValido(){
    var str = $('#titular').val();
    var patt = new RegExp("[a-zA-Z]+$");
    ret= patt.test(str);

    return ret;
}

function objectifyForm(formArray) {
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
function validateForm() {
    var isValid = true;
        $('form input').not("input[name='tarjeta'],input[name='cupon_id']").each(function() {
            if($(this).attr("name") != 'rl'){
                if ( $(this).val() === '' ){
                    console.log($(this).attr("name"),$(this).attr("id"));
                    isValid = false;
                }
            }
        });
    return isValid;
}
function validExpDate(){
    $("#exp_date").keydown(function (e) {
        console.log(e.keyCode,e.shiftKey);
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110,191]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40) || (e.shiftKey && e.keyCode ==55 )) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            if( e.keyCode != 111)
                e.preventDefault();
        }
    });
    $('#exp_date').keyup(function(){
        var str = $(this).val();
        var patt = new RegExp("20[0-9]{2}/(0[1-9]|10|11|12)$");
        d_valid = patt.test(str);
        if(d_valid){
            ano = str.substring(0,4);
            mes = str.substring(5,7);
            actual = new Date();
            console.log("fecha:",ano,mes,actual,(ano == actual.getFullYear() && mes > (actual.getMonth()+1) || ano > actual.getFullYear() ));
            d_valid = (ano == actual.getFullYear() && mes > (actual.getMonth()+1) || ano > actual.getFullYear() );
        }
    });
}
function validarFecha(){

    var str = $('#exp_date').val();
    var patt = new RegExp("20[0-9]{2}/(0[1-9]|10|11|12)$");
    d_valid = patt.test(str);
    if(d_valid){
        ano = str.substring(0,4);
        mes = str.substring(5,7);
        actual = new Date();
        console.log("fecha:",ano,mes,actual,(ano == actual.getFullYear() && mes > (actual.getMonth()+1) || ano > actual.getFullYear() ));
        d_valid = (ano == actual.getFullYear() && mes > (actual.getMonth()+1) || ano > actual.getFullYear() );
    }
}
function validCCV(){
    $("#ccv").keydown(function (e) {
        console.log(e.keyCode);
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#ccv').keyup(function(){
        var str = $(this).val();
        var patt = new RegExp("[0-9]{3}$");
        c_valid = patt.test(str);
        //$(this).val(str.substring(0, 3));
        console.log(d_valid);
    });
}
function cleanDoc(){
    $("#num_doc").keydown(function (e) {
        console.log(e.keyCode);
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}
var precio,servicio, no_plan = true;
function plan(){
    precio = $("input[name='plan']:checked").parent().data('precio');
    totales();
    $('.plan').click(function(){
        precio = $(this).data('precio');
        servicio = $("input[name='plan']:checked").parent().data('servicio');
        totales();
        console.log(precio);
        console.log(servicio);
        console.log(no_plan);
        if(no_plan){
            $('.desc').hide();
            $('#cont_tarjeta').css('display','flex');
        }
    });
}



function totales(){
    var total= precio;
    if(descuento_valor != 0){
        total=total - descuento_valor;
    }else if(descuento_porcentaje != 0){
        total=total - (total*descuento_porcentaje/100);
    }
    //var servicio= $("#servicio_listapp").val();
    //var base = total/1.08;
    fee = parseInt(fee);
    total = total-servicio;
    var imp = total*0.08;
    var imp2 = (servicio + fee) * 0.19;
    var gran_total = total+imp +servicio+fee+imp2;

    $('#precio').html('$ '+addCommas(parseInt(total)));
    $('#servicio').html('$ '+addCommas(servicio));
    $('#impo').html('$ '+addCommas(imp));
    $('#impo2').html('$ '+addCommas(imp2));
    $('#total').html('$ '+addCommas(gran_total));
    $('#precio_total').val(gran_total);
    $('#impuesto').val(imp2);
}
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function checkPlan(){
    var ref = database.ref("Plan");
    ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                planes.push(elem.val());
            });
        })
    var ref = database.ref("Restaurante");
    ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                restaurantes.push(elem.val());
            });
        })
    var ref = database.ref("Plato");
    ref.once("value")
        .then(function(snap) {
            snap.forEach(function(elem){
                platos.push(elem.val());
            });
        })

    var ref = database.ref("Usuario");
    ref.orderByChild("email").equalTo(getUser().email).limitToFirst(1).once("value")
        .then(function(snapshot) {
            if (!snapshot.exists()) {
                console.log('No hay plan asociado');
            }
            else {
                //console.log('usuario existente: '+snapshot.val());
                snapshot.forEach(function(userSnapshot) {
                    console.log(userSnapshot.val().id);
                    var ref = database.ref("Compra");
                    ref.orderByChild("usuario").equalTo(userSnapshot.val().id).limitToFirst(1).once("value")
                        .then(function(compras) {
                            var comp;
                            compras.forEach(function(compra) {
                                comp = compra.val();
                                console.log(comp);
                            });
                            var ref = database.ref("Pedido");
                            ref.orderByChild("compra").equalTo(comp.id).once("value")
                                .then(function(pedidos) {
                                    var usados = 0;
                                    pedidos.forEach(function(pedidoSnap) {
                                        var pedido = pedidoSnap.val();
                                        if(pedido.usuario = userSnapshot.val().id ){
                                            usados++;
                                            console.log(pedido);
                                            plato = $.grep( platos, function( n, i ) {return n.id === pedido.plato;})[0];
                                            restaurante = $.grep( restaurantes, function( n, i ) {return n.id === plato.restaurante;})[0];
                                            plan = $.grep( planes, function( n, i ) {return n.id === comp.plan;})[0];
                                        }
                                        if(usados > 0){
                                            var inicio = new Date(comp.inicio);
                                            inicio.setMonth(inicio.getMonth() + 1);
                                            var hoy = calcTime();
                                            console.log(hoy > inicio);
                                            if(hoy > inicio){
                                                alertar('Lo sentimos, el plan se venció, debes comprar uno nuevo');
                                            }else{
                                                if((plan.dias - usados) <= 0){
                                                    alertar('Lo sentimos, ya reclamaste todos los Listapps de tu plan');
                                                }else{
                                                    no_plan = false;
                                                    $('.plan').click(function(e){
                                                        e.stopPropagation();
                                                        e.preventDefault();
                                                        alertar('Ya tienes un plan comprado');
                                                    });
                                                }
                                            }
                                            $('#hasta').html('Listapps hasta el '+inicio.getDate()+'/'+(inicio.getMonth()+1))
                                        }
                                    });
                                })
                        })
                });
                /*
                */
            }
        })
}
function calcTime() {
    var offset = -5;
    var d = new Date();
    var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    var nd = new Date(utc + (3600000*offset));

    // return time as a string
    return nd;
}