var restaurantItemTemplate = function (itemData) {
    return '<div class="restaurants__item" data-id="' + itemData.id + '">' +
        '<div class="restaurants__item-img" style="background-image: url(\'' + itemData.foto + '\')"></div>' +
        '<p class="restaurants__item-title text-left">' + itemData.nombre + '</p>' + // ToDo__MOCK
        '<p class="restaurants__item-subtitle bold text-left">' + itemData.restauranteObject.nombre + '</p>' +
        '<div class="rateyo" data-rateyo-rating="' + itemData.calificacion + '"></div>' + // ToDo__MOCK
    '</div>';
};