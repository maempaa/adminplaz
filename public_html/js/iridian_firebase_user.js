var database, auth, hand;
$(function(){
    var config = {
        apiKey: "AIzaSyD5NXURVbNlpIyp5MtJYO0nbaepSo6Rtto",
        authDomain: "plazapp-c5d01.firebaseapp.com",
        databaseURL: "https://plazapp-c5d01.firebaseio.com",
        projectId: "plazapp-c5d01",
        storageBucket: "plazapp-c5d01.appspot.com",
        messagingSenderId: "218552618964"
    };
    firebase.initializeApp(config);
    auth = firebase.auth();
    database = firebase.database();
    estado();
    $('#google').click(function(e){
        e.preventDefault();
        signInWithGoogle();
    });
    $('#facebook').click(function(e){
        e.preventDefault();
        signInWithFacebook();
    });
    $('#form_registro').submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        crearUsuario();
    });
    $('#form_login').submit(function(e){
        if($('#username').val() != 'admin'){
            e.preventDefault();
            e.stopPropagation();
            login();
        }
    });
    $('#form_reset').submit(function(e){
        e.preventDefault();
        e.stopPropagation();
        reset();
    });
    hand = false;
    //datos = {"email":"mauricio2@iridian.co","displayName":"Mauricio Caviedes","Telefono":"3","photoURL":""};
    //checkUser(datos);
});


function alertar(msg){
    //alert(msg);
    alertify.alert('Mensaje',msg);
}

function checkUser(datos){
    //var newRef = database.ref('users_listapp'+'/').push(datos);
    console.log(datos);
    var ref = database.ref("users_listapp");
    ref.orderByChild("email").equalTo(datos.email).limitToFirst(1).once("value")
    .then(function(snapshot) {
        if (!snapshot.exists()) {
            console.log("No existe "+datos.email);
            var newRef = database.ref('users_listapp'+'/').push(datos).then(function(){
                if(estaEnLR()) {
                    window.location = $raiz+'/perfil';
                }
            });
        }
        else {
        }
    })
}

function crearUsuario(){
    var email = $('#fos_user_registration_form_email').val();
    var password = $('#fos_user_registration_form_plainPassword_first').val();
    var nombre = $('#fos_user_registration_form_username').val();
    var telefono = $('#fos_user_registration_form_telefono').val();
    firebase.auth().createUserWithEmailAndPassword(email, password).then(function(){
        hand = true;
    })
    .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
        alertar('La contraseña es muy debíl');
        } else if(errorCode == 'auth/email-already-in-use'){
            alertar('El email ya está en uso');
        }else if(errorCode == 'auth/operation-not-allowed'){
            alertar('Lo sentimos, esta operación no es permitida en este momento');
        }else if(errorCode == 'auth/invalid-email'){
            alertar('Email inválido');
        }else {
            alertar(errorMessage);
        }
        console.log(error);
      });
}

function signOut(){
    firebase.auth().signOut().then(function() {
        // Sign-out successful.
        window.location = $raiz+'/';
        localStorage.removeItem('user');
      }).catch(function(error) {
        // An error happened.
      });
}

function login(){
    firebase.auth().signInWithEmailAndPassword($('#username').val(),$('#password').val()).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        if (errorCode == 'auth/invalid-email') {
            alertar('Emial inválido');
        } else if(errorCode == 'auth/user-disabled'){
            alertar('Usuario deshabilitado');
        }else if(errorCode == 'auth/user-not-found'){
            alertar('usuario inválido');
        }else if(errorCode == 'auth/wrong-password'){
            alertar('Contraseña inválida');
        }else {
            alertar(errorMessage);
        }
      });
}

function estaEnLR(){
    return (window.location.href.indexOf("login") > -1 || window.location.href.indexOf("registro") > -1);
}
function estado(){
    c_user = getUser();
    console.log(c_user);
    if(c_user){
        if(estaEnLR()) {
            window.location = $raiz+'/perfil';
         }
         $('#login').text('Cerrar sesión');
         //$('#login').unbind('click');
         $('#login').click(function(){
             signOut();
         });
        $('#login_mob').text('Cerrar sesión');
        //$('#login').unbind('click');
        $('#login_mob').click(function(){
            signOut();
        });
         $('#registro').remove();
         if(window.location.href.indexOf("perfil") > -1){
            perfil();
         }
    }else{
        if(window.location.href.indexOf("perfil") > -1) {
            window.location = $raiz+'/login';
         }
    }
    firebase.auth().onAuthStateChanged(function(user) {
        console.log(user);
        if (user) {
            // User is signed in.
            localStorage.setItem('user', JSON.stringify(user));
            var displayName = user.displayName;
            var email = user.email;
            var emailVerified = user.emailVerified;
            var photoURL = user.photoURL;
            var isAnonymous = user.isAnonymous;
            var uid = user.uid;
            var providerData = user.providerData;
            
            var nombre = $('#fos_user_registration_form_username').val();
            var telefono = $('#fos_user_registration_form_telefono').val();
            if(!displayName){
                console.log('no display');
                var user = firebase.auth().currentUser;
                user.updateProfile({
                    displayName: nombre,
                    telefono: telefono
                }).then(function() {
                    datos = {"email":email,"displayName":nombre,"Telefono":telefono,"photoURL":photoURL};
                    checkUser(datos);
                    console.log(displayName);
                }).catch(function(error) {
                    // An error happened.
                });
            }else{
                
                console.log('si display: '+displayName);
                datos = {"email":email,"displayName":displayName,"photoURL":photoURL};
                checkUser(datos);
                console.log(displayName);
                if(estaEnLR()) {
                    window.location = $raiz+'/perfil';
                }
            }
                

            
             
          // ...
        } else {
            console.log("salio este man");
            //localStorage.setItem('user',"");
            //window.location = $raiz+'/login';

        }
      });
}
function getUser(){
    return JSON.parse(localStorage.getItem('user'));
}

function signInWithGoogle(){
    auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
      resp = auth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(function(result) {
            var token = result.credential.accessToken;
            var user = result.user;
            console.log(user);
        }).catch(function(error) {
        if (error.code === 'auth/account-exists-with-different-credential') {
            console.log(error);
          var pendingCred = error.credential;
          var email = error.email;
          auth.fetchProvidersForEmail(email).then(function(providers) {
            if (providers[0] === 'password') {
                var password = promptUserForPassword();
                auth.signInWithEmailAndPassword(email, password).then(function(user) {
                // Step 4a.
                return user.link(pendingCred);
              }).then(function() {
                goToApp();
              });
              return;
            }
            var provider = getProviderForProviderId(providers[0]);
            auth.signInWithPopup(provider).then(function(result) {
              result.user.link(pendingCred).then(function() {
                goToApp();
              });
            });
          });
        }else{
            alertar('Lo sentimos, se presentó un error');
        }
      });
      console.log(resp);
}

function signInWithFacebook(){
    auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()).then(function(result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        console.log(user);
        // ...
      }).catch(function(error) {
        if (error.code === 'auth/account-exists-with-different-credential') {
          var pendingCred = error.credential;
          var email = error.email;
          auth.fetchSignInMethodsForEmail(email).then(function(methods) {
            if (methods[0] === 'password') {
              var password = promptUserForPassword(); // TODO: implement promptUserForPassword.
              auth.signInWithEmailAndPassword(email, password).then(function(user) {
                return user.link(pendingCred);
              }).then(function() {
                goToApp();
              });
              return;
            }
            var provider = getProviderForProviderId(methods[0]);
            auth.signInWithPopup(provider).then(function(result) {
              result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
                goToApp();
              });
            });
          });
        }else{
            alertar('Lo sentimos, se presentó un error');
        }
      });
}

function reset(){
    auth.sendPasswordResetEmail($('#username').val()).then(function() {
        // Email sent.
        alertar('Hemos enviado un mail para que puedas cambiar tu contraseña');
      }).catch(function(error) {
          console.log(error)
        // An error happened.
      });
}

function promptUserForPassword(){

}

function perfil(){
    c_user = getUser();
    $('#nombre').text(c_user.displayName);
}

