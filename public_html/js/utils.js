$raiz = '';
if(window.location.href.indexOf("app_dev.php") > -1) {
    $raiz = '/app_dev.php';
}

var form_news;

function newsletter(){
    $('#form_newsletter').submit(function (e) {
        $.LoadingOverlay("show",{ zIndex: 9999, image: '/js/jquery-loading-overlay/src/loading.gif'});
        e.preventDefault();hasta
        $('#terms_error,#news_email_error').hide();
        $acepto = $('#acepta').prop('checked');
        if($acepto){
            if(validateEmail($(this).find('input[type="email"]').val())){
                data = $(this).serialize();
                $.ajax({
                    url: $raiz+"/newsletter/"+$(this).find('input[type="email"]').val()
                }).done(function(data) {
                    $.LoadingOverlay("hide");
                    $('#exitoso').remove();
                    if(data.success == 1 || data.success == "1"){
                        $('#form_newsletter').prepend('<p id="exitoso">Inscrito exitosamente</p>');
                        $('#form_newsletter').find('input[type="email"]').val('');
                    }else{
                        $('#form_newsletter').prepend('<p id="exitoso" class="error">Email inscrito anteriormente</p>');
                    }
                });
            }else {
                $.LoadingOverlay("hide");
                $('#news_email_error').css('display','inline-block');
            }
        }else{
            $.LoadingOverlay("hide");
            $('#terms_error').css('display','inline-block');
        }
    });
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

$(function(){

    $(".share").jsSocials({
        shares: [
            {
                share: "twitter",
                logo: "fab fa-twitter",
            },
            {
                share: "facebook",
                logo: "fab fa-facebook",
            }
        ]
    });

    newsletter();
    if(isMobile){
        var slideout = new Slideout({
            'panel': document.getElementById('content'),
            'menu': document.getElementById('mobile-menu'),
            'padding': 256,
            'tolerance': 70,
            'touch': false
        });

        var $mobileMenuBtn = $('.menu__mobile-menu-btn');

        if($mobileMenuBtn.length > 0){
            $mobileMenuBtn.on('click', function() {
                slideout.toggle();
            });
        }
    }
});

function scrollToElement(element) {
    $('html, body').animate({
        scrollTop: $(element).offset().top
    }, 1000);
}


function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function setMainHeight() {
    var windowHeight =  $(window).height();
    var $menu = $('#menu');
    var menuHeight = $menu.height() + parseInt($menu.css('padding-top'), 10) + parseInt($menu.css('padding-bottom'), 10);
    var $footer = $('footer');
    var footerHeight = $footer.height() + parseInt($footer.css('padding-top'), 10) + parseInt($footer.css('padding-bottom'), 10);

    $('main').css({minHeight: (windowHeight - (menuHeight + footerHeight)) + 'px'});
}


function createRateyo() {
    if(!(window.location.href.indexOf("perfil") > -1)) {
        $(".rateyo").rateYo({
            halfStar: true,
            readOnly: true,
            ratedFill: "#15EACB",
            normalFill: "#B3B3B3",
            starWidth: "18px",
            starSvg: "<svg xmlns='http://www.w3.org/2000/svg' width='18' height='18' viewBox='0 0 24 24'>"+
            "<path d='M12 .587l3.668 7.568 8.332 1.151-6.064 5.828 1.48 8.279-7.416-3.967-7.417 3.967 1.481-8.279-6.064-5.828 8.332-1.151z'></path>"+
            "</svg>"
        });
    }
}

$( document ).ready(function() {
    setMainHeight();
    $( ".datepicker" ).datepicker();

    $(".scrollbar").mCustomScrollbar();

    var $bookingGuests = $('.booking__guests');

    $('.booking__guests-btn--substract').on('click', function () {
        var oldValue = Number( $bookingGuests.val() );

        if (oldValue > 0) {
            $bookingGuests.val(oldValue - 1);
        }
    });

    $('.booking__guests-btn--add').on('click', function () {
        var oldValue = Number( $bookingGuests.val() );
        $bookingGuests.val(oldValue + 1);

        if (oldValue < 100) {
            $bookingGuests.val(oldValue + 1);
        }
    });

    $('.cta-selector').click(function(event) {
        setTimeout(
            function(){
                $([document.documentElement, document.body]).animate({
                    scrollTop: $("#cont_tarjeta").offset().top
                }, 2000);
            },
            10
        );
        
        console.log('hice scroll');
        $('.cta-button').removeClass('cta-selector--active');
        $(this).addClass('cta-selector--active');
        /*
        var $item = $(event.currentTarget);
        var id = $item.data('id');
        var className = 'cta-selector--active';

        $('[data-id=' + id + ']').removeClass(className);
        $item.addClass(className);
        */
    });

    createRateyo();
});


$( window ).resize(function() {
    setMainHeight();
});


$(window).on("load", function() {

});

$(window).scroll(function(){

});



function isMobile() {
    return(/Android|iPhone|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) );
}

function isIpad(){
    return navigator.userAgent.match(/iPad/i);
}
