var database;
$(function(){
    var config = {
        apiKey: "AIzaSyD5NXURVbNlpIyp5MtJYO0nbaepSo6Rtto",
        authDomain: "plazapp-c5d01.firebaseapp.com",
        databaseURL: "https://plazapp-c5d01.firebaseio.com",
        projectId: "plazapp-c5d01",
        storageBucket: "plazapp-c5d01.appspot.com",
        messagingSenderId: "218552618964"
    };
    firebase.initializeApp(config);
    database = firebase.database();
    //interceptForms();
    firebaseImages();
});

function interceptForms() {
    $('.action-save').css('display','none').after('<a href="" id="enviar" class="btn btn-primary"><i class="fa fa-save"></i>Save</a>');
    $('#delete-form').remove();
    $('#enviar').click(function (e) {
        e.preventDefault();
        $form = $('#main form');
        if(requeridos($form)){
            console.log('valido');
            json_obj = objectifyForm($form.serializeArray());
            crear(json_obj,$form.attr('name'));
        }else{
            console.log('invalido');
        }
    });
}

function crear(json_obj, name) {
    $form = $('#main form');
    key = $('#'+name+'_key_firebase').val();
    if(key){
        console.log('existe');
        var newRef = database.ref(name+'/'+key).set(json_obj);
        console.log(newRef);
    }else{
        console.log('no existe');
        var newRef = database.ref(name+'/').push(json_obj);
        $('#'+name+'_key_firebase').val(newRef.key);
    }
    $form.submit();
}

function firebaseImages(){
    var imgs = ['plato_foto','restaurante_foto','categoria_imagen','producto_imagen','categoriablog_imagen','post_image'];
    for(var i = 0; i < imgs.length; i++){
        var id = imgs[i];
        var el = $('#'+id);
        if(el.length > 0){
            el.after('<div class="fire_cont" data-id="'+id+'"><input type="file" class="fire"></div>');
            if(el.val() != null)
                el.next().append('<img src="'+el.val()+'" style="max-width: 200px; max-height: 200px; border: 10px solid #ffffff; margin-top: 10px"/>');
            el.attr('type','hidden');
        }
    }
    const ref = firebase.storage().ref();
    var padre;
    $( ".fire" ).change(function () {
        padre = $(this).parent();
        const file = $(this).get(0).files[0]
        const name = (+new Date()) + '-' + file.name;
        const metadata = {
            contentType: file.type
        };
        const task = ref.child(name).put(file, metadata);
        task.then((snapshot) => {
            var meta = snapshot.metadata;
            console.log(snapshot);
            const url = 'https://firebasestorage.googleapis.com/v0/b/'+meta.bucket+'/o/'+meta.fullPath+'?alt=media';
            console.log(url);
            padre.find('img').remove();
            padre.append('<img src="'+url+'"/>');
            $('#'+padre.data('id')).val(url);
        }).catch((error) => {
            console.error(error);
        });
    });
}


function objectifyForm(formArray) {//serialize data function

    var returnArray = {};
    for (var i = 0; i < formArray.length; i++){
        if(formArray[i]['name'] != 'referer' && !formArray[i]['name'].includes('token') && formArray[i]['name'].includes('[')){
            var campo = formArray[i]['name'];
            campo = campo.split('[')[1];
            campo = campo.split(']')[0];
            returnArray[campo] = formArray[i]['value'];
        }
    }
    return returnArray;
}

function requeridos(form) {
    form = form.get(0);
    for(var i=0; i < form.elements.length; i++){
        if(form.elements[i].value === '' && form.elements[i].hasAttribute('required')){
            alert('Debes llenar todos los campos requeridos');
            return false;
        }
    }
    return true;
}

